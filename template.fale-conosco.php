<?php 
/**
  * Template Name: Fale conosco
  *
  * @package WordPress
  * @subpackage modabiz
  * @since ModaBiz 1.0
  */
 global $modabiz_option;

 if(isset($_POST['submited'])) {
 	$nome = filter_var($_POST['nome'],FILTER_SANITIZE_STRING);
 	$email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
 	$telefone = filter_var($_POST['tel'],FILTER_SANITIZE_STRING);
 	$ciduf = filter_var($_POST['ciduf'],FILTER_SANITIZE_STRING);
 	$assunto = filter_var($_POST['assunto'],FILTER_SANITIZE_STRING);
 	$mensagem = filter_var($_POST['mensagem'],FILTER_SANITIZE_STRING);

 	//Verificar os campos obrigatórios
 	if($nome && !empty($nome))
 		$err_nome = true;
 	else
 		$err_nome = false;

 	if($telefone && !empty($telefone))
 		$err_telefone = true;
 	else
 		$err_telefone = false;

 	if($email && !empty($email))
 		$err_email = true;
 	else
 		$err_email = false;

 	if($mensagem && !empty($mensagem))
 		$err_mensagem = true;
 	else
 		$err_mensagem = false;

 	//Se todos os campos forem válidos, redirecione
 	if($err_nome && $err_telefone && $err_email && $err_mensagem) {
 		$msg = "Nome: " . $nome . "\n";
 		$msg .= "Email: " . $email . "\n";
 		$msg .= "Telefone: " . $telefone . "\n";
 		$msg .= "Assunto: " . $assunto . "\n";
 		$msg .= "Mensagem: " . $mensagem . "\n";

 		if($modabiz_option['page-contato-emails']) {
 			$to = explode(', ', $modabiz_option['page-contato-emails']);
 		} else {
 			$to = 'contato@modabiz.com.br';
 		}
 		
 		if(wp_mail( $to, $assunto, $msg)) {
 			wp_redirect('http://google.com');
 		} else {
 			$err_servidor = false;
 		}
 	}
 }
 get_header();
?>	
	<section class="small-12 left section-block">
		<div class="row">
			<?php 
				//Breadcrumb
				get_template_part( 'content' , 'breadcrumb' );

				//Cabeçalho da página
				get_template_part( 'content' , 'header' );
			?>

			<!-- inicio do template -->
			<section class="small-12 left page-template">
				<header>
					<h6 class="divide-30 column"><?php echo get_field('template_excerpt'); ?></h6>
				</header>
				
				<!-- formulario -->
				<div class="small-12 medium-6 columns">
					
					<form action="<?php the_permalink(); ?>" class="small-12 left generic-form" id="contact-form" method="post">
						<?php if(isset($err_servidor) && !$err_servidor) echo "<p><span class=\"error\">Ocorreu algum erro interno. Tente novamente dentro de alguns instantes.</span></p>"; ?>
						<p>
							<input type="text" maxwidth="250" required name="nome" title="Seu nome" placeholder="NOME *">
							<?php if(isset($err_nome) && !$err_nome) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="email" required name="email" title="Seu email" placeholder="E-MAIL *">
							<?php if(isset($err_email) && !$err_email) echo "<span class=\"error\">Campo obrigatório. E-mail inválido.</span>"; ?>
						</p>

						<p>
							<input type="tel" required name="tel" class="phone" title="Seu telefone" placeholder="TELEFONE *">
							<?php if(isset($err_telefone) && !$err_telefone) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="text" required maxwidth="250" name="ciduf" title="Cidade e Estado" placeholder="CIDADE/ESTADO">
						</p>

						<p>
							<select required name="assunto" title="Selecionar assunto" class="no-margin">
								<option value="Assuntos gerais">ASSUNTO *</option>
							</select>
						</p>

						<p>
							<textarea required name="mensagem" maxwidth="1000" title="Digite uma mensagem" class="small-12 left no-margin" placeholder="Mensagem *" rows="10"></textarea>
							<?php if(isset($err_mensagem) && !$err_mensagem) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p class="text-right">
							<button type="submit" name="submited" class="generic-btn text-up no-margin button"><strong>Enviar</strong></button>
						</p>
					</form>
				</div>

				<!-- dados de contato -->
				<div class="small-12 medium-6 columns contact-info">
					<?php

						//Telefone
						if(isset($modabiz_option['corp-telefone']) && !empty($modabiz_option['corp-telefone'])):
							//$phone = explode(', ', $modabiz_option['corp-telefone']);
						?>
						<h5 class="no-margin secondary"><span class="icon-phone"></span> Telefone</h5>
						<h5 class="divide-30"><?php echo $modabiz_option['corp-telefone']; ?></h5>
						<?php
						endif;

						//Whasapp
						if(isset($modabiz_option['corp-whatsapp']) && !empty($modabiz_option['corp-whatsapp'])):
						?>
						<h5 class="no-margin secondary"><span class="icon-whatsapp"></span> Whatsapp</h5>
						<h5 class="divide-30"><?php echo $modabiz_option['corp-whatsapp']; ?></h5>
						<?php
						endif;

						//Endereço do escritório
						?>
						<p class="divide-5"><strong>Endereço do escritório</strong></p>
						<p class="no-margin">
							<?php 
								if(!empty($modabiz_option['corp-rua'])) echo $modabiz_option['corp-rua']; 
								if(!empty($modabiz_option['corp-bairro'])) echo ', ' . $modabiz_option['corp-bairro'];
								if(!empty($modabiz_option['corp-cep'])) echo ', CEP ' . $modabiz_option['corp-cep'];
							?>
						</p>
						<p>
							<?php 
								if(!empty($modabiz_option['corp-cidade'])) echo $modabiz_option['corp-cidade'];
							?>
						</p>
						
						<?php if(!empty($modabiz_option['corp-horario'])): ?>
						<p class="divide-5"><strong>Horário de funcionamento</strong></p>
						<p class="divide-30">
							<?php 
								 echo $modabiz_option['corp-horario'];
							?>
						</p>
						<p id="map-layer" class="mini-map small-12 left"
						data-arraylocal="<?php echo get_stylesheet_directory_uri() . "/places.json"; ?>"
						data-brandicon="<?php echo $modabiz_option['gmap-placeicon']['url']; ?>"
						data-lat="<?php echo $modabiz_option['gmap-lat']; ?>"
						data-lng="<?php echo $modabiz_option['gmap-lng']; ?>"
						></p>
						<?php
						endif;
					?>
				</div>
			</section>
		</div>
	</section>

 	<?php
 	//if(is_active_sidebar('contact-layers')):
		/**
		 * Camadas para a página principal
		 */
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>
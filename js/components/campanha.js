(function() {
	$('h3[data-icon]').hover(
		function() {
			$(this).find('img').eq(0).attr('src', $(this).data('icon-hover'));
		},
		function() {
			$(this).find('img').eq(0).attr('src', $(this).data('icon'));
		}
	);
})();
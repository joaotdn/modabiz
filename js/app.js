
// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
(function($) {
  var items = new Array(),
    errors = new Array(),
    onComplete = function() {},
    current = 0;

  var jpreOptions = {
    splashVPos: '35%',
    loaderVPos: '75%',
    splashID: '#jpreContent',
    showSplash: true,
    showPercentage: true,
    autoClose: true,
    closeBtnText: 'Start!',
    onetimeLoad: false,
    debugMode: false,
    splashFunction: function() {}
  }

  //cookie
  var getCookie = function() {
    if( jpreOptions.onetimeLoad ) {
      var cookies = document.cookie.split('; ');
      for (var i = 0, parts; (parts = cookies[i] && cookies[i].split('=')); i++) {
        if ((parts.shift()) === "jpreLoader") {
          return (parts.join('='));
        }
      }
      return false;
    } else {
      return false;
    }

  }
  var setCookie = function(expires) {
    if( jpreOptions.onetimeLoad ) {
      var exdate = new Date();
      exdate.setDate( exdate.getDate() + expires );
      var c_value = ((expires==null) ? "" : "expires=" + exdate.toUTCString());
      document.cookie="jpreLoader=loaded; " + c_value;
    }
  }

  //create jpreLoader UI
  var createContainer = function() {

    jOverlay = $('<div></div>')
    .attr('id', 'jpreOverlay')
    .css({
      position: "fixed",
      top: 0,
      left: 0,
      width: '0',
      height: '0',
      zIndex: 0
    })
    .appendTo('body');

    if(jpreOptions.showSplash) {
      jContent = $('<div></div>')
      .attr('id', 'jpreSlide')
      .appendTo(jOverlay);

      var conWidth = $(window).width() - $(jContent).width();
      $(jContent).css({
        position: "absolute",
        top: jpreOptions.splashVPos,
        left: Math.round((50 / $(window).width()) * conWidth) + '%'
      });
      $(jContent).html($(jpreOptions.splashID).wrap('<div/>').parent().html());
      $(jpreOptions.splashID).remove();
      jpreOptions.splashFunction()
    }

    jLoader = $('<div></div>')
    .attr('id', 'jpreLoader')
    .appendTo(jOverlay);

    var posWidth = $(window).width() - $(jLoader).width();
    $(jLoader).css({
      position: 'absolute',
      top: jpreOptions.loaderVPos,
      left: Math.round((50 / $(window).width()) * posWidth) + '%'
    });

    jBar = $('<div></div>')
    .attr('id', 'jpreBar')
    .css({
      width: '100%',
      height: '3px',
      position: 'fixed',
      top: '0',
      left: '0',
      zIndex: 999999
    })
    .addClass('bg-primary')
    .appendTo('body'); // aqui adiciona a barra

    if(jpreOptions.showPercentage) {
      jPer = $('<div></div>')
      .attr('id', 'jprePercentage')
      .css({
        position: 'relative',
        height: '100%'
      })
      .appendTo(jLoader)
      .html('Loading...');
    }
    if( !jpreOptions.autoclose ) {
      jButton = $('<div></div>')
      .attr('id', 'jpreButton')
      .on('click', function() {
        loadComplete();
      })
      .css({
        position: 'relative',
        height: '100%'
      })
      .appendTo(jLoader)
      .text(jpreOptions.closeBtnText)
      .hide();
    }
  }

  //get all images from css and <img> tag
  var getImages = function(element) {
    $(element).find('*:not(script)').each(function() {
      var url = "";

      if ($(this).css('background-image').indexOf('none') == -1 && $(this).css('background-image').indexOf('-gradient') == -1) {
        url = $(this).css('background-image');
        if(url.indexOf('url') != -1) {
          var temp = url.match(/url\((.*?)\)/);
          url = temp[1].replace(/\"/g, '');
        }
      } else if ($(this).get(0).nodeName.toLowerCase() == 'img' && typeof($(this).attr('src')) != 'undefined') {
        url = $(this).attr('src');
      }

      if (url.length > 0) {
        items.push(url);
      }
    });
  }

  //create preloaded image
  var preloading = function() {
    for (var i = 0; i < items.length; i++) {
      if(loadImg(items[i]));
    }
  }
  var loadImg = function(url) {
    var imgLoad = new Image();
    $(imgLoad)
    .load(function() {
      completeLoading();
    })
    .error(function() {
      errors.push($(this).attr('src'));
      completeLoading();
    })
    .attr('src', url);
  }

  //update progress bar once image loaded
  var completeLoading = function() {
    current++;

    var per = Math.round((current / items.length) * 100);
    $(jBar).stop().animate({
      width: per + '%'
    }, 500, 'linear');

    if(jpreOptions.showPercentage) {
      $(jPer).text(per+"%");
    }

    //if all images loaded
    if(current >= items.length) {
      current = items.length;
      setCookie();  //create cookie

      if(jpreOptions.showPercentage) {
        $(jPer).text("100%");
      }

      //fire debug mode
      if (jpreOptions.debugMode) {
        var error = debug();
      }


      //max progress bar
      $(jBar).stop().animate({
        width: '100%'
      }, 500, 'linear', function() {
        //autoclose on
        if( jpreOptions.autoClose )
          loadComplete();
        else
          $(jButton).fadeIn(1000);
      });
    }
  }

  //triggered when all images are loaded
  var loadComplete = function() {
    $(jOverlay).fadeOut(800, function() {
      $(jOverlay).remove();
      onComplete(); //callback function
    });
  }

  //debug mode
  var debug = function() {
    if(errors.length > 0) {
      var str = 'ERROR - IMAGE FILES MISSING!!!\n\r'
      str += errors.length + ' image files cound not be found. \n\r';
      str += 'Please check your image paths and filenames:\n\r';
      for (var i = 0; i < errors.length; i++) {
        str += '- ' + errors[i] + '\n\r';
      }
      return true;
    } else {
      return false;
    }
  }

  $.fn.jpreLoader = function(options, callback) {
        if(options) {
            $.extend(jpreOptions, options );
        }
    if(typeof callback == 'function') {
      onComplete = callback;
    }

    //show preloader once JS loaded
    $('body').css({
      'display': 'block'
    });

    return this.each(function() {
      if( !(getCookie()) ) {
        createContainer();
        getImages(this);
        preloading();
      }
      else {  //onetime load / cookie is set
        $(jpreOptions.splashID).remove();
        onComplete();
      }
    });
    };

})(jQuery);

$.fn.getDataThumb = function(options) {
    options = $.extend({
        bgClass: 'bg-cover'
    }, options || {});
    return this.each(function() {
        var th = $(this).data('thumb');
        if (th) {
            $(this).css('background-image', 'url(' + th + ')').addClass(options.bgClass);
        }
    });
};

$('*[data-thumb]').getDataThumb();

$(document).foundation();

$(document).ready(function() {
    $('#jpreBar', '#jpreLoader').clone().appendTo('#main-menu');
    $('body').jpreLoader({
        //loaderVPos: '41.5%',
        //splashID: '#logo-footer',
        //autoClose: false,
        showPercentage: false,
        closeBtnText: ''
    }, function() {
       //galeria
        $('.grid').masonry({
            itemSelector: '.gallery-item'
        });
        $('.show-onload').addClass('show');
        $('.hide-onload').fadeOut('fast');
    });

    gapi.plusone.go();
});

$('figure > a:not(.cpn)','#gallery-campaing').on('click',function(e) {
  e.preventDefault();
});

//Menu fixo
$('#main-menu').clone().appendTo('#nav-fixed');

$(window).on('scroll',function() {
 if($(this).scrollTop() > 400) {
  $('#nav-fixed').addClass('show');
 } else {
  $('#nav-fixed').removeClass('show');
 }
});

//iteração com menu offcanvas
$('.icon-menu, .icon-cancel, .hide-menu').on('click',function(e) {
  e.preventDefault();
  $('#off-menu').toggleClass('show');
  $('.hide-menu').toggleClass('show');
});
//scroll para menu offcanvas
$('#off-menu').perfectScrollbar();
$('#nav-loactions').perfectScrollbar();
$('#lb-table').perfectScrollbar();

/**
 * Home slide
 */
(function() {
  $('.slider-items').on('cycle-after', function() {
    $('hgroup',this).addClass('show');
  });
  $('.slider-items').on('cycle-before', function() {
    $('hgroup',this).removeClass('show');
  });
  $('.slider-items').on('cycle-initialized', function() {
    $('hgroup',this).addClass('show');
  });
})();

//Modals
$('.video-modal').on('open.fndtn.reveal', function () {
  var dt = $('figure',this).data('media');
  $('figure',this).html(dt);
});
$('.video-modal').on('closed.fndtn.reveal', function () {
  var dt = $('figure',this).data('media');
  $('figure',this).html('');
});

/**
 * jquery.mask.js
 * @version: v1.13.3
 * @author: Igor Escobar
 *
 * Created by Igor Escobar on 2012-03-10. Please report any bug at http://blog.igorescobar.com
 *
 * Copyright (c) 2012 Igor Escobar http://blog.igorescobar.com
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* jshint laxbreak: true */
/* global define, jQuery, Zepto */

'use strict';

// UMD (Universal Module Definition) patterns for JavaScript modules that work everywhere.
// https://github.com/umdjs/umd/blob/master/jqueryPluginCommonjs.js
(function (factory) {

    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery || Zepto);
    }

}(function ($) {

    var Mask = function (el, mask, options) {
        el = $(el);

        var jMask = this, oldValue = el.val(), regexMask;

        mask = typeof mask === 'function' ? mask(el.val(), undefined, el,  options) : mask;

        var p = {
            invalid: [],
            getCaret: function () {
                try {
                    var sel,
                        pos = 0,
                        ctrl = el.get(0),
                        dSel = document.selection,
                        cSelStart = ctrl.selectionStart;

                    // IE Support
                    if (dSel && navigator.appVersion.indexOf('MSIE 10') === -1) {
                        sel = dSel.createRange();
                        sel.moveStart('character', el.is('input') ? -el.val().length : -el.text().length);
                        pos = sel.text.length;
                    }
                    // Firefox support
                    else if (cSelStart || cSelStart === '0') {
                        pos = cSelStart;
                    }

                    return pos;
                } catch (e) {}
            },
            setCaret: function(pos) {
                try {
                    if (el.is(':focus')) {
                        var range, ctrl = el.get(0);

                        if (ctrl.setSelectionRange) {
                            ctrl.setSelectionRange(pos,pos);
                        } else if (ctrl.createTextRange) {
                            range = ctrl.createTextRange();
                            range.collapse(true);
                            range.moveEnd('character', pos);
                            range.moveStart('character', pos);
                            range.select();
                        }
                    }
                } catch (e) {}
            },
            events: function() {
                el
                .on('input.mask keyup.mask', p.behaviour)
                .on('paste.mask drop.mask', function() {
                    setTimeout(function() {
                        el.keydown().keyup();
                    }, 100);
                })
                .on('change.mask', function(){
                    el.data('changed', true);
                })
                .on('blur.mask', function(){
                    if (oldValue !== el.val() && !el.data('changed')) {
                        el.triggerHandler('change');
                    }
                    el.data('changed', false);
                })
                // it's very important that this callback remains in this position
                // otherwhise oldValue it's going to work buggy
                .on('blur.mask', function() {
                    oldValue = el.val();
                })
                // select all text on focus
                .on('focus.mask', function (e) {
                    if (options.selectOnFocus === true) {
                        $(e.target).select();
                    }
                })
                // clear the value if it not complete the mask
                .on('focusout.mask', function() {
                    if (options.clearIfNotMatch && !regexMask.test(p.val())) {
                       p.val('');
                   }
                });
            },
            getRegexMask: function() {
                var maskChunks = [], translation, pattern, optional, recursive, oRecursive, r;

                for (var i = 0; i < mask.length; i++) {
                    translation = jMask.translation[mask.charAt(i)];

                    if (translation) {

                        pattern = translation.pattern.toString().replace(/.{1}$|^.{1}/g, '');
                        optional = translation.optional;
                        recursive = translation.recursive;

                        if (recursive) {
                            maskChunks.push(mask.charAt(i));
                            oRecursive = {digit: mask.charAt(i), pattern: pattern};
                        } else {
                            maskChunks.push(!optional && !recursive ? pattern : (pattern + '?'));
                        }

                    } else {
                        maskChunks.push(mask.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'));
                    }
                }

                r = maskChunks.join('');

                if (oRecursive) {
                    r = r.replace(new RegExp('(' + oRecursive.digit + '(.*' + oRecursive.digit + ')?)'), '($1)?')
                         .replace(new RegExp(oRecursive.digit, 'g'), oRecursive.pattern);
                }

                return new RegExp(r);
            },
            destroyEvents: function() {
                el.off(['input', 'keydown', 'keyup', 'paste', 'drop', 'blur', 'focusout', ''].join('.mask '));
            },
            val: function(v) {
                var isInput = el.is('input'),
                    method = isInput ? 'val' : 'text',
                    r;

                if (arguments.length > 0) {
                    if (el[method]() !== v) {
                        el[method](v);
                    }
                    r = el;
                } else {
                    r = el[method]();
                }

                return r;
            },
            getMCharsBeforeCount: function(index, onCleanVal) {
                for (var count = 0, i = 0, maskL = mask.length; i < maskL && i < index; i++) {
                    if (!jMask.translation[mask.charAt(i)]) {
                        index = onCleanVal ? index + 1 : index;
                        count++;
                    }
                }
                return count;
            },
            caretPos: function (originalCaretPos, oldLength, newLength, maskDif) {
                var translation = jMask.translation[mask.charAt(Math.min(originalCaretPos - 1, mask.length - 1))];

                return !translation ? p.caretPos(originalCaretPos + 1, oldLength, newLength, maskDif)
                                    : Math.min(originalCaretPos + newLength - oldLength - maskDif, newLength);
            },
            behaviour: function(e) {
                e = e || window.event;
                p.invalid = [];
                var keyCode = e.keyCode || e.which;
                if ($.inArray(keyCode, jMask.byPassKeys) === -1) {

                    var caretPos = p.getCaret(),
                        currVal = p.val(),
                        currValL = currVal.length,
                        changeCaret = caretPos < currValL,
                        newVal = p.getMasked(),
                        newValL = newVal.length,
                        maskDif = p.getMCharsBeforeCount(newValL - 1) - p.getMCharsBeforeCount(currValL - 1);

                    p.val(newVal);

                    // change caret but avoid CTRL+A
                    if (changeCaret && !(keyCode === 65 && e.ctrlKey)) {
                        // Avoid adjusting caret on backspace or delete
                        if (!(keyCode === 8 || keyCode === 46)) {
                            caretPos = p.caretPos(caretPos, currValL, newValL, maskDif);
                        }
                        p.setCaret(caretPos);
                    }

                    return p.callbacks(e);
                }
            },
            getMasked: function(skipMaskChars) {
                var buf = [],
                    value = p.val(),
                    m = 0, maskLen = mask.length,
                    v = 0, valLen = value.length,
                    offset = 1, addMethod = 'push',
                    resetPos = -1,
                    lastMaskChar,
                    check;

                if (options.reverse) {
                    addMethod = 'unshift';
                    offset = -1;
                    lastMaskChar = 0;
                    m = maskLen - 1;
                    v = valLen - 1;
                    check = function () {
                        return m > -1 && v > -1;
                    };
                } else {
                    lastMaskChar = maskLen - 1;
                    check = function () {
                        return m < maskLen && v < valLen;
                    };
                }

                while (check()) {
                    var maskDigit = mask.charAt(m),
                        valDigit = value.charAt(v),
                        translation = jMask.translation[maskDigit];

                    if (translation) {
                        if (valDigit.match(translation.pattern)) {
                            buf[addMethod](valDigit);
                             if (translation.recursive) {
                                if (resetPos === -1) {
                                    resetPos = m;
                                } else if (m === lastMaskChar) {
                                    m = resetPos - offset;
                                }

                                if (lastMaskChar === resetPos) {
                                    m -= offset;
                                }
                            }
                            m += offset;
                        } else if (translation.optional) {
                            m += offset;
                            v -= offset;
                        } else if (translation.fallback) {
                            buf[addMethod](translation.fallback);
                            m += offset;
                            v -= offset;
                        } else {
                          p.invalid.push({p: v, v: valDigit, e: translation.pattern});
                        }
                        v += offset;
                    } else {
                        if (!skipMaskChars) {
                            buf[addMethod](maskDigit);
                        }

                        if (valDigit === maskDigit) {
                            v += offset;
                        }

                        m += offset;
                    }
                }

                var lastMaskCharDigit = mask.charAt(lastMaskChar);
                if (maskLen === valLen + 1 && !jMask.translation[lastMaskCharDigit]) {
                    buf.push(lastMaskCharDigit);
                }

                return buf.join('');
            },
            callbacks: function (e) {
                var val = p.val(),
                    changed = val !== oldValue,
                    defaultArgs = [val, e, el, options],
                    callback = function(name, criteria, args) {
                        if (typeof options[name] === 'function' && criteria) {
                            options[name].apply(this, args);
                        }
                    };

                callback('onChange', changed === true, defaultArgs);
                callback('onKeyPress', changed === true, defaultArgs);
                callback('onComplete', val.length === mask.length, defaultArgs);
                callback('onInvalid', p.invalid.length > 0, [val, e, el, p.invalid, options]);
            }
        };


        // public methods
        jMask.mask = mask;
        jMask.options = options;
        jMask.remove = function() {
            var caret = p.getCaret();
            p.destroyEvents();
            p.val(jMask.getCleanVal());
            p.setCaret(caret - p.getMCharsBeforeCount(caret));
            return el;
        };

        // get value without mask
        jMask.getCleanVal = function() {
           return p.getMasked(true);
        };

       jMask.init = function(onlyMask) {
            onlyMask = onlyMask || false;
            options = options || {};

            jMask.byPassKeys = $.jMaskGlobals.byPassKeys;
            jMask.translation = $.jMaskGlobals.translation;

            jMask.translation = $.extend({}, jMask.translation, options.translation);
            jMask = $.extend(true, {}, jMask, options);

            regexMask = p.getRegexMask();

            if (onlyMask === false) {

                if (options.placeholder) {
                    el.attr('placeholder' , options.placeholder);
                }

                // this is necessary, otherwise if the user submit the form
                // and then press the "back" button, the autocomplete will erase
                // the data. Works fine on IE9+, FF, Opera, Safari.
                if ('oninput' in $('input')[0] === false && el.attr('autocomplete') === 'on') {
                  el.attr('autocomplete', 'off');
                }

                p.destroyEvents();
                p.events();

                var caret = p.getCaret();
                p.val(p.getMasked());
                p.setCaret(caret + p.getMCharsBeforeCount(caret, true));

            } else {
                p.events();
                p.val(p.getMasked());
            }
        };

        jMask.init(!el.is('input'));
    };

    $.maskWatchers = {};
    var HTMLAttributes = function () {
            var input = $(this),
                options = {},
                prefix = 'data-mask-',
                mask = input.attr('data-mask');

            if (input.attr(prefix + 'reverse')) {
                options.reverse = true;
            }

            if (input.attr(prefix + 'clearifnotmatch')) {
                options.clearIfNotMatch = true;
            }

            if (input.attr(prefix + 'selectonfocus') === 'true') {
               options.selectOnFocus = true;
            }

            if (notSameMaskObject(input, mask, options)) {
                return input.data('mask', new Mask(this, mask, options));
            }
        },
        notSameMaskObject = function(field, mask, options) {
            options = options || {};
            var maskObject = $(field).data('mask'),
                stringify = JSON.stringify,
                value = $(field).val() || $(field).text();
            try {
                if (typeof mask === 'function') {
                    mask = mask(value);
                }
                return typeof maskObject !== 'object' || stringify(maskObject.options) !== stringify(options) || maskObject.mask !== mask;
            } catch (e) {}
        };


    $.fn.mask = function(mask, options) {
        options = options || {};
        var selector = this.selector,
            globals = $.jMaskGlobals,
            interval = $.jMaskGlobals.watchInterval,
            maskFunction = function() {
                if (notSameMaskObject(this, mask, options)) {
                    return $(this).data('mask', new Mask(this, mask, options));
                }
            };

        $(this).each(maskFunction);

        if (selector && selector !== '' && globals.watchInputs) {
            clearInterval($.maskWatchers[selector]);
            $.maskWatchers[selector] = setInterval(function(){
                $(document).find(selector).each(maskFunction);
            }, interval);
        }
        return this;
    };

    $.fn.unmask = function() {
        clearInterval($.maskWatchers[this.selector]);
        delete $.maskWatchers[this.selector];
        return this.each(function() {
            var dataMask = $(this).data('mask');
            if (dataMask) {
                dataMask.remove().removeData('mask');
            }
        });
    };

    $.fn.cleanVal = function() {
        return this.data('mask').getCleanVal();
    };

    $.applyDataMask = function(selector) {
        selector = selector || $.jMaskGlobals.maskElements;
        var $selector = (selector instanceof $) ? selector : $(selector);
        $selector.filter($.jMaskGlobals.dataMaskAttr).each(HTMLAttributes);
    };

    var globals = {
        maskElements: 'input,td,span,div',
        dataMaskAttr: '*[data-mask]',
        dataMask: true,
        watchInterval: 300,
        watchInputs: true,
        watchDataMask: false,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            '0': {pattern: /\d/},
            '9': {pattern: /\d/, optional: true},
            '#': {pattern: /\d/, recursive: true},
            'A': {pattern: /[a-zA-Z0-9]/},
            'S': {pattern: /[a-zA-Z]/}
        }
    };

    $.jMaskGlobals = $.jMaskGlobals || {};
    globals = $.jMaskGlobals = $.extend(true, {}, globals, $.jMaskGlobals);

    // looking for inputs with data-mask attribute
    if (globals.dataMask) { $.applyDataMask(); }

    setInterval(function(){
        if ($.jMaskGlobals.watchDataMask) { $.applyDataMask(); }
    }, globals.watchInterval);
}));

(function() {
  //mascaras de formularios
  $('.phone').mask('(00) 0000-0000');
  $('.birthday').mask('00/00/0000');
})();

//botão upload de arquivo
(function() {
  $('.btn-attach').click(function() {
    
    var field = $(this).siblings('.attach-name'),
        inputFile = $(this).siblings('input[type="file"]');
    
    inputFile.trigger('click')
    .change(function() {
      field.text(inputFile.val());
    });

  });
})();

//renderizar botões de compartilhamento após chamada ajax
$( document ).ajaxComplete(function() {
  twttr.widgets.load();
  try{
      FB.XFBML.parse(); 
  }catch(ex){};
  gapi.plusone.go();
});

/**
 * ENVIAR NEWSLETTER
 */
(function() {
  $('.send-newsletter').on('click', function() {
    var mail = $(this).parents('form').find('input[name="email"]').val();
    if(mail != '') {
      console.log(mail);
    } else {
      alert('É necessário digitar seu email');
    }
  });
})();

/**
 * Seja revendedor
 */
(function() {
  $(".choose-people").on('click',function(e) {
    $(this).addClass('active')
    .parents('div')
    .siblings('div')
    .find('span').removeClass('active');

    var txt = $(this).text();
    $('input[name="pessoa"]','#contact-form').val(txt);
  });

  $('li','#faq-list').on('click',function() {
    $(this).toggleClass('active')
    .siblings('li')
    .removeClass('active');
  });

  //scrolls
  $('.button','#revendedor-header').on('click',function(e) {
    e.preventDefault();
    
    var toScroll = $('#contact-form').eq(0).offset().top, body = $("html, body");

    body.stop().animate({scrollTop:toScroll}, '500', 'swing', function() { 
       $('#contact-form').eq(0).find('input[name="nome"]').focus();
    });
  });

  $('.jump-questions').on('click',function() {
    var toScroll = $('#faq-list').eq(0).offset().top, body = $("html, body");

    body.stop().animate({scrollTop:toScroll}, '500', 'swing');
  });
})();

/**
 * Slider campanha
 */
(function() {

    var reloadActiveThumb = function() {
      var i = $(".owl-wrapper > .active","#campaing-items").index();
      $(".owl-item","#caroulsel-items").eq(i).find('figure').addClass('active')
      .parents('.owl-item')
      .siblings('.owl-item')
      .find('figure').removeClass('active');
    }

    //iniciar slider
    var partners = $("#campaing-items");
    partners.owlCarousel({
        responsiveBaseWidth: $(".row"),
        responsive: true,
        pagination: false,
        itemsCustom: [
          [0,1]
        ],
        slideSpeed: 500,
        //rewindNav: false,
        rewindSpeed: 300,
        addClassActive: true,
        afterAction: reloadActiveThumb
    });

    //iniciar carrossel
    var carousel = $("#caroulsel-items");
    carousel.owlCarousel({
        responsive: true,
        responsiveBaseWidth: $("#campaing-carousel"),
        pagination: false,
        itemsCustom: [
            [200, 3],
            [700, 6],
            [800, 10],
        ],
        //rewindNav: false,
        rewindSpeed: 300,
    });

    //instancias
    var owl = $("#campaing-items").data('owlCarousel'), car = $("#caroulsel-items").data('owlCarousel');

    //navegação do slider
    $(".prev-campaing").click(function(e){
        e.preventDefault();
        partners.trigger('owl.next');

        reloadActiveThumb();
    });

    $(".next-campaing").click(function(e){
        e.preventDefault();
        partners.trigger('owl.prev');

        reloadActiveThumb();
    });

    $('.owl-item','#caroulsel-items').each(function(i) {
      if(i == 0)
        $('figure',this).addClass('active');

      $('figure',this).on('click',function() {
        $(this).addClass('active')
        .parents('div').siblings('div')
        .find('figure').removeClass('active');

        owl.goTo(i);
      });

    });
})();

//input select custom
$('.icon-chevron-thin-down','.custom-select').on('click',function() {
  $(this).siblings('span.deactive').toggleClass('active');
});
$('span','.custom-select').on('click',function() {
  var $this = $(this);
  $(this).removeClass('deactive')
  .addClass('active')
  .siblings('span').removeClass('active')
  .addClass('deactive');

  var dt = $(this).data('showgenero');

  $('nav','.list-meters').each(function(index, el) {
    if($(this).data('genero') == dt)
      $(this).addClass('active').siblings('nav').removeClass('active');
  });
});

//pedir presente
(function() {
  $('.form-gift').on('submit',function(e) {
    e.preventDefault();
    $.ajax({
      url: getData.ajaxDir,
      type: "GET",
      dataType: "html",
      data: {
        action: 'send_gift_from_friend',
        form_data: $(this).serialize()
      },
      beforeSend: function() {
        $('.load-gift').addClass('active');
      },
      complete: function() {
        $('.load-gift').removeClass('active');
      },
      success: function(data) {
        if(data == 'true') {
          alert('E-mail enviado com sucesso');
        } else {
          alert('Ocorreu algum erro durante o envio. Tente novamente.');
        }
      }
    });
  });
})();


<?php
global $modabiz_option;
get_header();
	?>
	
	<section id="query-blog" class="small-12 left rel">
		<header id="blog-header" class="small-12 left d-table rel" data-thumb="<?php echo $modabiz_option['blog-header-bg']['url']; ?>">
			<div class="black-mask"></div>
			<div class="small-12 d-table-cell">
				<hgroup class="small-12 no-margin left text-center">
					<h1 class="text-up">Blog</h1>
					<?php if(!empty($modabiz_option['blog-header-desc'])) echo "<h5 class=\"text-low\">{$modabiz_option['blog-header-desc']}</h5>"; ?>
				</hgroup>
			</div>
		</header>
		<div class="row">
			<div class="small-12 medium-8 left section-block">
				<nav id="content" class="small-12 columns">
					<?php
	              		if (have_posts()) : while (have_posts()) : the_post();
	              		global $post;
	              		$comments_count = wp_count_comments();
	            	?>
					<article class="small-12 left post-item post">
						<header>
							<p class="font-small"><time pubdate><i class="icon-calendar3"></i> <?php echo the_time('d \d\e F, Y'); ?></time></p>
							
							<h1 class="divide-20"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
							
							<nav class="post-item-share small-12 left">
								<ul class="inline-list d-iblock">
						          <li><div class="fb-like" data-layout="button_count" data-href="<?php the_permalink(); ?>"></div></li>
						          <li><a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>">Tweet</a></li>
						          <li><div class="g-plusone" data-size="medium" data-width="65" data-href="<?php the_permalink(); ?>"></div></li>
						        </ul>
							</nav>

						</header>
						<?php
							$thumb = getThumbUrl('large',$post->ID);
							$video = get_field('post_video');
							$link = get_permalink($post->ID);
							if(!empty($thumb) || !empty($video)) {
								echo "<figure class=\"divide-30\">";
								if(!empty($video))
									echo '<div class="flex-video no-margin">' . $video . '</div>';
								else
									echo "<a href=\"{$link}\"><img src=\"{$thumb}\"></a>";
								echo "</figure>";
							}
						?>

						
						<p class="divide-30 post-author text-up">
							<strong>Por: <?php echo the_author_meta( 'first_name' ); ?></strong>
						</p>

						<nav class="small-12 left post-tags">
							<ul class="inline-list font-bold d-iblock">
				            <?php
				              /*
				                Tags
				               */
				              if(get_the_tags()):
				                $posttags = get_the_tags();
				                foreach ($posttags as $tag):
				                  $tag_link = get_tag_link($tag->term_id);
				                ?>
				                <li><h5><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></h5></li>
				                <?php 
				                    endforeach;
				                  endif;
				                ?>
				            </ul>
						</nav>

						<p class="divide-30"><?php the_excerpt(); ?></p>

						<footer class="small-12 left">
							
							<p class="no-margin small-6 left"><a href="<?php the_permalink(); ?>#disqus_thread"><i class="icon-bubble"></i> <?php comments_number( 'Sem comentários', '1 comentário', '% comentários' ); ?></a></p>

							<p class="no-margin text-up small-6 left"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><strong>Continuar lendo</strong> <span class="icon-chevron-thin-right"></span></a></p>
							
							
						</footer>
					</article>
					<?php endwhile; endif; ?>
				</nav>
			</div>

			<?php get_sidebar(); ?>

			<footer id="nav-below" class="small-12 left text-center hide">
                <div class="divide-20"></div>
                <?php
                  global $wp_query;

                  $big = 999999999; // need an unlikely integer

                  echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total' => $wp_query->max_num_pages,
                    'next_text'    => '&raquo;',
                    'prev_text'    => '&laquo;',
                    'type'         => 'list',
                  ) );
                ?>
            </footer>
		</div>
	</section>

<?php

get_template_part( 'content' , 'newsletter' );
get_footer();

?>

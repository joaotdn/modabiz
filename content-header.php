<header class="small-12 columns content-header">
	<section class="small-12 left section-block">
		<h1 class="small-12 medium-6 left no-margin font-bold"><?php the_title(); ?></h1>

		<nav class="right small-12 medium-6 left show-for-medium-up share-footer small-text-center medium-text-right">
			<ul class="inline-list d-iblock no-margin">
	          <li><div class="fb-like" data-layout="button_count" data-href="<?php the_permalink(); ?>"></div></li>
	          <li><a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>">Tweet</a></li>
	          <li><div class="g-plusone" data-size="medium" data-width="65" data-href="<?php the_permalink(); ?>"></div></li>
	        </ul>
		</nav>
	</section>
</header>
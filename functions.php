<?php
/**
 * Funções para a plataforma ModaBiz
 *
 * Neste arquivo contém as funções e classes principais que configuram
 * a aplicação.
 *
 * @link https://bitbucket.org/joaotdn/modabiz
 *
 * Link para API
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage ModaBiz
 * @since ModaBiz 1.0
 * @author https://bitbucket.org/joaotdn/
 */

//Endereço do ModaBiz
define('MODABIZ_URL','http://www.modabiz.com.br');

//Versão do tema (RELEASES)
define('THEME_VERSION', '1.0.1');

//Icone do tema
define('THEME_ICON', get_stylesheet_directory_uri() . '/images/icon.png');

add_action( 'init', 'jt_remove_default_widgets_action' );
/**
 * Removes all default widgets before they have a chance to get registered.
 * @since ModaBiz 1.0
 */
function jt_remove_default_widgets_action() {

	remove_action( 'init', 'wp_widgets_init', 1 );

}

/**
 * Utilitários
 */
require_once (dirname(__FILE__) . '/inc/utils/Utilities.class.php');

/**
* Configure funções para campos personalizados da aplicação
*/
define( 'USE_LOCAL_ACF_CONFIGURATION', true ); // apenas conf. local

add_filter('acf/settings/path', 'modabiz_acf_path');

function modabiz_acf_path( $path ) {

	   // update path
	   $path = get_stylesheet_directory() . '/inc/acf/';

	   // return
	   return $path;

}

add_filter('acf/settings/dir', 'modabiz_acf_dir');

function modabiz_acf_dir( $dir ) {

	   // update path
	   $dir = get_stylesheet_directory_uri() . '/inc/acf/';

	   // return
	   return $dir;

}

/**
 * Framework para personalização de campos
 * (custom meta post)
 */
include_once( get_stylesheet_directory() . '/inc/acf/acf.php' );
include_once( get_stylesheet_directory() . '/inc/acf-repeater/acf-repeater.php' );
define( 'ACF_LITE' , true );
include_once( get_stylesheet_directory() . '/inc/acf/preconfig.php' );

/**
 * Infinite scroll
 */
include_once( get_stylesheet_directory() . '/inc/infinite-scroll/infinite-scroll.php' );

if ( ! function_exists( 'modabiz_setup' ) ) :
/**
 * Esta função será chamada logo após a inicialização da aplicação
 *
 * @since ModaBiz 1.0
 */
function modabiz_setup() {

	//Thumbnails
	add_theme_support('post-thumbnails');
        
    if (function_exists('add_image_size')) {
        add_image_size('blog-component', 303, 415, true);
    }

	/**
	 * Registre os menus do topo e rodapé
	 */
	register_nav_menus( array(
		'main_menu' => __( 'Menu topo',   'modabiz' ),
		'footer_menu'  => __( 'Menu rodape', 'modabiz' ),
	) );

	// Muda o nome da classe de submenu nativa
	  function change_submenu_class($menu) {
	      $menu = preg_replace('/ class="sub-menu"/', '/ class="submenu" /', $menu);
	      return $menu;
	  }
	  add_filter('wp_nav_menu', 'change_submenu_class');

	/*
		Remova widgets padrões do wordpress
	 */
	function remove_dashboard_widgets() {
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
		remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
		remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
		remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
		remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
		remove_meta_box('dashboard_primary', 'dashboard', 'side');
		remove_meta_box('dashboard_secondary', 'dashboard', 'side');
		remove_meta_box('dashboard_activity', 'dashboard', 'normal');
		remove_meta_box('dashboard_welcome', 'dashboard', 'normal');
	}
	add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

	/**
	 * Páginas padrões da aplicação
	 */
	$page = get_page_by_title('Contato');
	if(!isset($page)) {
		$modaUtils = new ModaBizUtils();
		$modaUtils->registerInitPage('A Marca', '');
		$modaUtils->registerInitPage('Coleção', '');
		$modaUtils->registerInitPage('Seja revendedor', '', 0,'template.seja-revendedor.php');
		$modaUtils->registerInitPage('Trabalhe conosco', '', 0,'template.trabalhe-conosco.php');
		$modaUtils->registerInitPage('Lojas e revendas', '', 0,'template.lojas-revendas.php');
		$modaUtils->registerInitPage('Contato', '', 0,'template.fale-conosco.php');

		//Páginas filhas
		$colecao = get_page_by_title('Coleção');
		$modaUtils->registerInitPage('Masculino', '',$colecao->ID);
		$modaUtils->registerInitPage('Feminino', '',$colecao->ID);
	}

	/**
	 * Renomear categoria inicial para blog
	 */
	wp_update_term(1, 'category', array(
	  'name' => 'Blog',
	  'slug' => 'blog'
	));
	
	// remove paragrafo em resumos
    remove_filter('the_excerpt', 'wpautop');
    //limita tamanho dos resumos
    function new_excerpt_length($length) {
		return 80;
	}
	add_filter('excerpt_length', 'new_excerpt_length');

}
add_action('init','modabiz_setup');
endif;

/**
 * Incorpore scripts essenciais para toda a
 * aplicação
 *
 * @since ModaBiz 1.0
 */
function modabiz_scripts() {
	if(is_single()) {
		wp_enqueue_style('blogposts_style', get_stylesheet_directory_uri() . '/components/layers/css/blog.block.css', array(), THEME_VERSION, "screen");
	}
	/*
		Folha de estilo base para a aplicação
	 */
	wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array(), THEME_VERSION, "screen");

    /*
    	modernizr
    */
    wp_enqueue_script('modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), THEME_VERSION);

 /**
     * AangularJS
     */
    wp_enqueue_script('angularjs', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js', array(), THEME_VERSION, true);


    /*
    	Jquery
     */
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', array(), THEME_VERSION, true);
    wp_enqueue_script('jquery');

   
    /*
	    Scripts essenciais minificados em
	    um arquivo unico e essenciais para
	    o funcionamento do lado cliente
    */
    wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/scripts.js', array(), THEME_VERSION, true);

}
add_action( 'wp_enqueue_scripts', 'modabiz_scripts' );

/**
 * Incorpore scripts essenciais
 * apenas para o admin
 *
 * @since ModaBiz 1.0
 */
function add_menu_icons_styles() {
	?>
		<style>
			#menu-posts-locations div.wp-menu-image:before {
				content: "\f230";
			}
			#menu-posts-panel div.wp-menu-image:before {
				content: "\f181";
			}
			#menu-posts-lookbook div.wp-menu-image:before {
				content: "\f306";
			}
			
			input[id^="grid-photos-url"],
			input[id^="testimonials-items-subtitle"],
			input[id^="faq-items-subtitle"],
			input[id^="faq-items-url"],
			#modabiz_option-faq-items .upload_button_div,
			input[id^="grid-photos-subtitle"] {
				display: none !important;
			}
			.mb-widget-label {color: darkgray;padding: 10px;background-color: whitesmoke;border-bottom: 1px solid darkgray;}		
		</style>

		<script>
      //<![CDATA[
      var getData = {
        'urlDir':'<?php bloginfo('template_directory');?>/',
        'ajaxDir':'<?php echo stripslashes(get_admin_url()).'admin-ajax.php';?>',
      }
      //]]>
    </script>
	<?php
	wp_enqueue_script('admin-jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', array(), THEME_VERSION, true);
	wp_enqueue_script('admin-scripts', get_stylesheet_directory_uri() . '/admin-scripts.js', array(), THEME_VERSION, true);
}
add_action('admin_head', 'add_menu_icons_styles');


// PostTypes
// ------------------------------------------------------------------------------------

/**
 * Localizações
 * (CPT)
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/post-types/locais.php');

/**
 * lookbook
 * (CPT)
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/post-types/lookbook.php');

/**
 * campanha
 * (CPT)
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/post-types/campanha.php');

// Opções do tema
// ------------------------------------------------------------------------------------

/**
 * Opções gerais para a aplicação e seus
 * componentes
 * @link https://github.com/reduxframework/redux-framework
 *
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/inc/redux/redux-framework.php');
require_once (dirname(__FILE__) . '/inc/redux/sample/barebones-config.php');

/**
 * inicio da injeção de camadas
 * ------------------------------------------------------------------------------------
 */

/**
 * Painel
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/panel.layer.php');

/**
 * Gmap
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/gmap.layer.php');

/**
 * Instagram Hashtag
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/instagram.hashtag.layer.php');

/**
 * Instagram Profile
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/instagram.profile.layer.php');

/**
 * Blog
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/blog.layer.php');

/**
 * Galeria
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/gallery.layer.php');

/**
 * Seja revendedor
 * @since ModaBiz 1.0
 */
require_once (dirname(__FILE__) . '/components/layers/smart.layer.php');

/**
 * Registrar camadas
 */
add_action('widgets_init',function() {
	//Painel
	register_widget( 'Panel_Widget' );

	//Google maps
	register_widget( 'Gmap_Widget' );

	//Instagram Hashtags
	register_widget( 'Instagram_Hash_Widget' );

	//Instagram Hashtags
	register_widget( 'Instagram_Profile_Widget' );

	//Blog
	register_widget( 'Blog_Widget' );

	//Galeria
	register_widget( 'Gallery_Widget' );

	//Seja revendedor
	register_widget( 'Smart_Widget' );
});

/**
 * fim da injeção de camadas
 * -------------------------------------------------------------------------------------
 */

/**
 * Registro de locais para camadas (Sidebars)
 * @since ModaBiz 1.0
 */
function layers_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Página principal', 'modabiz' ),
        'id' => 'home-layers',
        'description' => __( 'Camadas da página principal', 'modabiz' ),
        'before_widget' => '<section class="small-12 left layer-block">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Fale conosco', 'modabiz' ),
        'id' => 'contact-layers',
        'description' => __( 'Camadas do template fale conosco', 'modabiz' ),
        'before_widget' => '<section class="small-12 left layer-block">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
    ) );
}
add_action('widgets_init','layers_widgets_init');

/**
 * Scripts sob demanda para componentes ativados
 */
function insertJSComponents() {
	global $modabiz_option;

	//Google Maps ativado
	if ( is_active_widget( false, false, 'gmap_widget', true ) ) {
		$api_gmap = $modabiz_option['gmap-api']; // credencial no gmap
		echo "<script src='https://maps.googleapis.com/maps/api/js?key=". $api_gmap ."&sensor=false'></script>";
	}

}
add_action('wp_head','insertJSComponents');

// Funções para administrar componentes
// ------------------------------------------------------------------------------------

// LOCALIZAÇÕES
//******************************************************

/**
 * Ao cadastrar um mapa escreva as informações no arquivo
 * json para consultas no lado cliente
 */
function save_gmap_meta() {

	$args = array(
	    'post_type' => 'locations',
	    'post_status' => 'publish',
	    'nopaging' => true
	);
	$query = new WP_Query( $args );
	$posts = $query->get_posts();

	$output = array();

	foreach( $posts as $post ) {
		$location = get_field('gmap_latlng', $post->ID);

	    $output[] = array(
	    	'id' => $post->ID,
	    	'titulo' => $post->post_title,
	    	'nome' => get_field('gmap_nome',$post->ID),
	    	'tipo' => get_field('gmap_tipo',$post->ID),
	    	'endereco' => get_field('gmap_endereco',$post->ID),
	    	'cep' => get_field('gmap_cep',$post->ID),
	    	'uf' => get_field('gmap_uf',$post->ID),
	    	'horarios' => get_field('gmap_horarios',$post->ID),
	    	'telefones' => get_field('gmap_telefones',$post->ID),
	    	'whatsapp' => get_field('gmap_whatsapp',$post->ID),
	    	'facebook' => get_field('gmap_facebook',$post->ID),
	    	'instagram' => get_field('gmap_instagram',$post->ID),
	    	'email' => get_field('gmap_email',$post->ID),
	    	'endereco_formatado' => get_field('gmap_nome',$post->ID) . " - " . get_field('gmap_uf',$post->ID),
	    	'lat' => $location['lat'],
	    	'lng' => $location['lng'],
	    );
	}

	$f = fopen(dirname(__FILE__) . '/places.json', 'w');
	$txt = json_encode($output);
	fwrite($f, $txt);
	fclose($f);

}
add_action( 'save_post', 'save_gmap_meta' );

// INSTAGRAM
//******************************************************

/**
 * Funciona na hashtag. Deve retornar o vetor de imagens
 * com as imagens mais recentes relacionadas a hash digitada
 */
add_action('wp_ajax_nopriv_redux_hashtag_typeahead', 'redux_hashtag_typeahead');
add_action('wp_ajax_redux_hashtag_typeahead', 'redux_hashtag_typeahead');

function redux_hashtag_typeahead() {
	global $modabiz_option;
	$value = $_REQUEST['thisValue'];
	$j = 0;

	$var = file_get_contents('https://api.instagram.com/v1/tags/'. $value .'/media/recent?access_token='. $modabiz_option['instagram-token']);
	$var = json_decode($var);
	foreach($var->data as $user) {
		?>
		<li class="inst-th">
			<figure class="small-12 left rel">
				<img src="<?php echo $var->data[$j]->images->thumbnail->url; ?>" alt="<?php echo $var->data[$j]->user->username; ?>" />
			</figure>
			<a href="#" class="unthis" target="_blank"></a>
			<a href="<?php echo $var->data[$j]->link; ?>" class="el el-instagram" target="_blank"></a>
		</li>
		<?php
		$j++;
	}

	exit();
}

/**
 * Atualize as imagens da hashtag
 */
add_action('wp_ajax_nopriv_store_hash_imgs', 'store_hash_imgs');
add_action('wp_ajax_store_hash_imgs', 'store_hash_imgs');

function store_hash_imgs() {
	global $modabiz_option;
	$obj = $_REQUEST['obj'];
	$len = count($obj);

	//Verifique se a opção existe
	if( !get_option( 'hashtag_object' ) ) {
		add_option( 'hashtag_object', $obj );
		exit();
	}

	//verifique se a opção é válida
	if($len <= 0) {
		exit();
	} else {
		//Os dados da opção serão substituidos pelos dados enviados
		update_option( 'hashtag_object', $obj );
	}

	exit();
}

/**
 * Funciona no perfil Instagram. Deve retornar o vetor de imagens
 * com as imagens mais recentes relacionadas a hash digitada
 */
add_action('wp_ajax_nopriv_redux_profile_typeahead', 'redux_profile_typeahead');
add_action('wp_ajax_redux_profile_typeahead', 'redux_profile_typeahead');

function redux_profile_typeahead() {
	global $modabiz_option;
	$value = $_REQUEST['thisValue'];
	$j = 0;
	$var = file_get_contents('https://api.instagram.com/v1/users/'. $value .'/media/recent/?access_token='.$modabiz_option['instagram-token']);

	$var = json_decode($var);
	foreach($var->data as $user) {
		?>
		<li class="inst-th">
			<figure class="small-12 left rel">
				<img src="<?php echo $var->data[$j]->images->thumbnail->url; ?>" alt="<?php echo $var->data[$j]->user->username; ?>" />
			</figure>
			<a href="#" class="unthis" target="_blank"></a>
			<a href="<?php echo $var->data[$j]->link; ?>" class="el el-instagram" target="_blank"></a>
		</li>
		<?php
		$j++;
	}

	exit();
}

/**
 * Atualize as imagens do perfil
 */
add_action('wp_ajax_nopriv_store_profile_imgs', 'store_profile_imgs');
add_action('wp_ajax_store_profile_imgs', 'store_profile_imgs');

function store_profile_imgs() {
	global $modabiz_option;
	$obj = $_REQUEST['obj'];
	$len = count($obj);

	//Verifique se a opção existe
	if( !get_option( 'userprofile_object' ) ) {
		add_option( 'userprofile_object', $obj );
		exit();
	}

	//verifique se a opção é válida
	if($len <= 0) {
		exit();
	} else {
		//Os dados da opção serão substituidos pelos dados enviados
		update_option( 'userprofile_object', $obj );
	}

	exit();
}


//Pedir presente
add_action('wp_ajax_nopriv_send_gift_from_friend', 'send_gift_from_friend');
add_action('wp_ajax_send_gift_from_friend', 'send_gift_from_friend');

function send_gift_from_friend() {
	$data = $_GET['form_data'];
	$email_val = false;
	$msg_val = false;

	$params = array();
	parse_str($data, $params);
	
	if(array_key_exists('email', $params) && !empty($params['email'])) {
		$email = filter_var($params['email'],FILTER_VALIDATE_EMAIL);
		if($email)
			$email_val = true;
	}

	if(array_key_exists('mensagem', $params) && !empty($params['mensagem'])) {
		$mensagem = filter_var($params['mensagem'],FILTER_SANITIZE_STRING);
		if($mensagem && strlen($mensagem) < 700)
			$msg_val = true;
	}

	if($email_val && $msg_val) {
		$message = 'Alguém passou em ' . get_bloginfo('name') . ' e enviou um presente para você. Confira: ' . $params['link'];
		
		if(wp_mail( $params['email'], 'Um presente para você', $message )) {
			print('true');
			header('Location: ' . home_url('/' ));
		} else {
			print('false');
		}
	} else {
		print('false');
		exit();
	}

	exit();
}
?>

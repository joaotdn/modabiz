<?php 
/**
  * Template Name: Seja revendedor
  *
  * @package WordPress
  * @subpackage modabiz
  * @since ModaBiz 1.0
  */
 global $modabiz_option;
 $header_bg = $modabiz_option['revendedor-header-bg']['url'];

 if(isset($_POST['submited'])) {
 	$nome = filter_var($_POST['nome'],FILTER_SANITIZE_STRING);
 	$email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
 	$nascimento = filter_var($_POST['nascimento'],FILTER_SANITIZE_STRING);
 	$telefone = filter_var($_POST['tel'],FILTER_SANITIZE_STRING);
 	$ciduf = filter_var($_POST['ciduf'],FILTER_SANITIZE_STRING);
 	$endereco = filter_var($_POST['endereco'],FILTER_SANITIZE_STRING);
 	$bairro = filter_var($_POST['bairro'],FILTER_SANITIZE_STRING);
 	$complemento = filter_var($_POST['complemento'],FILTER_SANITIZE_STRING);
 	$file = (isset($_FILES)) ? $_FILES['anexo']['name'] : false;

 	//Verificar os campos obrigatórios
 	if($nome && !empty($nome))
 		$err_nome = true;
 	else
 		$err_nome = false;

 	if($telefone && !empty($telefone))
 		$err_telefone = true;
 	else
 		$err_telefone = false;

 	if($nascimento && !empty($nascimento))
 		$err_nascimento = true;
 	else
 		$err_nascimento = false;

 	if($email && !empty($email))
 		$err_email = true;
 	else
 		$err_email = false;

 	if($endereco && !empty($endereco))
 		$err_endereco = true;
 	else
 		$err_endereco = false;

 	if($bairro && !empty($bairro))
 		$err_bairro = true;
 	else
 		$err_bairro = false;

 	if($complemento && !empty($complemento))
 		$err_complemento = true;
 	else
 		$err_complemento = false;

 	if($ciduf && !empty($ciduf))
 		$err_ciduf = true;
 	else
 		$err_ciduf = false;
 	
 	//Anexo
 	if($file != false) {
 		if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
 		$uploadedfile = $_FILES['anexo'];
        $upload_overrides = array( 'test_form' => false );
        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
 	} else {
 		$err_anexo = false;
 	}

 	//Se todos os campos forem válidos, redirecione
 	if($err_nome && $err_telefone && $err_email && $err_mensagem && isset($movefile)) {
 		$msg = "Nome: " . $nome . "\n";
 		$msg .= "Email: " . $email . "\n";
 		$msg .= "Telefone: " . $telefone . "\n";
 		$msg .= "Nascimento: " . $nascimento . "\n";
 		$msg .= "Endereço: " . $endereco . "\n";
 		$msg .= "Bairro: " . $bairro . "\n";
 		$msg .= "Complemento: " . $complemento . "\n";
 		$msg .= "Complemento: " . $complemento . "\n";
 		$msg .= "Tipo: " . $_POST['pessoa'] . "\n";
 		$msg .= "Permitir envio de email: " . ($_POST['newsletter']) ? 'sim' : 'não' . "\n";
 		$msg .= "Permitir envio de SMS/Whatsapp: " . ($_POST['sms']) ? 'sim' : 'não' . "\n";

 		$attachments = $movefile[ 'file' ];

 		if($modabiz_option['page-revendedor-emails']) {
 			$to = explode(', ', $modabiz_option['page-revendedor-emails']);
 		} else {
 			$to = 'contato@modabiz.com.br';
 		}
 		
 		if(wp_mail( $to, '[Seja Revendedor] - Formulário do site', strip_tags($msg), $attachments)) {
 			wp_redirect('http://google.com');
 		} else {
 			$err_servidor = false;
 		}
 	}
 }
 get_header();
?>	
	<section class="small-12 left section-block">
		<div class="row">
			<?php 
				//Breadcrumb
				get_template_part( 'content' , 'breadcrumb' );

				//Cabeçalho da página
				get_template_part( 'content' , 'header' );
			?>
		</div>
		
		<!-- cabeçalho -->
		<header id="revendedor-header" class="small-12 left d-table rel" data-thumb="<?php echo $header_bg; ?>">
			<div class="black-mask"></div>
			<hgroup class="small-12 text-center d-table-cell rel">
				<h1 class="divide-30"><?php echo $modabiz_option['revendedor-header-title']; ?></h1>
				<p class="divide-30"><?php echo $modabiz_option['revendedor-header-subtitle']; ?></p>
				<p class="no-margin">
					<a href="#" class="button generic-btn text-up"><?php echo $modabiz_option['revendedor-header-btn']; ?></a>
				</p>
			</hgroup>
		</header>
		
		<div class="row">
			<!-- vantagens -->
			<section class="small-12 left revendedor-skills">
				
				<?php
					if(!empty($modabiz_option['revendedor-skills-title-1'])):
				?>
				<figure class="small-12 medium-4 columns text-center">
					<?php
						if(!empty($modabiz_option['revendedor-skills-icon-1']['url'])):
					?>
					<img src="<?php echo $modabiz_option['revendedor-skills-icon-1']['url']; ?>" alt="" class="d-iblock">
					<?php endif; ?>
					<div class="divide-30"></div>
					<figcaption class="small-12 left">
						<h4 class="divide-30"><?php echo $modabiz_option['revendedor-skills-title-1']; ?></h4>
						<p><?php echo $modabiz_option['revendedor-skills-desc-1']; ?></p>
					</figcaption>
				</figure>
				<?php endif; ?>

				<?php
					if(!empty($modabiz_option['revendedor-skills-title-2'])):
				?>
				<figure class="small-12 medium-4 columns text-center">
					<?php
						if(!empty($modabiz_option['revendedor-skills-icon-2']['url'])):
					?>
					<img src="<?php echo $modabiz_option['revendedor-skills-icon-2']['url']; ?>" alt="" class="d-iblock">
					<?php endif; ?>
					<div class="divide-30"></div>
					<figcaption class="small-12 left">
						<h4 class="divide-30"><?php echo $modabiz_option['revendedor-skills-title-2']; ?></h4>
						<p><?php echo $modabiz_option['revendedor-skills-desc-2']; ?></p>
					</figcaption>
				</figure>
				<?php endif; ?>

				<?php
					if(!empty($modabiz_option['revendedor-skills-title-3'])):
				?>
				<figure class="small-12 medium-4 columns text-center">
					<?php
						if(!empty($modabiz_option['revendedor-skills-icon-3']['url'])):
					?>
					<img src="<?php echo $modabiz_option['revendedor-skills-icon-3']['url']; ?>" alt="" class="d-iblock">
					<?php endif; ?>
					<div class="divide-30"></div>
					<figcaption class="small-12 left">
						<h4 class="divide-30"><?php echo $modabiz_option['revendedor-skills-title-3']; ?></h4>
						<p><?php echo $modabiz_option['revendedor-skills-desc-3']; ?></p>
					</figcaption>
				</figure>
				<?php endif; ?>

			</section>

			<!-- inicio do template -->
			<section class="small-12 left page-template">				
				<!-- formulario -->
				<div class="small-12 medium-6 columns">
					<header class="divide-30 text-center">
						<h3>Cadastre-se</h3>	
					</header>

					<nav class="divide-30">
						<div class="small-6 text-center left">
							<p><span class="choose-people active text-up">Pessoa física</span></p>
						</div>
						<div class="small-6 text-center left">
							<p><span class="choose-people text-up">Pessoa Jurídica</span></p>
						</div>
					</nav>

					<form action="<?php the_permalink(); ?>" class="small-12 left generic-form" id="contact-form" method="post" enctype="multipart/form-data">
						<?php if(isset($err_servidor) && !$err_servidor) echo "<p><span class=\"error\">Ocorreu algum erro interno. Tente novamente dentro de alguns instantes.</span></p>"; ?>
						<p>
							<input type="text" maxwidth="250" required name="nome" title="Nome completo" placeholder="NOME *">
							<?php if(isset($err_nome) && !$err_nome) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="text" required name="nascimento" class="birthday small-6 left pd-form" title="Seu telefone" placeholder="DATA DE NASCIMENTO *">
							<?php if(isset($err_nascimento) && !$err_nascimento) echo "<span class=\"error\">Campo obrigatório</span>"; ?>

							<input type="tel" required name="tel" class="phone small-6 right pd-form" title="Seu telefone" placeholder="TELEFONE *">
							<?php if(isset($err_telefone) && !$err_telefone) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="email" required name="email" title="Seu email" placeholder="E-MAIL *">
							<?php if(isset($err_email) && !$err_email) echo "<span class=\"error\">Campo obrigatório. E-mail inválido.</span>"; ?>
						</p>

						<p>
							<input type="text" required maxwidth="250" name="ciduf" title="ENDEREÇO" placeholder="ENDEREÇO *">
							<?php if(isset($err_endereco) && !$err_endereco) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="text" required name="bairro" class="small-6 left pd-form" title="Sua data de nascimento" placeholder="BAIRRO *">
							<?php if(isset($err_bairro) && !$err_bairro) echo "<span class=\"error\">Campo obrigatório</span>"; ?>

							<input type="text" required name="complemento" class="small-6 right pd-form" title="Seu complemento de endereço" placeholder="COMPLEMENTO *">
							<?php if(isset($err_complemento) && !$err_complemento) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<input type="text" required maxwidth="250" name="ciduf" title="Cidade e Estado" placeholder="CIDADE/ESTADO">
							<?php if(isset($err_ciduf) && !$err_ciduf) echo "<span class=\"error\">Campo obrigatório</span>"; ?>
						</p>

						<p>
							<button class="btn-attach no-margin text-up left generic-btn button">Comp. de residência</button>
							<span class="attach-name left ghost">Nenhum arquivo selecionado</span>
							<input type="file" name="anexo">
							<?php if(isset($err_anexo) && !$err_anexo) echo "<span class=\"error\">Campo obrigatório. Necessário enviar currículo.</span>"; ?>
						</p>
						
						<div class="divide-30"></div>
						<p class="divide-10"><label><input type="checkbox" name="newsletter"> <span>Permitir o envio de emails com promoções e novidades</span></label></p>
						<p><label><input type="checkbox" name="sms"> <span>Permitir o envio de mensagem via whatsapp ou SMS</span></label></p>
						<div class="divide-30"></div>

						<p class="text-right">
							<button type="submit" name="submited" class="small-12 generic-btn text-up no-margin button">Enviar e dar o primeiro passo</button>
						</p>
						<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
						<input type="hidden" name="pessoa" value="Pessoa Física">
					</form>
				</div>
				
				
				<div class="small-12 medium-6 columns contact-info">
					<?php
						//O que voce precisa
						$items = $modabiz_option['revendedor-multitext'];
						if(isset($items) && !empty($items)):
					?>
					<header class="divide-30">
						<h3>Você só precisa de</h3>
					</header>
					<nav class="divide-30">
						<?php
							foreach ($items as $item) {
								echo '<p class="divide-10"><span class="icon-check-circle lh-zero"></span> '. $item .'</p>';
							}
						?>
					</nav>
					<?php endif; ?>
					
					<?php
						if(!empty($modabiz_option['revendedor-pagamento'])):
					?>
					<!-- formas de pagamento -->
					<header class="divide-30">
						<h3>Formas de pagamento</h3>
					</header>
					<article class="divide-30">
						<p><?php echo $modabiz_option['revendedor-pagamento']; ?></p>
					</article>
				    <?php endif; ?>

					<!-- dúvida -->
					<header class="divide-30">
						<h3>Alguma dúvida?</h3>
					</header>
					<?php
						//Telefone
						if(isset($modabiz_option['corp-telefone']) && !empty($modabiz_option['corp-telefone'])):
							//$phone = explode(', ', $modabiz_option['corp-telefone']);
						?>
						<h5 class="no-margin secondary"><span class="icon-phone"></span> Telefone</h5>
						<h5 class="divide-30"><?php echo $modabiz_option['corp-telefone']; ?></h5>
						<?php
						endif;

						//Whasapp
						if(isset($modabiz_option['corp-whatsapp']) && !empty($modabiz_option['corp-whatsapp'])):
						?>
						<h5 class="no-margin secondary"><span class="icon-whatsapp"></span> Whatsapp</h5>
						<h5 class="divide-30"><?php echo $modabiz_option['corp-whatsapp']; ?></h5>
						<?php
						endif;
					?>
					<p class="text-up jump-questions"><strong>Veja as perguntas frequentes <span class="icon-chevron-thin-down"></span></strong></p>
					<p class="less-opc">Talvez sua dúvida já tenha sido resolvida lá</p>
				</div>
			</section>
		</div>

		
	</section>
	
	<!-- depoimentos -->
	<?php if(isset($modabiz_option['testimonials-items'])): ?>
	<div id="testimonials-slider" class="small-12 left rel">
		<div class="small-12 d-table-cell">
			<div class="row">
				<div class="small-12 medium-8 medium-offset-2 columns">
					<header class="divide-20 text-center">
						<h1 class="no-margin lh-small"><span class="icon-quotes-left"></span></h1>
					</header>

					<nav class="small-12 left nav-testimonials cycle-slideshow"
						data-cycle-fx="fade"
						data-cycle-timeout="8000"
						data-cycle-prev=".prev-panel"
						data-cycle-next=".next-panel"
						data-cycle-slides="> article"
						data-cycle-pager=".panel-pager"
						data-cycle-pager-template="<span></span>"
						data-cycle-swipe="true"
						data-cycle-swipe-fx="scrollHorz"
					>
					<?php
						foreach ($modabiz_option['testimonials-items'] as $tml):
					?>
						<article class="small-12 left text-center">
							<p><?php echo $tml['description'] ?></p>
							<figure class="small-12 left text-center">
								<?php if(!empty($tml['thumb'])): ?>
								<div class="testimonial-avatar" data-thumb="<?php echo $tml['thumb']; ?>"></div>
								<?php endif; ?>
								<span class="author-tes"><strong> <?php echo $tml['title']; if(!empty($tml['url'])) echo " - " . $tml['url']; ?></strong></span>
							</figure>
						</article>
					<?php
						endforeach;
					?>
					</nav>
					
				</div>
			</div>
			<div class="small-12 text-center abs pager-container">
				<nav class="panel-pager d-iblock"></nav>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<!-- perguntas frequentes -->
	<?php if(isset($modabiz_option['faq-items'])): ?>
	<section id="faq-list" class="small-12 left">
		<div class="row">
			<header class="small-12 columns text-center">
				<h3>Perguntas frequentes</h3>
			</header>

			<nav class="small-12 columns">
				<ul class="no-bullet no-margin small-12 left">
					<?php
						foreach ($modabiz_option['faq-items'] as $quest):
					?>
					<li>
						<span><span><?php echo $quest['title']; ?></span> <i class="icon-chevron-thin-down right"></i></span>
						<div class="small-12 left">
							<span class="less-opc"><?php echo $quest['description'] ?></span>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</nav>
		</div>
	</section>
	<?php endif; ?>

 	<?php
 	//if(is_active_sidebar('contact-layers')):
		/**
		 * Camadas para a página principal
		 */
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>
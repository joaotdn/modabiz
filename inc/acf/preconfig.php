<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_background-do-cabecalho',
		'title' => 'Background do cabeçalho',
		'fields' => array (
			array (
				'key' => 'field_55e5f67b15fba',
				'label' => 'Adicionar uma imagem',
				'name' => 'tax_bg',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'lookbooks',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'campanhas',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_campanha',
		'title' => 'Campanha',
		'fields' => array (
			array (
				'key' => 'field_55e5e8332a0a7',
				'label' => 'Adicionar imagem',
				'name' => 'campanha_galeria',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_55e5e8572a0a8',
						'label' => 'Imagem',
						'name' => 'campanha_imagem',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'full',
						'library' => 'all',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Adicionar imagem',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'campanha',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_lookbook',
		'title' => 'Lookbook',
		'fields' => array (
			array (
				'key' => 'field_55ccd2d28f7cf',
				'label' => 'Liste as peças desse Lookbook',
				'name' => 'lookbook_galeria',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_55e3304f85513',
						'label' => 'Imagem',
						'name' => 'lookbook_imagem',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'full',
						'library' => 'all',
					),
					array (
						'key' => 'field_55e5e3edeb21a',
						'label' => 'Link da peça na loja',
						'name' => 'comprar_peca',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55e5121cb529b',
						'label' => 'Medidas masculinas',
						'name' => 'medidas_masculinas',
						'type' => 'repeater',
						'column_width' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_55e5124db529c',
								'label' => 'Numeração',
								'name' => 'medidas_masculinas_numeracao',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e51292b529d',
								'label' => 'Busto (cm)',
								'name' => 'medidas_masculinas_busto',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e512acb529e',
								'label' => 'Cintura (cm)',
								'name' => 'medidas_masculinas_cintura',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e512cbb529f',
								'label' => 'Quadril (cm)',
								'name' => 'medidas_masculinas_quadril',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
						),
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'table',
						'button_label' => 'Adicionar medidas',
					),
					array (
						'key' => 'field_55e51326d7087',
						'label' => 'Medidas femininas',
						'name' => 'medidas_femininas',
						'type' => 'repeater',
						'column_width' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_55e5134fd7088',
								'label' => 'Numeração',
								'name' => 'medidas_femininas_numeracao',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e51370d7089',
								'label' => 'Busto (cm)',
								'name' => 'medidas_femininas_busto',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e51387d708a',
								'label' => 'Cintura (cm)',
								'name' => 'medidas_femininas_cintura',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_55e5139ad708b',
								'label' => 'Quadril (cm)',
								'name' => 'medidas_femininas_quadril',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
						),
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'table',
						'button_label' => 'Adicionar medidas',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Adicionar peça',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'lookbook',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
?>
<?php
	/**
	* Métodos úteis para snippets Wordpress
	* @since ModaBiz 1.0
	* @package Wordpress
	* @subpackage ModaBiz
	*/
	class ModaBizUtils
	{
		/**
		 * Use para registrar páginas no inicio da
		 * instalaçao do tema
		 * @param $title título da página
		 * @param $desc resumo da página
		 * @param $type tipo da postagem
		 */
		public function registerInitPage($title, $desc, $parent = 0, $template = '') {
			$page = get_page_by_title($title);
			if(!isset($page)) {
				$defaults = array(
				  'post_type'             => 'page',
				  'post_title'    		  => $title,
				  //'post_content'  		  => '',
				  'post_status'   		  => 'publish',
				  'post_author'   		  => 1,
				  'post_excerpt'          => __($desc,'modabiz'),
				  'post_parent' 		  => $parent,
				  'page_template' 		  => $template
				);
				wp_insert_post( $defaults );
			}
		}

		/**
		 * Lista as redes sociais institucionais da entidade
		 */
		static function listSocialNetWork() {
				global $modabiz_option;
				$html = '';
	      //Facebook institucional
	      if(!empty($modabiz_option['corp-facebook']))
	        $html .= '<li><h2><a href="'. $modabiz_option['corp-facebook'] .'" class="icon-facebook-with-circle" title="Seguir no Facebook" target="_blank"></a></h2></li>';

	      //Instagram institucional
	      if(!empty($modabiz_option['corp-instagram']))
	        $html .= '<li><h2><a href="'. $modabiz_option['corp-instagram'] .'" class="icon-instagram-with-circle" title="Seguir no Instagram" target="_blank"></a></h2></li>';

				return $html;
		}

	}

	//Retorna url do thumbnail
	function getThumbUrl($size,$post_id) {
	    if (!isset($size)) {
	        $size = 'full';
	    }
	    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);
	    if($thumb[0] != "") {
	        return $thumb[0];
	    }
	}

	//Nome da 1a categoria de uma postagem em um loop
	function get_first_category_name($post_id) {
	    $category = get_the_category($post_id);
	    if ($category[0]) {
	        return $category[0]->cat_name;
	    }
	}

	//Link da 1a categoria de uma postagem em um loop
	function get_first_category_link($post_id) {
	    $category = get_the_category($post_id);
	    if ($category[0]) {
	        return get_category_link($category[0]->term_id);
	    }
	}

	/**
	 * Breadcrumb
	 * @return {String} Caminho da pagina corrente
	 */
	function the_breadcrumb() {
	     
	    // Settings
	    $separator  = '<small>&bullet;</small>';
	    $id         = 'breadcrumb';
	    $class      = 'breadcrumbs';
	    $home_title = get_bloginfo('name');
	     
	    // Get the query & post information
	    global $post,$wp_query;
	    $category = get_categories();
	     
	    // Build the breadcrums
	    // echo '<ul id="' . $id . '" class="' . $class . ' inline-list">';
	     
	    // Do not display on the homepage
	    if ( !is_front_page() ) {
	         
	        // Home page
	        echo '<span class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></span>';
	        echo '<i class="separator separator-home"> ' . $separator . ' </i>';
	         
	        if ( is_singular() ) {
	             $obj = get_queried_object();
	             echo '<span><a href="'. get_post_type_archive_link( $obj->post_type ) .'">'. ucwords($obj->post_type) .'</a></span>';
	             echo '<i class="separator"> ' . $separator . ' </i>';
	             echo '<span>'. $obj->post_title .'</span>';
	            
	        } else if ( is_single() ) {

	        	// Single post (Only display the first category)
	            echo '<span class="item-cat item-cat-' . $category->term_id . ' item-cat-' . $category->category_nicename . '"><a class="bread-cat bread-cat-' . $category->term_id . ' bread-cat-' . $category->category_nicename . '" href="' . get_category_link($category->term_id ) . '" title="' . $category->cat_name . '">' . $category->cat_name . '</a></span>';
	            echo '<i class="separator separator-' . $category->term_id . '"> ' . $separator . ' </i>';
	            echo '<span class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></span>';

	        } else if ( is_category() ) {
	             
	            // Category page
	            echo '<span class="item-current item-cat-' . $category->term_id . ' item-cat-' . $category->category_nicename . '"><span class="bread-current bread-cat-' . $category->term_id . ' bread-cat-' . $category->category_nicename . '">' . $category->cat_name . '</span></span>';
	             
	        } else if ( is_page() ) {
	             
	            // Standard page
	            if( $post->post_parent ){
	                 
	                // If child page, get parents 
	                $anc = get_post_ancestors( $post->ID );
	                 
	                // Get parents in the right order
	                $anc = array_reverse($anc);
	                 
	                // Parent page loop
	                foreach ( $anc as $ancestor ) {
	                    $parents .= '<span class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></span>';
	                    $parents .= '<i class="separator separator-' . $ancestor . '"> ' . $separator . ' </i>';
	                }
	                 
	                // Display parent pages
	                echo $parents;
	                 
	                // Current page
	                echo '<span class="item-current item-' . $post->ID . '"><span title="' . get_the_title() . '"> ' . get_the_title() . '</span></span>';
	                 
	            } else {
	                 
	                // Just display current page if not parents
	                echo '<span class="item-current item-' . $post->ID . '"><span class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></span>';
	                 
	            }
	             
	        } else if ( is_tag() ) {
	             
	            // Tag page
	             
	            // Get tag information
	            $term_id = get_query_var('tag_id');
	            $taxonomy = 'post_tag';
	            $args ='include=' . $term_id;
	            $terms = get_terms( $taxonomy, $args );
	             
	            // Display the tag name
	            echo '<span class="item-current item-tag-' . $terms[0]->term_id . ' item-tag-' . $terms[0]->slug . '"><span class="bread-current bread-tag-' . $terms[0]->term_id . ' bread-tag-' . $terms[0]->slug . '">' . $terms[0]->name . '</span></span>';
	         
	        } elseif ( is_day() ) {
	             
	            // Day archive
	             
	            // Year link
	            echo '<span class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></span>';
	            echo '<i class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </i>';
	             
	            // Month link
	            echo '<span class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></span>';
	            echo '<i class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </i>';
	             
	            // Day display
	            echo '<span class="item-current item-' . get_the_time('j') . '"><span class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></span>';
	             
	        } else if ( is_month() ) {
	             
	            // Month Archive
	             
	            // Year link
	            echo '<span class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></span>';
	            echo '<i class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </i>';
	             
	            // Month display
	            echo '<span class="item-month item-month-' . get_the_time('m') . '"><span class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></span>';
	             
	        } else if ( is_year() ) {
	             
	            // Display year archive
	            echo '<span class="item-current item-current-' . get_the_time('Y') . '"><span class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></span>';
	             
	        } else if ( is_author() ) {
	             
	            // Auhor archive
	             
	            // Get the author information
	            global $author;
	            $userdata = get_userdata( $author );
	             
	            // Display author name
	            echo '<span class="item-current item-current-' . $userdata->user_nicename . '"><span class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</span></span>';
	         
	        } else if ( get_query_var('paged') ) {
	             
	            // Paginated archives
	            echo '<span class="item-current item-current-' . get_query_var('paged') . '"><span class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></span>';
	             
	        } else if ( is_search() ) {
	         
	            // Search results page
	            echo '<span class="item-current item-current-' . get_search_query() . '"><span class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></span>';
	         
	        } elseif ( is_404() ) {
	             
	            // 404 page
	            echo '<span>' . 'Error 404' . '</span>';
	        }
	         
	    }
	     
	}

	//Filtro para postagens mais lidas do blog
	function filter_where($where = '') {
	    //posts in the last 30 days
	    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
	    return $where;
	}
	add_filter('posts_where', 'filter_where');
?>

<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "modabiz_option";
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);
    
    /*function compiler_action($options, $css, $changed_values) {
        echo '<h1>The compiler hook has run!</h1>';
         
        print_r ($options);
        print_r ($css);
        print_r ($changed_values);
    }*/

    //array de avatars para o instagram
    //$arr_hash_imgs = get_hash_imgs();

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => THEME_VERSION,
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'ModaBiz', 'redux-framework-demo' ),
        'page_title'           => __( 'ModaBiz', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => THEME_ICON,
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            =>  THEME_ICON,
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'https://www.facebook.com/sigamodabiz?fref=ts',
        'title' => __( 'Siga-nos', 'redux-framework-demo' ),
    );

    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/sigamodabiz?fref=ts',
        'title' => 'ModaBiz no Facebook',
        'icon'  => 'el el-facebook'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Opções gerais para configurações do tema</p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p></p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'ModaBiz', 'redux-framework-demo' ),
            'content' => __( '<p>Somos uma ótima solução de marketing online focada na captação, organização e relacionamento de revendedoras para marcas de moda atacadista</p>', 'redux-framework-demo' )
        ),

        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Localizações', 'redux-framework-demo' ),
            'content' => __( '<h4>Regras</h4><ul><li>Caso não seja definido a latitude e a longitude inicial da localização, as coordenadas iniciais serão da primeira localização cadastada no banco.</li><li>Se os ícones marcadores não forem definidos, o mapa irá utilizar o padrão google</li></ul>', 'redux-framework-demo' )
        ),

        array(
            'id'      => 'redux-help-tab-3',
            'title'   => __( 'Instagram', 'redux-framework-demo' ),
            'content' => __( '<h4>Regras</h4><ul><li>Se algum campo das credenciais estiver em branco, o componente não será inicializado.</li><li>Sempre que marcar as fotos da hashtag e perfil as fotos exibidas no período corrente serão apagadas e substituidas pelas fotos marcadas após o salvamento</li><li></li></ul>', 'redux-framework-demo' )
        ),
    );

    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>Para outras dúvidas, entre em contato com o suporte</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );

    /**
     * INSTITUCIONAL
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Institucional', 'redux-framework-demo' ),
        'id'        => 'corporation',
        'desc'      => __( 'Dados de contato da entidade', 'redux-framework-demo' ),
        'icon'      => 'el el-group',
        'fields'    => array(
          array(
              'id'       => 'corp-rua',
              'type'     => 'text',
              'title'    => __( 'Rua, Número', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'Rua Francisco de Assis Frade, 150',
          ),
          array(
              'id'       => 'corp-complemento',
              'type'     => 'text',
              'title'    => __( 'Complemento', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'corp-bairro',
              'type'     => 'text',
              'title'    => __( 'Bairro', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'Manaíra',
          ),
          array(
              'id'       => 'corp-cep',
              'type'     => 'text',
              'title'    => __( 'CEP', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'corp-cidade',
              'type'     => 'text',
              'title'    => __( 'Cidade - UF', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'João Pessoa - PB',
          ),
          array(
              'id'       => 'corp-telefone',
              'type'     => 'text',
              'title'    => __( 'Telefones', 'redux-framework-demo' ),
              'subtitle' => __( 'Coloque os telefones separados por ","', 'redux-framework-demo' ),
              'desc'     => __( 'Ex.: (83) 9898-9898, (83) 7676-7676...', 'redux-framework-demo' ),
              'default'  => '83 3513.9633, 81 3731.1059',
          ),
          array(
              'id'       => 'corp-email',
              'type'     => 'text',
              'title'    => __( 'Email principal', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'Coloque aqui o email primário da empresa', 'redux-framework-demo' ),
              'default'  => 'contato@modabiz.com.br',
          ),
          array(
              'id'       => 'corp-horario',
              'type'     => 'text',
              'title'    => __( 'Horário de funcionamento', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '08h às 17h30, de seg. à sex. e de 08h às 12h30 aos sábados',
          ),
          array(
              'id'       => 'corp-loja',
              'type'     => 'text',
              'title'    => __( 'Link da loja virtual', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'Link completo da loja', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'corp-map',
              'type'     => 'text',
              'title'    => __( 'Link no Google Maps', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'Link do mapa', 'redux-framework-demo' ),
              'default'  => '',
          )
        )
    ) );

    //Redes sociais
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Redes sociais', 'redux-framework-demo' ),
        'id'        => 'corp-social',
        'desc'      => __( 'Principais redes sociais da entidade', 'redux-framework-demo' ),
        'subsection'=> true,
        'fields'    => array(
          array(
              'id'       => 'corp-facebook',
              'type'     => 'text',
              'title'    => __( 'Link da fanpage', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'com http:// ou https://', 'redux-framework-demo' ),
              'default'  => 'https://www.facebook.com/sigamodabiz/timeline',
          ),
          array(
              'id'       => 'corp-instagram',
              'type'     => 'text',
              'title'    => __( 'Link para o Instagram', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'com http:// ou https://', 'redux-framework-demo' ),
              'default'  => 'https://instagram.com/sigamodabiz/',
          ),
          array(
              'id'       => 'corp-whatsapp',
              'type'     => 'text',
              'title'    => __( 'Número do whatsapp', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'ex.: (81) 98899-6754', 'redux-framework-demo' ),
              'default'  => '',
          ),
        )
    ) );

    /**
     * OPÇÕES GERAIS
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Opções gerais', 'redux-framework-demo' ),
        'id'        => 'basic',
        'desc'      => __( 'Conjunto de opções globais para toda a aplicação', 'redux-framework-demo' ),
        'icon'      => 'el el-home',
        'fields'    => array(
          array(
              'id'       => 'modabiz-bg-color',
              'type'     => 'color',
              'output'      => array('background-color' => 'body'),
              'title'    => __('Cor de fundo do site', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#ffffff',
              'validate' => 'color',
          ),
        )
    ) );

    /**
     * Tipografia
     */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Tipografia', 'redux-framework-demo' ),
        'desc'       => __( 'Opções globais para a tipografia da aplicação', 'redux-framework-demo' ),
        'id'         => 'modabiz-typography',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'          => 'modabiz-type-h1',
                'type'        => 'typography',
                'title'       => __('Títulos primários', 'redux-framework-demo'),
                'compiler'    => true,
                'google'      => true,
                'output'      => array('h1,h2,h3,h4,h5,h6'),
                'units'       =>'px',
                'subtitle'    => __('Define o estilo das fontes de cabeçalho', 'redux-framework-demo'),
                'preview'     => array(
                    'always_display' => true,
                    'font-size'   => '30px',
                ),
                'color'      => true,
                'font-size'   => false,
                'line-height' => false,
                'text-align'  => false,
                'font-style'  => false,
                'font-backup' => false,
                //'font-weight' => false,
                'subsets' => false,

                'default'     => array(
                    'color'       => '#333',
                    'font-style'  => '700',
                    'font-family' => 'Open',
                    'google'      => true,
                    'font-size'   => '40px',
                    'line-height' => '40px',
                    'text-align'  => 'left',
                    'font-weight' => '700'
                ),
            ),
            array(
                'id'          => 'modabiz-type-h2',
                'type'        => 'typography',
                'title'       => __('Títulos secundários', 'redux-framework-demo'),
                'compiler'    => true,
                'google'      => true,
                'output'      => array('.secondary'),
                'units'       =>'px',
                'subtitle'    => __('Estilo para cabeçalhos de 2<sup>o</sup> nível', 'redux-framework-demo'),
                'preview'     => array(
                    'always_display' => true,
                    'font-size'   => '30px',
                ),
                'color'      => true,
                'font-size'   => false,
                'line-height' => false,
                'text-align'  => false,
                'font-style'  => false,
                'font-backup' => false,
                //'font-weight' => false,
                'subsets' => false,

                'default'     => array(
                    'color'       => '#333',
                    'font-style'  => '700',
                    'font-family' => 'Open',
                    'google'      => true,
                    'font-size'   => '40px',
                    'line-height' => '40px',
                    'text-align'  => 'left',
                    'font-weight' => '700'
                ),
            ),
            array(
                'id'          => 'modabiz-type-p',
                'type'        => 'typography',
                'title'       => __('Parágrafos', 'redux-framework-demo'),
                //'compiler'    => true,
                'google'      => true,
                'output'      => array('p','#breadcrumb span','.article-content li'),
                'units'       =>'px',
                'subtitle'    => __('Define o estilo das fontes para os textos', 'redux-framework-demo'),
                'preview'     => array(
                    'always_display' => true,
                    //'font-size'   => '16px',
                ),
                'color'      => true,
                'font-size'   => true,
                'line-height' => true,
                'text-align'  => false,
                //'font-style'  => false,
                'font-backup' => false,
                //'font-weight' => false,
                'subsets' => false,

                'default'     => array(
                    'color'       => '#666666',
                    //'font-style'  => '700',
                    'font-family' => 'Helvetica',
                    'google'      => true,
                    'font-size'   => '16px',
                    'line-height' => '20px',
                    'text-align'  => 'left',
                    'font-weight' => '300'
                ),
            ),
            array(
                'id'       => 'anchor-color',
                'type'     => 'color',
                'output'      => array('color' => 'a, a:focus'),
                'title'    => __('Cor para os links', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#069',
                'validate' => 'color',
            ),
            array(
                'id'       => 'anchor-hover-color',
                'type'     => 'color',
                'output'      => array('color' => 'a:hover'),
                'title'    => __('Cor para hover no link', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#c00',
                'validate' => 'color',
            ),
            array(
                'id'          => 'modabiz-type-input',
                'type'        => 'typography',
                'title'       => __('Inputs', 'redux-framework-demo'),
                //'compiler'    => true,
                'google'      => true,
                'output'      => array('input[type="text"],input[type="email"],input[type="tel"]'),
                'units'       =>'px',
                'subtitle'    => __('Define o estilo das fontes para os botões e formularios de entrada de texto (input, textareas)', 'redux-framework-demo'),
                'preview'     => array(
                    'always_display' => true,
                    //'font-size'   => '16px',
                ),
                'color'      => true,
                'font-size'   => true,
                'line-height' => false,
                'text-align'  => true,
                'font-style'  => false,
                'font-backup' => false,
                //'font-weight' => false,
                'subsets' => false,

                'default'     => array(
                    'color'       => '#666666',
                    //'font-style'  => '700',
                    'font-family' => 'Helvetica',
                    'google'      => true,
                    'font-size'   => '16px',
                    //'line-height' => '20px',
                    'text-align'  => 'left',
                    'font-weight' => '300'
                ),
            )

        )
    ) );

    //botões
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Botões', 'redux-framework-demo' ),
        'desc'       => __( 'Configurar botão primário', 'redux-framework-demo' ),
        'id'         => 'modabiz-button-global',
        'subsection' => true,

        'fields'     => array(
            array(
                'id'       => 'global-btn-color',
                'type'     => 'color',
                'output'      => array('color' => '.generic-btn, .generic-btn:hover'),
                'title'    => __('Cor da fonte', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#333',
                'validate' => 'color',
            ),

            array(
                'id'       => 'global-btn-bgcolor',
                'type'     => 'color',
                'output'      => array('background-color' => '.generic-btn,.generic-btn:focus'),
                'title'    => __('Cor de fundo', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#fff',
                'validate' => 'color',
            ),

            array(
                'id'       => 'global-btn-color-hover',
                'type'     => 'color',
                'output'      => array('background-color' => '.generic-btn:hover'),
                'title'    => __('Cor de fundo no hover', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => 'transparent',
                'validate' => 'color',
            ),

            array(
                'id'            => 'global-btn-bwidth',
                'type'          => 'slider',
                'output'      => array('border-width' => '.generic-btn'),
                'title'         => __( 'Largura da borda', 'redux-framework-demo' ),
                'subtitle'      => '',
                'desc'          => '',
                'default'       => 5,
                'min'           => 0,
                'step'          => 1,
                'max'           => 10,
                'display_value' => 'text'
            ),

            array(
                'id'       => 'global-btn-bcolor',
                'type'     => 'color',
                'output'      => array('border-color' => '.generic-btn'),
                'title'    => __('Cor da borda', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#333',
                'validate' => 'color',
            ),
        )
    ) );

    /**
     * Navegação
     */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Topo', 'redux-framework-demo' ),
        'desc'       => __( 'Opções para exibição da marca', 'redux-framework-demo' ),
        'id'         => 'modabiz-brand',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'top-color',
                'type'     => 'color',
                'output'      => array('background-color' => '#main-menu'),
                'title'    => __('Cor de fundo para o topo', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#ffffff',
                'validate' => 'color',
            ),
            array(
                'id'       => 'modabiz-brand-img',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Imagem para a logo', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( '', 'redux-framework-demo' ),
                'subtitle' => __( 'SVG, PNG, GIF ou JPG', 'redux-framework-demo' ),
                'default'  => dirname(__FILE__) . '/images/logo.png',
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'          => 'modabiz-brand-align',
                'type'        => 'typography',
                'title'       => __('Alinhamento da logo', 'redux-framework-demo'),
                'compiler'    => true,
                'google'      => true,
                'output'      => array('#main-menu'),
                'units'       =>'px',
                'subtitle'    => __('Alinhe a posição da marca no topo do site', 'redux-framework-demo'),
                'preview'     => false,
                'color'      => false,
                'font-size'   => false,
                'line-height' => false,
                'text-align'  => true,
                'font-style'  => false,
                'font-backup' => false,
                'font-weight' => false,
                'subsets' => false,
                'font-family' => false,

                'default'     => array(
                    'text-align'  => 'left'
                ),
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Menu', 'redux-framework-demo' ),
        'desc'       => __( 'Opções para exibição do menu', 'redux-framework-demo' ),
        'id'         => 'modabiz-nav',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'nav-btn-color',
                'type'     => 'color',
                'output'      => array('color' => '.toggle-menu'),
                'title'    => __('Cor do botão menu', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#333',
                'validate' => 'color',
            ),

            array(
                'id'       => 'nav-bg-color',
                'type'     => 'color',
                'output'      => array('background-color' => '#off-menu'),
                'title'    => __('Cor de fundo do menu', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#333',
                'validate' => 'color',
            ),

            array(
                'id'       => 'nav-font-color',
                'type'     => 'color',
                'output'      => array('color' => '#off-menu a', 'border-color' => '#off-menu input'),
                'title'    => __('Cor das fontes do menu', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#fff',
                'validate' => 'color',
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Rodapé', 'redux-framework-demo' ),
        'desc'       => __( 'Opções para exibição do rodapé', 'redux-framework-demo' ),
        'id'         => 'modabiz-footer',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'footer-bg',
                'type'     => 'color',
                'output'      => array('background-color' => '#footer'),
                'title'    => __('Cor de fundo', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#ffffff',
                'validate' => 'color',
            ),
            array(
                'id'       => 'footer-type',
                'type'     => 'color',
                'output'      => array('color' => '#footer p, #footer h1, #footer h2, #footer h5, #footer h3, #footer h4, #footer h6, #footer small'),
                'title'    => __('Cor da fonte', 'redux-framework-demo'),
                'subtitle' => __('', 'redux-framework-demo'),
                'default'  => '#333333',
                'validate' => 'color',
            ),
            array(
                'id'       => 'footer-anchor-colors',
                'type'     => 'link_color',
                'title'    => __( 'Cor dos links', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'output'      => array('#footer a'),
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                //'active'    => false, // Disable Active Color
                //'visited'   => true,  // Enable Visited Color
                'default'  => array(
                    'regular' => '#333',
                    'hover'   => '#666',
                    'active'  => '#333',
                )
            )
        )
    ) );

    /**
     * PAINEL
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Painel', 'redux-framework-demo' ),
        'id'        => 'slider-panel',
        'desc'      => __( 'Monte aqui o slide para o painel. Todas as imagens serão exibidas no slider.', 'redux-framework-demo' ),
        'icon'      => 'el el-photo',
        'fields'    => array(
          array(
              'id'          => 'slide-gallery',
              'type'        => 'slides',
              'title'       => __('Galeria', 'redux-framework-demo'),
              'subtitle'    => __('', 'redux-framework-demo'),
              'desc'        => __('', 'redux-framework-demo'),
              'placeholder' => array(
                  'title'           => __('Título', 'redux-framework-demo'),
                  'subtitle'           => __('Título do botão', 'redux-framework-demo'),
                  'description'     => __('Subtítulo', 'redux-framework-demo'),
                  'url'             => __('Link', 'redux-framework-demo'),
                  'width'           => '1280',
                  'height'          => '490'
              ),
          ),
        )
    ) );

    //estilo
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Estilo', 'redux-framework-demo' ),
        'id'        => 'slider-panel-css',
        'desc'      => __( 'Estilizar componentes do painel', 'redux-framework-demo' ),
        'subsection'      => true,
        'fields'    => array(
          array(
            'id'       => 'section-panel-css-0',
            'type'     => 'section',
            'title'    => __( 'Container', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'indent'   => true,
          ),
          array(
              'id'             => 'section-panel-css-height',
              'type'           => 'dimensions',
              'units'          => array( 'em', 'px' ),
              'units_extended' => 'true',
              'output'      => array('height' => '#panel-slider, .slider-items, .slider-items figure, .slider-items figure > .row, .nav-panel'),
              'title'          => __( 'Altura do painel', 'redux-framework-demo' ),
              'subtitle'       => __( '', 'redux-framework-demo' ),
              'desc'           => __( '', 'redux-framework-demo' ),
              'width'          => false,
              'default'        => array(
                  'height' => 490,
              )
          ),
          array(
              'id'       => 'section-panel-mask',
              'type'     => 'color_rgba',
              'title'    => __( 'Máscara', 'redux-framework-demo' ),
              'subtitle' => __( 'Película para background', 'redux-framework-demo' ),
              'default'  => array(
                  'color' => '#000',
                  'alpha' => '.6'
              ),
              'output'   => array( '#panel-slider .black-mask' ),
              'mode'     => 'background',
              'validate' => 'colorrgba',
          ),
          array(
              'id'       => 'section-panel-nav',
              'type'     => 'color',
              'output'      => array(
                'color' => '#panel-slider .nav-panel',
                'background-color' => '#panel-slider .panel-pager span:not(.cycle-pager-active)',
                'border-color' => '#panel-slider .panel-pager span'),

              'title'    => __('Navegação', 'redux-framework-demo'),
              'subtitle' => __('(setas, bullets)', 'redux-framework-demo'),
              'default'  => '#FFF',
              'validate' => 'color',
          ),
          array(
            'id'     => 'section-panel-css-end-0',
            'type'   => 'section',
            'indent' => true,
          ),
          array(
            'id'       => 'section-panel-css-1',
            'type'     => 'section',
            'title'    => __( 'Fontes', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'indent'   => true,
          ),
          array(
              'id'          => 'modabiz-panel-title',
              'type'        => 'typography',
              'title'       => __('Título', 'redux-framework-demo'),
              'compiler'    => true,
              'google'      => false,
              'output'      => array('#panel-slider h1'),
              'units'       =>'px',
              'subtitle'    => __('', 'redux-framework-demo'),
              'preview'     => false,
              'color'      => false,
              'font-size'   => true,
              'line-height' => false,
              'text-align'  => false,
              'font-style'  => true,
              'font-backup' => false,
              'font-weight' => true,
              'subsets' => false,
              'font-family' => false,

              'default'     => array(
                  'text-align'  => 'center',
                  'font-size'   => '72px'
              ),
          ),
          array(
              'id'          => 'modabiz-panel-subtitle',
              'type'        => 'typography',
              'title'       => __('Sub Título', 'redux-framework-demo'),
              'compiler'    => true,
              'google'      => false,
              'output'      => array('#panel-slider h4'),
              'units'       =>'px',
              'subtitle'    => __('', 'redux-framework-demo'),
              'preview'     => false,
              'color'      => false,
              'font-size'   => true,
              'line-height' => false,
              'text-align'  => false,
              'font-style'  => true,
              'font-backup' => false,
              'font-weight' => true,
              'subsets' => false,
              'font-family' => false,

              'default'     => array(
                  'text-align'  => 'center',
                  'font-size'   => '20px'
              ),
          ),
          array(
              'id'       => 'modabiz-panel-anchor',
              'type'     => 'link_color',
              'title'    => __( 'Âncora', 'redux-framework-demo' ),
              'subtitle' => __( 'Estilo do link', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'output'      => array('#panel-slider a'),
              'default'  => array(
                  'regular' => '#fff',
                  'hover'   => '#f1f1f1',
                  'active'  => '#fff',
              )
          ),
          array(
            'id'     => 'section-panel-css-end-1',
            'type'   => 'section',
            'indent' => true,
          ),
          //botao
          array(
            'id'       => 'section-panel-css-2',
            'type'     => 'section',
            'title'    => __( 'Botão', 'redux-framework-demo' ),
            'subtitle' => __( '', 'redux-framework-demo' ),
            'indent'   => true,
          ),
          array(
              'id'          => 'modabiz-panel-btn',
              'type'        => 'typography',
              'title'       => __('Fonte', 'redux-framework-demo'),
              'compiler'    => true,
              'google'      => false,
              'output'      => array('#panel-slider .button'),
              'units'       =>'px',
              'subtitle'    => __('', 'redux-framework-demo'),
              'preview'     => false,
              'color'      => false,
              'font-size'   => true,
              'line-height' => false,
              'text-align'  => false,
              'font-style'  => true,
              'font-backup' => false,
              'font-weight' => true,
              'subsets' => false,
              'font-family' => false,

              'default'     => array(
                  'text-align'  => 'center',
                  'font-size'   => '20px'
              ),
          ),
          array(
              'id'       => 'modabiz-panel-btn-anchor',
              'type'     => 'link_color',
              'title'    => __( 'Âncora', 'redux-framework-demo' ),
              'subtitle' => __( 'Estilo do link', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'output'      => array('#panel-slider .button'),
              'default'  => array(
                  'regular' => '#fff',
                  'hover'   => '#f1f1f1',
                  'active'  => '#fff',
              )
          ),
          array(
              'id'       => 'section-panel-bgbtn',
              'type'     => 'color',
              'output'      => array('background-color' => '#panel-slider .button, #panel-slider .button:focus'),
              'title'    => __('Regular', 'redux-framework-demo'),
              'subtitle' => __('Background', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'section-panel-bgbtn-hover',
              'type'     => 'color',
              'output'      => array('background-color' => '#panel-slider .button:hover', 'border-color' => '#panel-slider .button:hover'),
              'title'    => __('Suspenso', 'redux-framework-demo'),
              'subtitle' => __('Background', 'redux-framework-demo'),
              'default'  => '#000',
              'validate' => 'color',
          ),
          array(
              'id'       => 'section-panel-btn-border',
              'type'     => 'border',
              'title'    => __( 'Borda', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'output'   => array( '#panel-slider .button' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => array(
                  'border-color'  => '#fff',
                  'border-style'  => 'solid',
                  'border-top'    => '3px',
                  'border-right'  => '3px',
                  'border-bottom' => '3px',
                  'border-left'   => '3px'
              )
          ),
          array(
            'id'     => 'section-panel-css-end-2',
            'type'   => 'section',
            'indent' => true,
          ),
        )
    ) );

    /**
     * Galeria
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Galeria', 'redux-framework-demo' ),
        'id'        => 'grid-gallery',
        'desc'      => __( 'Monte aqui o slide para o painel. Todas as imagens serão exibidas no slider.', 'redux-framework-demo' ),
        'icon'      => 'el el-camera',
        'fields'    => array(
          array(
            'id'          => 'grid-photos',
            'type'        => 'slides',
            'title'       => __('Conteúdo da galeria', 'redux-framework-demo'),
            'subtitle'    => __('', 'redux-framework-demo'),
            'desc'        => __('', 'redux-framework-demo'),
            'placeholder' => array(
                'title'           => __('Título', 'redux-framework-demo'),
                'description'     => __('Embedar vídeo (Opcional)', 'redux-framework-demo'),
                'url'             => __('Link (Opcional)', 'redux-framework-demo'),
                'width'             => '290',
                'subtitle' => 'teste'
            ),
          ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banners', 'redux-framework-demo' ),
        'desc'       => __( 'Banners juntos a galeria', 'redux-framework-demo' ),
        'id'         => 'grid-gallery-banners',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'grid-banner-top',
              'type'     => 'media',
              'url'      => true,
              'title'    => __( 'Banner topo', 'redux-framework-demo' ),
              'compiler' => 'true',
              'desc'     => __( 'Banner logo acima da galeria', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'grid-banner-link-1',
              'type'     => 'text',
              'title'    => __( 'Link do banner', 'redux-framework-demo' ),
              'subtitle' => __( 'Opcional', 'redux-framework-demo' ),
              'desc'     => __( 'Link para o banner do topo', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'grid-banner-bottom',
              'type'     => 'media',
              'url'      => true,
              'title'    => __( 'Banner rodape', 'redux-framework-demo' ),
              'compiler' => 'true',
              'desc'     => __( 'Banner logo abaixo da galeria', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'grid-banner-link-2',
              'type'     => 'text',
              'title'    => __( 'Link do banner', 'redux-framework-demo' ),
              'subtitle' => __( 'Opcional', 'redux-framework-demo' ),
              'desc'     => __( 'Link para o banner do rodapé', 'redux-framework-demo' ),
              'default'  => '',
          ),
        )
    ) );

    /**
     * LOCALIZAÇÔES
     * -------------------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Localizações', 'redux-framework-demo' ),
        'id'        => 'gmap',
        'desc'      => __( 'Opções globais para o componente de localizaçõs', 'redux-framework-demo' ),
        'icon'      => 'el el-map-marker',
        'fields'     => array(
            array(
                'id'       => 'gmap-api',
                'type'     => 'text',
                'title'    => __( 'API KEY', 'redux-framework-demo' ),
                'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
                'desc'     => __( 'Necessário para exibir o mapa', 'redux-framework-demo' ),
                'default'  => 'AIzaSyB664XOo1V2z76RD87sMi4b4nAM1JzKthg',
            ),

            array(
                'id'       => 'gmap-lat',
                'type'     => 'text',
                'title'    => __( 'Latitude inicial', 'redux-framework-demo' ),
                'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
                'desc'     => __( 'Latitude inicial para configurar o mapa', 'redux-framework-demo' ),
                'default'  => '',
            ),

            array(
                'id'       => 'gmap-lng',
                'type'     => 'text',
                'title'    => __( 'Longitude inicial', 'redux-framework-demo' ),
                'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
                'desc'     => __( 'Longitude inicial para configurar o mapa', 'redux-framework-demo' ),
                'default'  => '',
            ),
            array(
                'id'       => 'gmap-placeicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Marcador para as localizações', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Ícone que irá marcar os locais cadastrados', 'redux-framework-demo' ),
                'subtitle' => __( 'Envie icones pequenos, semelhanes ao do Google Maps', 'redux-framework-demo' ),
                'default'  => '',
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'       => 'gmap-placeicon-over',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Marcador para as localizações (suspenso)', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Ícone hover', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => '',
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            ),
            array(
                'id'       => 'gmap-geoicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Marcador para geolocalização', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Ícone que irá marcar o local do usuário', 'redux-framework-demo' ),
                'subtitle' => __( 'Envie icones pequenos, semelhanes ao do Google Maps', 'redux-framework-demo' ),
                'default'  => '',
                //'hint'      => array(
                //    'title'     => 'Hint Title',
                //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                //)
            )
        )
    ) );

    /**
     * INSTAGRAM
     * //doc: https://instagram.com/developer/endpoints/
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Instagram', 'redux-framework-demo' ),
        'id'        => 'instagram',
        'desc'      => __( 'Credenciais para iniciar o app Instagram. Mais informações em <a href="http://developers.instagram.com/" target="_blank">http://developers.instagram.com/</a>', 'redux-framework-demo' ),
        'icon'      => 'el el-instagram',
        'fields'    => array(
          array(
              'id'       => 'instagram-icon',
              'type'     => 'media',
              'url'      => true,
              'title'    => __( 'Logo personalizada para instagram', 'redux-framework-demo' ),
              'compiler' => 'true',
              //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
              'desc'     => __( '', 'redux-framework-demo' ),
              'subtitle' => __( 'SVG, PNG, GIF ou JPG', 'redux-framework-demo' ),
              'default'  => get_stylesheet_directory_uri() . '/images/logo.png',
              //'hint'      => array(
              //    'title'     => 'Hint Title',
              //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
              //)
          ),
          array(
              'id'       => 'instagram-id',
              'type'     => 'text',
              'title'    => __( 'ID do cliente', 'redux-framework-demo' ),
              'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'c83f2b9250fa4014969a0b94f324ded2',
          ),
          array(
              'id'       => 'instagram-uri',
              'type'     => 'text',
              'title'    => __( 'Url de redirecionamento', 'redux-framework-demo' ),
              'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'http://www.modabiz.com.br',
          ),

          array(
              'id'       => 'instagram-token',
              'type'     => 'text',
              'title'    => __( 'Token', 'redux-framework-demo' ),
              'subtitle' => __( 'Obrigatório', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '186283994.c83f2b9.ddee448c35734e39a6374c53ee9aeced',
          ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Perfil', 'redux-framework-demo' ),
        'desc'       => __( 'Fotos do perfil a partir do ID de usuário', 'redux-framework-demo' ),
        'id'         => 'instagram-profile-options',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'instagram-username',
              'type'     => 'text',
              'title'    => __( 'Nome do usuário', 'redux-framework-demo' ),
              'subtitle' => __( 'Opcional (impresso no componente)', 'redux-framework-demo' ),
              'desc'     => __( 'Ex.: @modaBiz', 'redux-framework-demo' ),
              'default'  => '@sigamodabiz',
          ),
          array(
              'id'       => 'instagram-userid',
              'type'     => 'text',
              'title'    => __( 'Id do usuário', 'redux-framework-demo' ),
              'subtitle' => __( 'Obrigatório (Apenas números)', 'redux-framework-demo' ),
              'desc'     => __( '<a href="http://www.otzberg.net/iguserid/" target="_blank">Encontre seu ID aqui</a><br><small class="hash_wait_res"></small>', 'redux-framework-demo' ),
              'default'  => '1943410735',
          ),
          array(
              'id'       => 'instagram-profile-img-list',
              'type'     => 'callback',
              'title'    => __( 'Resultados das fotos no perfil', 'redux-framework-demo' ),
              'subtitle' => __( 'Fotos públicas mais recentes do perfil<br><a href="#" class="unselect-us">Desmarcar todas</a>', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'callback' => 'call_profile_imgs'
          )
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'      => __( 'Hashtag', 'redux-framework-demo' ),
        'desc'       => __( 'Opções globais para a tipografia da aplicação', 'redux-framework-demo' ),
        'id'         => 'instagram-hash-options',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'instagram-hash',
              'type'     => 'text',
              'title'    => __( 'Hashtag para listar imagens', 'redux-framework-demo' ),
              'subtitle' => __( 'Obrigatório (sem "#")', 'redux-framework-demo' ),
              'desc'     => __( '<small class="hash_wait_res"></small>', 'redux-framework-demo' ),
              'default'  => 'modabiz',
          ),
          array(
              'id'       => 'instagram-hash-img-list',
              'type'     => 'callback',
              'title'    => __( 'Resultados da hashtag', 'redux-framework-demo' ),
              'subtitle' => __( 'Fotos mais recentes usando essa hashtag<br><a href="#" class="unselect-us">Desmarcar todas</a>', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'callback' => 'call_hash_imgs',
              'options'  => '',
              'hash'     => Redux::getField($opt_name, 'instagram-token')
          )
        )
    ) );

    /**
     * Campanhas
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Campanha', 'redux-framework-demo' ),
        'id'        => 'campaing',
        'desc'      => __( '', 'redux-framework-demo' ),
        'icon'      => 'el el-bullhorn',
        'fields'    => array(
          array(
              'id'       => 'campaing-link',
              'type'     => 'text',
              'title'    => __( 'Link para o banner campanha', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'campaing-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Imagem para o banner', 'redux-framework-demo'),
              'desc'     => __('Tamanho ideal: 303 x 415', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=>'',
                  'width' => '303',
                  'height' => '415'
              ),
          )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'     => __( 'Listagem', 'redux-framework-demo' ),
        'id'        => 'campaing-list',
        'desc'      => __( 'Listagem campanha', 'redux-framework-demo' ),
        'subsection'      => true,
        'fields'    => array(
          array(
              'id'       => 'section-campanha-one',
              'type'     => 'section',
              'title'    => __( 'Cabeçalho da página', 'redux-framework-demo' ),
              'subtitle' => __( 'Interação com as imagens (Campanha e lookbook)', 'redux-framework-demo' ),
              'indent'   => true,
          ),
          array(
              'id'          => 'campanha-header-type',
              'type'        => 'typography', 
              'title'       => __('Estilo do título', 'redux-framework-demo'),
              'google'      => true, 
              'font-backup' => true,
              'output'      => array('#camp-content-list > header h1'),
              'units'       =>'px',
              'font-family' => false,
              'text-align'  => false,
              'line-height' => false,
              'preview'     => false,
              'subtitle'    => __('', 'redux-framework-demo'),
              'default'     => array(
                  'color'       => '#fff', 
                  'font-style'  => '700',
                  'font-size'   => '72px',
                  'line-height'   => '1.4 !important',
              ),
          ),
          array(
              'id'       => 'section-campanha-two',
              'type'     => 'section',
              'title'    => __( 'Galeria', 'redux-framework-demo' ),
              'subtitle' => __( 'Campanha e lookbook', 'redux-framework-demo' ),
              'indent'   => true,
          ),
          array(
              'id'       => 'section-campanha-btn-border',
              'type'     => 'border',
              'title'    => __( 'Borda', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'output'   => array( '#gallery-campaing figure' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => array(
                  'border-color'  => '#fff',
                  'border-style'  => 'solid',
                  'border-top'    => '8px',
                  'border-right'  => '8px',
                  'border-bottom' => '8px',
                  'border-left'   => '8px'
              )
          ),
          array(
              'id'       => 'campaing-bg-color',
              'type'     => 'color',
              'output'      => array('background-color' => '#gallery-campaing'),
              'title'    => __('Cor de fundo', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
        )
    ));
    
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Página interna', 'redux-framework-demo' ),
        'id'        => 'campaing-inner',
        'desc'      => __( 'Página interna da campanha', 'redux-framework-demo' ),
        'subsection'      => true,
        'fields'    => array(
          array(
              'id'       => 'section-campanha-1',
              'type'     => 'section',
              'title'    => __( 'Botões', 'redux-framework-demo' ),
              'subtitle' => __( 'Interação com as imagens (Campanha e lookbook)', 'redux-framework-demo' ),
              'indent'   => true,
          ),
          array(
              'id'       => 'campaing-share-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de compartlhamento', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/share.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-share-img-hover',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de compartlhamento suspenso', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/share-hover.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-like-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de like', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/heart.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-like-img-hover',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de like suspenso', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/heart-hover.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-gift-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de presente', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/gift.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-gift-img-hover',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de presente suspenso', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/gift-hover.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-meter-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de medidas', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/meter.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-meter-img-hover',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de medidas suspenso', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/meter-hover.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-sacola-img',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de compras', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/sacola.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-sacola-img-hover',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone de compras suspenso', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => array(
                  'url'=> get_stylesheet_directory_uri() . '/images/sacola-hover.png',
                  'width' => '303',
                  'height' => '415'
              ),
          ),
          array(
              'id'       => 'campaing-btn-border',
              'type'     => 'color',
              'output'      => array('border-color' => '.ui-list h3','background-color' => '.ui-list ul'),
              'title'    => __('Cor da borda', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'campaing-btn-hover',
              'type'     => 'color',
              'output'      => array('background-color' => '.ui-list h3:hover', 'border-color' => '.ui-list h3:hover'),
              'title'    => __('Background suspenso', 'redux-framework-demo'),
              'subtitle' => __('Hover', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'campaing-btn-social',
              'type'     => 'color',
              'output'      => array('color' => '.ui-list ul a'),
              'title'    => __('Ícones das redes sociais', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#fff',
              'validate' => 'color',
          ),
          array(
              'id'     => 'section-campanha-end-1',
              'type'   => 'section',
              'indent' => false
          ),
        )
    ) );

    //Revendedor
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Revendedor', 'redux-framework-demo' ),
        'id'        => 'smart-section',
        'desc'      => __( 'Bloco Seja um Revendedor', 'redux-framework-demo' ),
        'icon'      => 'el el-shopping-cart',
        'fields'    => array(
          array(
              'id'       => 'smart-icon',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Ícone geral da loja virtual', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => array(
                'url'      => get_stylesheet_directory_uri() . '/images/loja.png'
              ),
          ),
          array(
              'id'       => 'smart-title',
              'type'     => 'text',
              'title'    => __( 'Título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'smart-subtitle',
              'type'     => 'text',
              'title'    => __( 'Descrição', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'smart-btn',
              'type'     => 'text',
              'title'    => __( 'Texto do botão', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
        )
    ) );

    /**
     * PAGINAS
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Páginas', 'redux-framework-demo' ),
        'id'        => 'modabiz-pages',
        'desc'      => __( 'Opções gerais para as páginas padrões do tema', 'redux-framework-demo' ),
        'icon'      => 'el el-website'
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contato', 'redux-framework-demo' ),
        'desc'       => __( 'Opções do template de contato', 'redux-framework-demo' ),
        'id'         => 'page-contato',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'page-contato-emails',
              'type'     => 'text',
              'title'    => __( 'Emails para receber o formulário de contato', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'Coloque os emials separados por vírgula (email@serv.com, email2@serv.com...)', 'redux-framework-demo' ),
              'default'  => '',
          ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Lojas e revendas', 'redux-framework-demo' ),
        'desc'       => __( 'Estilizar o template lojas e revendas', 'redux-framework-demo' ),
        'id'         => 'page-lojas',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'section-lojas-1',
              'type'     => 'section',
              'title'    => __( 'Listagem das lojas', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'lojas-bgcolor',
              'type'     => 'color',
              'output'      => array('background-color' => '#nav-loactions'),
              'title'    => __('Cor de fundo', 'redux-framework-demo'),
              'subtitle' => __('Lista de lojas', 'redux-framework-demo'),
              'default'  => '#FFF',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-abgcolor',
              'type'     => 'color',
              'output'      => array('background-color' => '.item-local.active'),
              'title'    => __('Cor de fundo do item ativo', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#eaeaea',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-h-color',
              'type'     => 'color',
              'output'      => array('color' => '#nav-loactions h4, #nav-loactions h5, #nav-loactions h6, #nav-loactions span[class^="icon-"]'),
              'title'    => __('Cor dos cabeçalhos', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-font-color',
              'type'     => 'color',
              'output'      => array('color' => '#nav-loactions p'),
              'title'    => __('Cor da fonte', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-anchor-color',
              'type'     => 'link_color',
              'title'    => __( 'Cor dos links', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'output'      => array('#nav-loactions a'),
              //'regular'   => false, // Disable Regular Color
              //'hover'     => false, // Disable Hover Color
              //'active'    => false, // Disable Active Color
              //'visited'   => true,  // Enable Visited Color
              'default'  => array(
                  'regular' => '#333',
                  'hover'   => '#666',
                  'active'  => '#333',
              )
          ),
          array(
              'id'       => 'lojas-border-color',
              'type'     => 'color',
              'output'      => array('border-bottom-color' => '.item-local'),
              'title'    => __('Borda do container', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#eaeaea',
              'validate' => 'color',
          ),
          array(
              'id'     => 'section-lojas-end',
              'type'   => 'section',
              'indent' => false, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'section-lojas-2',
              'type'     => 'section',
              'title'    => __( 'Formulário de busca', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'lojas-btn-bgcolor',
              'type'     => 'color',
              'output'      => array('background-color' => '#mapInner .btn-userlocal'),
              'title'    => __('Cor de fundo do botão', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-btnhover-bgcolor',
              'type'     => 'color',
              'output'      => array('background-color' => '#mapInner .btn-userlocal:hover'),
              'title'    => __('Cor de fundo do botão no hover', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#fff',
              'validate' => 'color',
          ),
          array(
              'id'       => 'lojas-btnanchor-color',
              'type'     => 'link_color',
              'title'    => __( 'Fonte do botão', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'output'      => array('#mapInner .btn-userlocal'),
              //'regular'   => false, // Disable Regular Color
              //'hover'     => false, // Disable Hover Color
              //'active'    => false, // Disable Active Color
              //'visited'   => true,  // Enable Visited Color
              'default'  => array(
                  'regular' => '#fff',
                  'hover'   => '#333',
                  'active'  => '#333',
              )
          ),
          array(
              'id'       => 'lojas-input-border',
              'type'     => 'border',
              'title'    => __( 'Borda da busca', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'output'   => array( '#mapInner .form-control' ),
              // An array of CSS selectors to apply this font style to
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => array(
                  'border-color'  => '#333',
                  'border-style'  => 'solid',
                  'border-top'    => '3px',
                  'border-right'  => '3px',
                  'border-bottom' => '3px',
                  'border-left'   => '3px'
              )
          ),
          array(
              'id'       => 'lojas-lupa',
              'type'     => 'color',
              'output'      => array('color' => '#mapInner .icon-search, #mapInner .form-control'),
              'title'    => __('Cor da lupa', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'     => 'section-lojas-2-end',
              'type'   => 'section',
              'indent' => false, // Indent all options below until the next 'section' option is set.
          ),
        )
    ) );
    
    //Seja revendedor
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Seja revendedor', 'redux-framework-demo' ),
        'desc'       => __( 'Opções do template de contato', 'redux-framework-demo' ),
        'id'         => 'page-revendedor',
        'subsection' => true,
        'fields'     => array(
          array(
              'id'       => 'section-revendedor-1',
              'type'     => 'section',
              'title'    => __( 'Cabeçalho', 'redux-framework-demo' ),
              'subtitle' => __( 'Opções de estilo para o cabeçalho do blog', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
            'id'       => 'revendedor-header-bg',
            'type'     => 'media', 
            'url'      => true,
            'title'    => __('Imagem de fundo', 'redux-framework-demo'),
            'desc'     => __('', 'redux-framework-demo'),
            'subtitle' => __('Imagem de background para cabeçalho', 'redux-framework-demo')
          ),
          array(
              'id'          => 'revendedor-header-type',
              'type'        => 'typography', 
              'title'       => __('Estilo do título', 'redux-framework-demo'),
              'google'      => true, 
              'font-backup' => true,
              'output'      => array('#revendedor-header h1, #revendedor-header p'),
              'units'       =>'px',
              'font-family' => false,
              'text-align'  => false,
              'line-height' => false,
              'preview'     => false,
              'subtitle'    => __('', 'redux-framework-demo'),
              'default'     => array(
                  'color'       => '#fff', 
                  'font-style'  => '700',
                  'font-size'   => '32px',
                  'line-height'   => '1.4 !important',
              ),
          ),
          array(
              'id'       => 'revendedor-header-title',
              'type'     => 'text',
              'title'    => __( 'Texto do título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'Revenda a melhor marca de<br> moda do Nordeste',
          ),
          array(
              'id'       => 'revendedor-header-subtitle',
              'type'     => 'text',
              'title'    => __( 'Texto do sub-título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'Conheça todas as vantagens e faça seu cadastro<br> agora mesmo, tudo online',
          ),
          array(
              'id'       => 'revendedor-header-btn',
              'type'     => 'text',
              'title'    => __( 'Texto do botão', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'Cadastre-se agora',
          ),
          array(
              'id'       => 'revendedor-header-btn-a',
              'type'     => 'link_color',
              'title'    => __( 'Link do botão', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'output'   => array('#revendedor-header .button'),
              //'regular'   => false, // Disable Regular Color
              //'hover'     => false, // Disable Hover Color
              'active'    => false, // Disable Active Color
              //'visited'   => true,  // Enable Visited Color
              'default'  => array(
                  'regular' => '#fff',
                  'hover'   => '#333',
                  'active'  => '#fff',
              )
          ),
          array(
              'id'       => 'revendedor-header-btn-border',
              'type'     => 'border',
              'title'    => __( 'Borda do botão', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'output'   => array( '#revendedor-header .button' ),
              // An array of CSS selectors to apply this font style to
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => array(
                  'border-color'  => '#ffffff',
                  'border-style'  => 'solid',
                  'border-top'    => '3px',
                  'border-right'  => '3px',
                  'border-bottom' => '3px',
                  'border-left'   => '3px'
              )
          ),
          array(
            'id'       => 'revendedor-header-btn-hover',
            'type'     => 'color',
            'output'      => array('background-color' => '#revendedor-header .button:hover'),
            'title'    => __('Hover do botão', 'redux-framework-demo'),
            'subtitle' => __('Cor de fundo', 'redux-framework-demo'),
            'default'  => '#ffffff',
            'validate' => 'color',
          ),
          array(
            'id'     => 'section-revendedor-1-end',
            'type'   => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'section-revendedor-2',
              'type'     => 'section',
              'title'    => __( 'Colunas de vantagens', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
            'id'       => 'revendedor-skills',
            'type'     => 'color',
            'output'      => array('color' => '.revendedor-skills p, .revendedor-skills h4'),
            'title'    => __('Cor do texto', 'redux-framework-demo'),
            'subtitle' => __('Cor de fundo', 'redux-framework-demo'),
            'default'  => '#333333',
            'validate' => 'color',
          ),
          array(
            'id'       => 'revendedor-skills-icon-1',
            'type'     => 'media', 
            'url'      => true,
            'title'    => __('Ícone (coluna 1)', 'redux-framework-demo'),
            'desc'     => __('', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo')
          ),
          array(
              'id'       => 'revendedor-skills-title-1',
              'type'     => 'text',
              'title'    => __( 'Título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'revendedor-skills-desc-1',
              'type'     => 'text',
              'title'    => __( 'Descrição', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
            'id'       => 'revendedor-skills-icon-2',
            'type'     => 'media', 
            'url'      => true,
            'title'    => __('Ícone (coluna 2)', 'redux-framework-demo'),
            'desc'     => __('', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo')
          ),
          array(
              'id'       => 'revendedor-skills-title-2',
              'type'     => 'text',
              'title'    => __( 'Título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'revendedor-skills-desc-2',
              'type'     => 'text',
              'title'    => __( 'Descrição', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
            'id'       => 'revendedor-skills-icon-3',
            'type'     => 'media', 
            'url'      => true,
            'title'    => __('Ícone (coluna 3)', 'redux-framework-demo'),
            'desc'     => __('', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo')
          ),
          array(
              'id'       => 'revendedor-skills-title-3',
              'type'     => 'text',
              'title'    => __( 'Título', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
              'id'       => 'revendedor-skills-desc-3',
              'type'     => 'text',
              'title'    => __( 'Descrição', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
          array(
            'id'     => 'section-revendedor-2-end',
            'type'   => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
          ),
           array(
              'id'       => 'section-revendedor-3',
              'type'     => 'section',
              'title'    => __( 'O que precisa', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
            'id'       => 'revendedor-check-color',
            'type'     => 'color',
            'output'      => array('color' => '.icon-check-circle'),
            'title'    => __('Cor do check', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'default'  => '#35912d',
            'validate' => 'color',
          ),
          array(
              'id'       => 'revendedor-multitext',
              'type'     => 'multi_text',
              'title'    => __( 'Lista de características', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' )
          ),
          array(
              'id'       => 'revendedor-pagamento',
              'type'     => 'textarea',
              'title'    => __( 'Descrição da forma de pagamento', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => '',
          ),
        )
    ) );

    /**
     * Blog
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Blog', 'redux-framework-demo' ),
        'id'        => 'modabiz-blog',
        'desc'      => __( 'Opções gerais para as páginas padrões do tema', 'redux-framework-demo' ),
        'icon'      => 'el el-pencil',
        'fields'     => array(
          array(
              'id'       => 'section-start-1',
              'type'     => 'section',
              'title'    => __( 'Cabeçalho do blog', 'redux-framework-demo' ),
              'subtitle' => __( 'Opções de estilo para o cabeçalho do blog', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'blog-header-bg',
              'type'     => 'media', 
              'url'      => true,
              'title'    => __('Imagem de fundo', 'redux-framework-demo'),
              'desc'     => __('', 'redux-framework-demo'),
              'subtitle' => __('Imagem de background para cabeçalho', 'redux-framework-demo')
          ),
          array(
              'id'          => 'blog-header-type',
              'type'        => 'typography', 
              'title'       => __('Estilo do título', 'redux-framework-demo'),
              'google'      => true, 
              'font-backup' => true,
              'output'      => array('#blog-header h1, #blog-header h5'),
              'units'       =>'px',
              'font-family' => false,
              'text-align'  => false,
              'line-height' => false,
              'preview'     => false,
              'subtitle'    => __('', 'redux-framework-demo'),
              'default'     => array(
                  'color'       => '#fff', 
                  'font-style'  => '700',
                  'font-size'   => '72px'
              ),
          ),
          array(
              'id'       => 'blog-header-desc',
              'type'     => 'text',
              'title'    => __( 'Descrição do blog', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => 'notícias e dicas do mundo da moda para você',
          ),
          array(
              'id'     => 'section-end',
              'type'   => 'section',
              'indent' => false, // Indent all options below until the next 'section' option is set.
          ),
          array(
              'id'       => 'section-start-2',
              'type'     => 'section',
              'title'    => __( 'Postagem', 'redux-framework-demo' ),
              'subtitle' => __( 'Estilize a chamada do post na listagem', 'redux-framework-demo' ),
              'indent'   => true, // Indent all options below until the next 'section' option is set.
          ),
          array( 
              'id'       => 'blog-item-border',
              'type'     => 'border',
              'title'    => __('Borda do container', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'output'   => array('.post-item'),
              'desc'     => __('', 'redux-framework-demo'),
              'default'  => array(
                  'border-color'  => '#ccc', 
                  'border-style'  => 'solid', 
                  'border-top'    => '10px', 
                  'border-right'  => '10px', 
                  'border-bottom' => '10px', 
                  'border-left'   => '10px'
              )
          ),
          array(
              'id'             => 'blog-item-padding',
              'type'           => 'spacing',
              'output'         => array('.post-item'),
              'mode'           => 'padding',
              'units'          => array('px','em'),
              'units_extended' => 'false',
              'title'          => __('Margem interna do container', 'redux-framework-demo'),
              'subtitle'       => __('', 'redux-framework-demo'),
              'desc'           => __('', 'redux-framework-demo'),
              'all'            => true,
              'default'            => array(
                  'margin-top'     => '30', 
                  'margin-right'   => '30', 
                  'margin-bottom'  => '30', 
                  'margin-left'    => '30',
                  'units'          => 'px', 
              )
          ),
          array(
              'id'          => 'blog-item-type',
              'type'        => 'typography', 
              'title'       => __('Alinhamento do texto', 'redux-framework-demo'),
              'google'      => true, 
              'font-backup' => true,
              'output'      => array('.post-item'),
              'units'       =>'px',
              'font-family' => false,
              'text-align'  => true,
              'line-height' => false,
              'color'       => false, 
              'font-style'  => false,
              'font-size'   => false,
              'font-weight'   => false,
              'preview'     => false,
              'subtitle'    => __('', 'redux-framework-demo'),
          ),
        ),
        array(
            'id'     => 'section-end-2',
            'type'   => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),
    ) );

    /**
     * Depoimentos
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Depoimentos', 'redux-framework-demo' ),
        'id'        => 'modabiz-testimonials',
        'desc'      => __( '', 'redux-framework-demo' ),
        'icon'      => 'el el-quote-right',
        'fields'    => array(
          array(
              'id'       => 'testimonials-bg',
              'type'     => 'color',
              'output'      => array('background-color' => '#testimonials-slider'),
              'title'    => __('Cor de fundo', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          ),
          array(
              'id'       => 'testimonials-color',
              'type'     => 'color',
              'output'      => array('color' => '#testimonials-slider span, #testimonials-slider p','border-color' => '#testimonials-slider .pager-container span','background-color' => '#testimonials-slider .pager-container span:not(.cycle-pager-active)'),
              'title'    => __('Cor do texto', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#fff',
              'validate' => 'color',
          ),
          array(
              'id'          => 'testimonials-items',
              'type'        => 'slides',
              'title'       => __('Lista de depoimentos', 'redux-framework-demo'),
              'subtitle'    => __('', 'redux-framework-demo'),
              'desc'        => __('', 'redux-framework-demo'),
              'placeholder' => array(
                  'title'           => __('Autor', 'redux-framework-demo'),
                  'description'     => __('Depoimento', 'redux-framework-demo'),
                  'url'             => __('Profissão (Opcional)', 'redux-framework-demo'),
                  'width'             => '50',
              ),
          )
        )
    ) );

    /**
     * Perguntas frequentes
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'FAQ', 'redux-framework-demo' ),
        'id'        => 'modabiz-faq',
        'desc'      => __( '', 'redux-framework-demo' ),
        'icon'      => 'el el-question-sign',
        'fields'    => array(
          array(
              'id'          => 'faq-items',
              'type'        => 'slides',
              'title'       => __('Lista de perguntas/respostas', 'redux-framework-demo'),
              'subtitle'    => __('', 'redux-framework-demo'),
              'desc'        => __('', 'redux-framework-demo'),
              'placeholder' => array(
                  'title'           => __('Pergunta', 'redux-framework-demo'),
                  'description'     => __('Resposta', 'redux-framework-demo'),
              ),
          )
        )
    ) );

    /**
     * Newsletter
     * ---------------------------------------------------------------
     */
    Redux::setSection( $opt_name, array(
        'title'     => __( 'Newsletter', 'redux-framework-demo' ),
        'id'        => 'modabiz-newsletter',
        'desc'      => __( '', 'redux-framework-demo' ),
        'icon'      => 'el el-envelope',
        'fields'    => array(
          array(
              'id'       => 'newsletter-header',
              'type'     => 'text',
              'title'    => __( 'Texto de cabeçalho', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'desc'     => __( 'Texto acima do formulário da newsletter', 'redux-framework-demo' ),
              'default'  => 'Receba todas as nossas novidades por email. Inscreva-se:',
          ),
          array(
              'id'       => 'newsletter-color',
              'type'     => 'color',
              'output'      => array('color' => '#newsletter input, #newsletter .button, #newsletter h5', 'border-color' => '#newsletter input, #newsletter .button'),
              'title'    => __('Cor do texto e bordas', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#FFF',
              'validate' => 'color',
          ),
          array(
              'id'       => 'newsletter-bg',
              'type'     => 'color',
              'output'      => array('background-color' => '#newsletter'),
              'title'    => __('Cor de fundo', 'redux-framework-demo'),
              'subtitle' => __('', 'redux-framework-demo'),
              'default'  => '#333',
              'validate' => 'color',
          )
        )
    ) );

    /*add_filter('redux/field/'. $opt_name .'/slides/render/after','teste_filter');
    function teste_filter($text,$field) {
      //$field['placeholder']['Subtitulo'] = 'teste';
      print_r($field);
      print_r($text);
    }*/


    /**
     * Retorna um array com as imagens e nome de usuário
     * no formato exigido pelo framework
     */
    // function get_hash_imgs($data,&$value) {
    //    $var = file_get_contents('https://api.instagram.com/v1/tags/moda/media/recent?access_token=186283994.c83f2b9.ddee448c35734e39a6374c53ee9aeced');
    //    $var = json_decode($var);
    //
    //    $j = 0;
    //    $arr = array();
    //    foreach($var->data as $user) {
    //      $arr[$j]['alt'] = $var->data[$j]->user->username;
    //      $arr[$j]['img'] = $var->data[$j]->user->profile_picture;
    //      $arr[$j]['url'] = $var->data[$j]->link;
    //      $arr[$j]['thumb'] = $var->data[$j]->images->thumbnail->url;
    //      $j++;
    //    }
    //    //return $arr;
    //    print_r($value);
    // }
    // add_action('redux/field/' . $args['opt_name'] . '/render/before','get_hash_imgs');
    //
    // /**
    //  * Retorna apenas os nomes de usuários
    //  */
    // function get_only_usernames($arr) {
    //   $v = array();
    //   $i = 0;
    //   foreach ($arr as $user) {
    //     $v[$i] = $user['alt'];
    //     $i++;
    //   }
    //   return $v;
    // }

    // $test_arr_instagram = array();
    // add_action('redux/options/' . $args['opt_name'] . '/saved', 'modabiz_saved_options');
    //
    // function modabiz_saved_options($value) {
    // 		// $f = fopen(dirname(__FILE__) . '/main.txt', 'w');
    // 		// fwrite($f, $value['instagram-uri']);
    // 		// fclose($f);
    //     $test_arr_instagram['token'] = $value['instagram-uri'];
    // }
    //add_option( $option, $value, $deprecated, $autoload );

    // add_action('redux/options/' . $args['opt_name'] . '/saved', 'jtdn_instagram_opts');
    // function jtdn_instagram_opts($value) {
    //   if( !get_option( 'jt_hash_imgs_cur' ) ) {
    //     add_option( 'jt_hash_imgs_cur' );
    //   }
    //   update_option( 'jt_hash_imgs_cur', $value['instagram-uri'] );
    // }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'call_hash_imgs' ) ) {
        function call_hash_imgs( $field, $value ) {
            //var_dump(get_option('jt_hash_imgs_cur'));
            echo '<ul class="jt-view-th choose-pics"></ul>';
        }
    }

    if ( ! function_exists( 'call_profile_imgs' ) ) {
        function call_profile_imgs( $field, $value ) {
            //var_dump(get_option('jt_hash_imgs_cur'));
            echo '<ul class="jt-view-profile choose-pics"></ul>';
        }
    }

    /**
     * CSS personalizado
     */
    add_filter('redux/page/' . $args['opt_name'] . '/enqueue/jquery-ui-css','custom_css_admin_redux');
    function custom_css_admin_redux() {
      return  get_stylesheet_directory_uri() . '/inc/redux/sample/custom.css';
    }

    /*
     * <--- END SECTIONS
     */

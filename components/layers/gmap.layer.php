<?php
class Gmap_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'gmap_widget', // Base ID
			__( 'Mapa e geolocalização', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( '', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);

		//Backend scripts
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_footer-widgets.php', array( $this, 'print_scripts' ), 9999 );

	}

	/**
	 * Enqueue scripts.
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'underscore' );
	}

	/**
	 * Print scripts. Scripts do personalizador de camadas
	 *
	 * @since 1.0
	 */
	public function print_scripts() {
		?>
		<script>
			( function( $ ){
				function initColorPicker( widget ) {
					widget.find( '.color-picker' ).wpColorPicker( {
						change: _.throttle( function() { // For Customizer
							$(this).trigger( 'change' );
						}, 3000 )
					});
				}

				function onFormUpdate( event, widget ) {
					initColorPicker( widget );
				}

				$( document ).on( 'widget-added widget-updated', onFormUpdate );

				$( document ).ready( function() {
					$( '#widgets-right .widget:has(.color-picker)' ).each( function () {
						initColorPicker( $( this ) );
					} );
				} );
			}( jQuery ) );
		</script>
		<?php
	}

	//Incorpore a folha de estilo apenas quando
	//estiver visivel
	public function gmap_style() {
		wp_enqueue_style('gmap_style', get_stylesheet_directory_uri() . '/components/layers/css/gmap.block.css', array(), THEME_VERSION, "screen");
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		//Frontend CSS
		add_action('wp_footer',array($this,'gmap_style'),0);

		echo $args['before_widget'];
		//opçoes globais da aplicação
		global $modabiz_option;
		//icone da marca
		$place_icon = (!empty($modabiz_option['gmap-placeicon']['url'])) ? $modabiz_option['gmap-placeicon']['url'] : 'http://gmaps-samples.googlecode.com/svn/trunk/markers/red/blank.png';
		//icone marca over
		$place_icon_over = (!empty($modabiz_option['gmap-placeicon-over']['url'])) ? $modabiz_option['gmap-placeicon-over']['url'] : 'http://gmaps-samples.googlecode.com/svn/trunk/markers/red/blank.png';
		//icone da localização do usuário
		$user_icon = (!empty($modabiz_option['gmap-geoicon']['url'])) ? $modabiz_option['gmap-geoicon']['url'] : 'http://gmaps-samples.googlecode.com/svn/trunk/markers/green/blank.png';
		//json com dados de localização das lojas
		$places = get_stylesheet_directory_uri() . "/places.json";
		//localização incial, se houver
		$initLat = $modabiz_option['gmap-lat'];
		$initLng = $modabiz_option['gmap-lng'];

		//Armazena o conteudo em vetor
		$content = array($places, $user_icon, $place_icon, $place_icon_over, $initLat, $initLng);

		//Armazena flags para ser sustituida pelo conteudo
		$blockContent = array('{% local_data %}','{% user_icon %}','{% brand_icon %}','{% brand_icon_over %}','{% gmap_lat %}','{% gmap_lng %}');

		//Bloco com o html do mapa
		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/gmap.block.html');

		//Contruir a camada com o conteudo final
		if ( ! empty( $instance['title'] ) ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			$str = str_replace('{% widget_title %}',$title,$str);
		} else {
			$str = str_replace('<h2>{% widget_title %}</h2>','',$str);
		}

		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		//Estilizar a camada
		$color1 = ( ! empty( $instance['color1'] ) ) ? $instance['color1'] : '#f1f1f1';
		$color2 = ( ! empty( $instance['color2'] ) ) ? $instance['color2'] : '#333333';
		$color3 = ( ! empty( $instance['color3'] ) ) ? $instance['color3'] : '#ffffff';
		$color4 = ( ! empty( $instance['color4'] ) ) ? $instance['color4'] : '#333333';
		$color5 = ( ! empty( $instance['color5'] ) ) ? $instance['color5'] : '#333333';
		$color6 = ( ! empty( $instance['color6'] ) ) ? $instance['color6'] : '#f4f4f4';
		$color7 = ( ! empty( $instance['color7'] ) ) ? $instance['color7'] : '#ffffff';
		$search_map_bw = ( ! empty( $instance['search_map_bw'] ) ) ? $instance['search_map_bw'] : '4';
		$info_bg = ( ! empty( $instance['info_bg'] ) ) ? $instance['info_bg'] : '#333333';
		$info_color = ( ! empty( $instance['info_color'] ) ) ? $instance['info_color'] : '#333333';
		$secondary = ( ! empty( $instance['secondary'] ) ) ? $instance['secondary'] : '#48992d';

		$str_css = file_get_contents(get_stylesheet_directory_uri() . '/components/layers/css/gmap.block.model.txt');

		$block_current_css = array(
			'{% bg-section %}',
			'{% title-color %}',
			'{% btn-bg %}',
			'{% btn-color %}',
			'{% search-bg %}',
			'{% search-color %}',
			'{% search-bw %}',
			'{% info-bg %}',
			'{% info-color %}',
			'{% secondary %}',
			'{% search-bc %}'
		);

		$block_new_css = array(
			$color1,
			$color2,
			$color3,
			$color4,
			$color5,
			$color6,
			$search_map_bw,
			$info_bg,
			$info_color,
			$secondary,
			$color7
		);

		$str_css = str_ireplace(
			$block_current_css,
			$block_new_css,
			$str_css
		);

		$f = fopen(dirname(__FILE__) . '/css/gmap.block.css', 'w');
		fwrite($f, $str_css);
		fclose($f);

		//Exibir a camada
		print($str);

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		/* Titulo */
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __(  'A unidade mais próxima de você', 'modabiz' );
		$instance = wp_parse_args(
			$instance,
			array(
				'color1' => '#f1f1f1',
				'color2' => '#333333',
				'color3' => '#ffffff',
				'color4' => '#333333',
				'color5' => '#333333',
				'color6' => '#333333',
				'color7' => '#ffffff',
				'search_map_bw' => 4,
				'info_bg' => '#333333',
				'info_color' => '#ffffff',
				'secondary' => '#48992d',
			)
		);

		$color1 = esc_attr( $instance[ 'color1' ] );
		$color2 = esc_attr( $instance[ 'color2' ] );
		$color3 = esc_attr( $instance[ 'color3' ] );
		$color4 = esc_attr( $instance[ 'color4' ] );
		$color5 = esc_attr( $instance[ 'color5' ] );
		$color6 = esc_attr( $instance[ 'color6' ] );
		$color7 = esc_attr( $instance[ 'color7' ] );
		$search_map_bw = esc_attr( $instance[ 'search_map_bw' ] );
		$info_bg = esc_attr( $instance[ 'info_bg' ] );
		$info_color = esc_attr( $instance[ 'info_color' ] );
		$secondary = esc_attr( $instance[ 'secondary' ] );
		?>
		<style>
			.mb-widget-label
			{color: darkgray;padding: 10px;background-color: whitesmoke;border-bottom: 1px solid darkgray;}
		</style>
		<h3 class="mb-widget-label">Geral</h3>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color1' ); ?>"><?php _e( 'Cor de fundo:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color1' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color1' ); ?>" value="<?php echo $color1; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color2' ); ?>"><?php _e( 'Cor do título:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color2' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color2' ); ?>" value="<?php echo $color2; ?>" data-default-color="#f00" />
		</p>
		<h3 class="mb-widget-label">Botão de geolocalização</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'color3' ); ?>"><?php _e( 'Fundo do botão:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color3' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color3' ); ?>" value="<?php echo $color3; ?>" data-default-color="#f00" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color4' ); ?>"><?php _e( 'Fonte do botão:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color4' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color4' ); ?>" value="<?php echo $color4; ?>" data-default-color="#f00" />
		</p>
		<h3 class="mb-widget-label">Campo de busca</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'color5' ); ?>"><?php _e( 'Fundo da busca:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color5' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color5' ); ?>" value="<?php echo $color5; ?>" data-default-color="#f00" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color6' ); ?>"><?php _e( 'Fonte da busca:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color6' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color6' ); ?>" value="<?php echo $color6; ?>" data-default-color="#f00" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'search_map_bw' ); ?>"><?php _e( 'Borda da busca (largura):' ); ?></label><br>
			<input type="number" name="<?php echo $this->get_field_name( 'search_map_bw' ); ?>" id="<?php echo $this->get_field_id( 'search_map_bw' ); ?>" value="<?php echo $search_map_bw; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color7' ); ?>"><?php _e( 'Borda da busca (cor):' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'color7' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'color7' ); ?>" value="<?php echo $color7; ?>" data-default-color="#ffffff" />
		</p>
		<h3 class="mb-widget-label">Caixa de informações</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'info_bg' ); ?>"><?php _e( 'Background das informações:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'info_bg' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'info_bg' ); ?>" value="<?php echo $info_bg; ?>" data-default-color="#f00" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'info_color' ); ?>"><?php _e( 'Fonte das informações:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'info_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'info_color' ); ?>" value="<?php echo $info_color; ?>" data-default-color="#ffffff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'secondary' ); ?>"><?php _e( 'Cor secundária:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'secondary' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'secondary' ); ?>" value="<?php echo $secondary; ?>" data-default-color="#ffffff" />
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : __(  'A unidade mais próxima de você', 'modabiz' );
		$instance[ 'color1' ] = strip_tags( $new_instance['color1'] );
		$instance[ 'color2' ] = strip_tags( $new_instance['color2'] );
		$instance[ 'color3' ] = strip_tags( $new_instance['color3'] );
		$instance[ 'color4' ] = strip_tags( $new_instance['color4'] );
		$instance[ 'color5' ] = strip_tags( $new_instance['color5'] );
		$instance[ 'color6' ] = strip_tags( $new_instance['color6'] );
		$instance[ 'color7' ] = strip_tags( $new_instance['color7'] );
		$instance[ 'search_map_bw' ] = strip_tags( $new_instance['search_map_bw'] );
		$instance[ 'info_bg' ] = strip_tags( $new_instance['info_bg'] );
		$instance[ 'info_color' ] = strip_tags( $new_instance['info_color'] );
		$instance[ 'secondary' ] = strip_tags( $new_instance['secondary'] );

		return $instance;

	}

}

?>

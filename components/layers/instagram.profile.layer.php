<?php
class Instagram_Profile_Widget extends WP_Widget {

	/**
	 * Registrar camada
	 */
	function __construct() {
		parent::__construct(
			'instagram_profile_widget', // Base ID
			__( 'Instagram - Fotos do perfil', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( '', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_footer-widgets.php', array( $this, 'print_scripts' ), 9999 );
	}

	//Incorpore a folha de estilo apenas quando
	//estiver visivel
	public function profile_style() {
		wp_enqueue_style('profile_style', get_stylesheet_directory_uri() . '/components/layers/css/instagram.profile.block.css', array(), THEME_VERSION, "screen");
	}

	/**
	 * Injetar scripts
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'underscore' );
	}

	/**
	 * Exibir scripts
	 *
	 * @since 1.0
	 */
	public function print_scripts() {
		?>
		<script>
			( function( $ ){
				function initColorPicker( widget ) {
					widget.find( '.color-picker' ).wpColorPicker( {
						change: _.throttle( function() { // For Customizer
							$(this).trigger( 'change' );
						}, 3000 )
					});
				}

				function onFormUpdate( event, widget ) {
					initColorPicker( widget );
				}

				$( document ).on( 'widget-added widget-updated', onFormUpdate );

				$( document ).ready( function() {
					$( '#widgets-right .widget:has(.color-picker)' ).each( function () {
						initColorPicker( $( this ) );
					} );
				} );
			}( jQuery ) );
		</script>
		<?php
	}

	/**
	 * Ajustes frontend da camda
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     argumentos da camada
	 * @param array $instance valores salvos para o banco de dados
	 */
	public function widget( $args, $instance ) {
		//Frontend CSS
		add_action('wp_footer',array($this,'profile_style'));

		echo $args['before_widget'];
		//opçoes globais da aplicação
		global $modabiz_option;

		//Propriedades dinamicas
		$bg_section_pf = ( ! empty( $instance['bg_section_pf'] ) ) ? $instance['bg_section_pf'] : '#333';
		$bg_profile = ( ! empty( $instance['bg_profile'] ) ) ? $instance['bg_profile'] : '#069';
		$bd_width_pf = ( ! empty( $instance['bd_width_pf'] ) ) ? $instance['bd_width_pf'] : '10px';
		$bd_color_pf = ( ! empty( $instance['bd_color_pf'] ) ) ? $instance['bd_color_pf'] : '#fff';
		$a_color_pf = ( ! empty( $instance['a_color_pf'] ) ) ? $instance['a_color_pf'] : '#fff';

		//CSS
		//--------------------------
		$str_css = file_get_contents(get_stylesheet_directory_uri() . '/components/layers/css/instagram.profile.block.model.txt');
		$block_current_css = array(
			'{% bg-section %}',
			'{% bg-profile %}',
			'{% bd-width %}',
			'{% bd-color %}',
			'{% a-color %}'
		);
		$block_new_css = array(
			$bg_section_pf,
			$bg_profile,
			$bd_width_pf,
			$bd_color_pf,
			$a_color_pf
		);
		$str_css = str_ireplace(
			$block_current_css,
			$block_new_css,
			$str_css
		);
		$f = fopen(dirname(__FILE__) . '/css/instagram.profile.block.css', 'w');
		fwrite($f, $str_css);
		fclose($f);

		//HTML
		//--------------------------

		//Galeria de imagens usando o usuário corrente
		$gallery = get_option( 'userprofile_object' );
		shuffle($gallery);
		$i = 0;
		$html = '<ul class="small-block-grid-2 medium-block-grid-4 collapse">';
		foreach ($gallery as $pic) {
			if(4 == $i) break;
			$html .= '<li>';
			$html .= '<a href="'. $pic['uri'] .'" target="_blank" title="'. $pic['user'] .'" class="d-block small-12 left rel">';
			$html .= '<img src="'. $pic['thumb'] .'" alt="'. $pic['user'] .'">';
			$html .= '<span class="icon-instagram abs"></span>';
			$html .= '</a></li>';
			$i++;
		}
		$html .= '</ul>';

		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/instagram.profile.block.html');
		//flags do html para reescrita
		$blockContent = array('{% profile %}','{% gallery %}');
		//reescreva as tags, na mesma ordem do conteudo
		$content = array($modabiz_option['instagram-username'],$html);
		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		print($str);
		echo $args['after_widget'];
	}

	/**
	 * Backend
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previamente salvo no banco de dados
	 */
	public function form( $instance ) {

		$instance = wp_parse_args(
			$instance,
			array(
				'bg_section_pf' => '#333',
				'bg_profile' => '#069',
				'bd_width_pf' => '10px',
				'bd_color_pf' => '#fff',
				'a_color_pf' => '#fff',
			)
		);
		$bg_section_pf = esc_attr( $instance[ 'bg_section_pf' ] );
		$bg_profile = esc_attr( $instance[ 'bg_profile' ] );
		$bd_width_pf = esc_attr( $instance[ 'bd_width_pf' ] );
		$bd_color_pf = esc_attr( $instance[ 'bd_color_pf' ] );
		$a_color_pf = esc_attr( $instance[ 'a_color_pf' ] );
		?>
		<style>
			.mb-widget-label
			{color: darkgray;padding: 10px;background-color: whitesmoke;border-bottom: 1px solid darkgray;}
		</style>
		<h3 class="mb-widget-label">Geral</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'bg_section_pf' ); ?>"><?php _e( 'Cor de fundo:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bg_section_pf' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bg_section_pf' ); ?>" value="<?php echo $bg_section_pf; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bg_profile' ); ?>"><?php _e( 'Cor de fundo com nome de usuário:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bg_profile' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bg_profile' ); ?>" value="<?php echo $bg_profile; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_width_pf' ); ?>"><?php _e( 'Largura da borda da do bloco com nome de usuário:' ); ?></label><br>
			<input type="number" name="<?php echo $this->get_field_name( 'bd_width_pf' ); ?>" id="<?php echo $this->get_field_id( 'bd_width_pf' ); ?>" value="<?php echo $bd_width_pf; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_color_pf' ); ?>"><?php _e( 'Cor da borda da do bloco com nome de usuário:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bd_color_pf' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bd_color_pf' ); ?>" value="<?php echo $bd_color_pf; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'a_color_pf' ); ?>"><?php _e( 'Cor do texto:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'a_color_pf' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'a_color_pf' ); ?>" value="<?php echo $a_color_pf; ?>" data-default-color="#fff" />
		</p>
		<?php
	}

	/**
	 * Validar valores salvos
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Valores a serem salvos
	 * @param array $old_instance Previamente salvo no banco de dados
	 *
	 * @return array Dados atualizados
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance[ 'bg_section_pf' ] = strip_tags( $new_instance['bg_section_pf'] );
		$instance[ 'bg_profile' ] = strip_tags( $new_instance['bg_profile'] );
		$instance[ 'bd_width_pf' ] = strip_tags( $new_instance['bd_width_pf'] );
		$instance[ 'bd_color_pf' ] = strip_tags( $new_instance['bd_color_pf'] );
		$instance[ 'a_color_pf' ] = strip_tags( $new_instance['a_color_pf'] );

		return $instance;

	}

}

?>

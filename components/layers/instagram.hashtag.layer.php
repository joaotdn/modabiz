<?php
class Instagram_Hash_Widget extends WP_Widget {
/**
	 * Registrar camada
	 */
	function __construct() {
		parent::__construct(
			'instagram_hash_widget', // Base ID
			__( 'Instagram - Álbum da hashtag', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( '', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_footer-widgets.php', array( $this, 'print_scripts' ), 9999 );
	}

	//Incorpore a folha de estilo apenas quando
	//estiver visivel
	public function hashtag_style() {
		wp_enqueue_style('hashtag_style', get_stylesheet_directory_uri() . '/components/layers/css/instagramhash.block.css', array(), THEME_VERSION, "screen");
	}

	/**
	 * Injetar scripts
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'underscore' );
	}

	/**
	 * Exibir scripts
	 *
	 * @since 1.0
	 */
	public function print_scripts() {
		?>
		<script>
			( function( $ ){
				function initColorPicker( widget ) {
					widget.find( '.color-picker' ).wpColorPicker( {
						change: _.throttle( function() { // For Customizer
							$(this).trigger( 'change' );
						}, 3000 )
					});
				}

				function onFormUpdate( event, widget ) {
					initColorPicker( widget );
				}

				$( document ).on( 'widget-added widget-updated', onFormUpdate );

				$( document ).ready( function() {
					$( '#widgets-right .widget:has(.color-picker)' ).each( function () {
						initColorPicker( $( this ) );
					} );
				} );
			}( jQuery ) );
		</script>
		<?php
	}

	/**
	 * Ajustes frontend da camda
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     argumentos da camada
	 * @param array $instance valores salvos para o banco de dados
	 */
	public function widget( $args, $instance ) {
		//Frontend CSS
		add_action('wp_footer',array($this,'hashtag_style'));

		echo $args['before_widget'];
		//opçoes globais da aplicação
		global $modabiz_option;

		//Propriedades dinamicas
		$bg_section = ( ! empty( $instance['bg_section'] ) ) ? $instance['bg_section'] : '#333';
		$bg_hash = ( ! empty( $instance['bg_hash'] ) ) ? $instance['bg_hash'] : '#069';
		$bd_width = ( ! empty( $instance['bd_width'] ) ) ? $instance['bd_width'] : '10px';
		$bd_color = ( ! empty( $instance['bd_color'] ) ) ? $instance['bd_color'] : '#fff';
		$a_color = ( ! empty( $instance['a_color'] ) ) ? $instance['a_color'] : '#fff';

		//CSS
		//--------------------------
		$str_css = file_get_contents(get_stylesheet_directory_uri() . '/components/layers/css/instagramhash.block.model.txt');
		$block_current_css = array(
			'{% bg-section %}',
			'{% bg-hash %}',
			'{% bd-width %}',
			'{% bd-color %}',
			'{% a-color %}'
		);
		$block_new_css = array(
			$bg_section,
			$bg_hash,
			$bd_width,
			$bd_color,
			$a_color
		);
		$str_css = str_ireplace(
			$block_current_css,
			$block_new_css,
			$str_css
		);
		$f = fopen(dirname(__FILE__) . '/css/instagramhash.block.css', 'w');
		fwrite($f, $str_css);
		fclose($f);

		//HTML
		//--------------------------

		//Conteudo dinamico
		$title = apply_filters( 'widget_title', $instance['title'] );

		//Galeria de imagens usando a hashtag corrente
		$gallery = get_option( 'hashtag_object' );
		shuffle($gallery);
		$i = 0;
		$html = '<ul class="small-block-grid-2 medium-block-grid-4 collapse">';
		foreach ($gallery as $pic) {
			if(8 == $i) break;
			$html .= '<li>';
			$html .= '<a href="'. $pic['uri'] .'" target="_blank" title="'. $pic['user'] .'" class="d-block small-12 left rel">';
			$html .= '<img src="'. $pic['thumb'] .'" alt="'. $pic['user'] .'">';
			$html .= '<span class="icon-instagram abs"></span>';
			$html .= '</a></li>';
			$i++;
		}
		$html .= '</ul>';

		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/instagram.hashtag.block.html');
		//flags do html para reescrita
		$blockContent = array('{% title %}','{% hash %}','{% gallery %}');
		//reescreva as tags, na mesma ordem do conteudo
		$content = array($title,$modabiz_option['instagram-hash'],$html);
		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		print($str);
		echo $args['after_widget'];
	}

	/**
	 * Backend
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previamente salvo no banco de dados
	 */
	public function form( $instance ) {
		/* Titulo */
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __(  'Publique suas fotos com a hashtag:', 'modabiz' );

		$instance = wp_parse_args(
			$instance,
			array(
				'bg_section' => '#333',
				'bg_hash' => '#069',
				'bd_width' => '10px',
				'bd_color' => '#fff',
				'a_color' => '#fff',
			)
		);
		$bg_section = esc_attr( $instance[ 'bg_section' ] );
		$bg_hash = esc_attr( $instance[ 'bg_hash' ] );
		$bd_width = esc_attr( $instance[ 'bd_width' ] );
		$bd_color = esc_attr( $instance[ 'bd_color' ] );
		$a_color = esc_attr( $instance[ 'a_color' ] );
		?>
		<style>
			.mb-widget-label
			{color: darkgray;padding: 10px;background-color: whitesmoke;border-bottom: 1px solid darkgray;}
		</style>
		<h3 class="mb-widget-label">Geral</h3>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Título:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bg_section' ); ?>"><?php _e( 'Cor de fundo:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bg_section' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bg_section' ); ?>" value="<?php echo $bg_section; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bg_hash' ); ?>"><?php _e( 'Cor de fundo com a Hashtag:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bg_hash' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bg_hash' ); ?>" value="<?php echo $bg_hash; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_width' ); ?>"><?php _e( 'Largura da borda da do bloco com a Hashtag:' ); ?></label><br>
			<input type="number" name="<?php echo $this->get_field_name( 'bd_width' ); ?>" id="<?php echo $this->get_field_id( 'bd_width' ); ?>" value="<?php echo $bd_width; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_color' ); ?>"><?php _e( 'Cor da borda da do bloco com a Hashtag:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bd_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bd_color' ); ?>" value="<?php echo $bd_color; ?>" data-default-color="#fff" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'a_color' ); ?>"><?php _e( 'Cor do texto:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'a_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'a_color' ); ?>" value="<?php echo $a_color; ?>" data-default-color="#fff" />
		</p>
		<?php
	}

	/**
	 * Validar valores salvos
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Valores a serem salvos
	 * @param array $old_instance Previamente salvo no banco de dados
	 *
	 * @return array Dados atualizados
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : __(  'Publique suas fotos com a hashtag:', 'modabiz' );
		$instance[ 'bg_section' ] = strip_tags( $new_instance['bg_section'] );
		$instance[ 'bg_hash' ] = strip_tags( $new_instance['bg_hash'] );
		$instance[ 'bd_width' ] = strip_tags( $new_instance['bd_width'] );
		$instance[ 'bd_color' ] = strip_tags( $new_instance['bd_color'] );
		$instance[ 'a_color' ] = strip_tags( $new_instance['a_color'] );

		return $instance;

	}

}

?>

<?php
class Panel_Widget extends WP_Widget {

	/**
	 * Registrar camada
	 */
	function __construct() {
		parent::__construct(
			'panel_widget', // Base ID
			__( 'Painel de sliders', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( '', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);
	}

	/**
	 * Ajustes frontend da camada
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     argumentos da camada
	 * @param array $instance valores salvos para o banco de dados
	 */
	public function widget( $args, $instance ) {
		//opçoes globais da aplicação
		global $modabiz_option;
		//$banners = file_get_contents(get_stylesheet_directory_uri() . "/banners.json");
		//$data_panel = json_decode($banners);
		ob_start();
		foreach($modabiz_option['slide-gallery'] as $data):
			$url = (isset($data['url']) && !empty($data['url'])) ? $data['url'] : '#';
		?>
		<figure class="small-12 left rel" role="banner">
		  <div class="black-mask"></div>
	      <div class="small-12 abs slider-th" data-thumb="<?php echo $data['image']; ?>"></div>
	      <div class="row rel d-table">
	        <hgroup class="small-10 small-offset-1 text-center d-table-cell">	
	          <h1 class="lh-small no-margin"><a href="<?php echo $url; ?>" title="<?php echo $data['title']; ?> rel"><?php echo $data['title']; ?></a></h1>
	          <h4 class="font-lite"><a href="<?php echo $url; ?>" title="<?php echo $data['title']; ?>"><?php echo $data['description']; ?></a></h4>
	          <?php if(!empty($data['subtitle'])): ?>
	          <h2 class="small-12 left text-center">
	          	<a href="<?php echo $url; ?>" class="button text-up rel"><?php echo $data['subtitle']; ?></a>
	          </h2>
	          <?php endif; ?>
	        </hgroup>
	      </div>
	    </figure>
		<?php
		endforeach;
		//Conteudo armazenado em buffer
		$result = ob_get_contents();
		ob_end_clean();

		//Armazena o conteudo em vetor
		$content = array($result, get_stylesheet_directory_uri() . '/images/load_panel.gif');

		//Armazena flags para ser sustituida pelo conteudo
		$blockContent = array('{% list-banners %}','{% load-img %}');

		//Bloco com o html do mapa
		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/panel.block.html');

		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		//Exibir a camada
		print($str);
	}

	/**
	 * Backend
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previamente salvo no banco de dados
	 */
	public function form( $instance ) {

	}

	/**
	 * Validar valores salvos
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Valores a serem salvos
	 * @param array $old_instance Previamente salvo no banco de dados
	 *
	 * @return array Dados atualizados
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
	}

}

?>

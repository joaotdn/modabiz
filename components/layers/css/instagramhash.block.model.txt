#instagram-hash {
  background-color: {% bg-section %};
  padding: 50px 0;
}
.hash-panel {
  background-color: {% bg-hash %};
  display: table;
  border: {% bd-width %}px solid {% bd-color %};
  height: 318px;
}
.hash-panel > div {
  display: table-cell;
  vertical-align: middle;
}
.hash-panel h1 {
  font-size: 56px;
  margin-bottom: 20px;
}
.hash-panel h1,
.hash-panel h2,
.hash-panel p,
.hash-gallery a > span {
  color: {% a-color %};
}

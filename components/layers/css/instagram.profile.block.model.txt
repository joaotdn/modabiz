#instagram-profile {
  background-color: {% bg-section %};
  padding: 50px 0;
}
.profile-panel {
  background-color: {% bg-profile %};
  display: table;
  border: {% bd-width %}px solid {% bd-color %};
  height: 159px;
}
.profile-panel > div {
  display: table-cell;
  vertical-align: middle;
}
.profile-panel h1 {
  font-size: 46px;
  margin-bottom: 15px;
}
.profile-panel h1,
.profile-panel h3,
.profile-panel p,
.profile-gallery a > span {
  color: {% a-color %};
}

<?php
class Gallery_Widget extends WP_Widget {

	/**
	 * Registrar camada
	 */
	function __construct() {
		parent::__construct(
			'gallery_widget', // Base ID
			__( 'Grade de fotos e banners', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( '', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);
	}

	/**
	 * Ajustes frontend da camda
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     argumentos da camada
	 * @param array $instance valores salvos para o banco de dados
	 */
	public function widget( $args, $instance ) {

		//HTML
		//---------------------------------------------------------------------------
		//opçoes globais da aplicação
		global $modabiz_option;

		ob_start();
		foreach($modabiz_option['grid-photos'] as $data):
			if(!empty($data['image'])):
			$url = (isset($data['url']) && !empty($data['url'])) ? $data['url'] : '#';
			$video_id = uniqid();
		?>
		<div class="small-6 column gallery-item">
			<figure class="small-12 left gallery-item">
			<img src="<?php echo $data['image']; ?>" alt="">
			<?php
				if(!empty($data['description'])):
			?>
			<a href="#" data-reveal-id="vide-<?php echo $video_id; ?>" class="small-12 abs left-axy d-block play-media" title="<?php echo $data['title']; ?>">
				<div class="d-table small-12 left">
					<span class="d-table-cell small-12 text-center">
						<i class="icon-play3"></i>
					</span>
				</div>
			</a>
			<div id="vide-<?php echo $video_id; ?>" class="reveal-modal video-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			  <figure class="flex-video no-margin" data-media='<?php echo $data['description']; ?>'>
			  	
			  </figure>
			  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
			<?php
				else:
			?>
			<a href="<?php echo $data['image']; ?>" data-lightbox="modabiz" class="small-12 abs left-axy d-block" title="<?php echo $data['title']; ?>"></a>
			<?php endif; ?>
		</figure>
		</div>
		<?php
		endif; endforeach;
		//Conteudo armazenado em buffer
		$result = ob_get_contents();
		ob_end_clean();

		if(!empty($modabiz_option['grid-banner-top']['url'])) {
			$banner_top = '<div class="small-12 column gallery-item"><figure class="small-12 left gallery-item rel"><img src="'. $modabiz_option['grid-banner-top']['url'] .'" alt="">';
			if(!empty($modabiz_option['grid-banner-link-1']))
				$banner_top .= '<a href="'. $modabiz_option['grid-banner-link-1'] .'" class="small-12 abs left-axy d-block" title="Campanha"></a>';
			$banner_top .= '</figure></div>';
		} else {
			$banner_top = NULL;
		}

		if(!empty($modabiz_option['grid-banner-bottom']['url'])) {
			$banner_bottom = '<div class="small-12 column gallery-item"><figure class="small-12 left gallery-item rel"><img src="'. $modabiz_option['grid-banner-bottom']['url'] .'" alt="">';
			if(!empty($modabiz_option['grid-banner-link-2']))
				$banner_bottom .= '<a href="'. $modabiz_option['grid-banner-link-2'] .'" class="small-12 abs left-axy d-block" title="Campanha"></a>';
			$banner_bottom .= '</figure></div>';
		} else {
			$banner_bottom = NULL;
		}

		//Armazena o conteudo em vetor
		$content = array($result,$banner_top,$banner_bottom,get_stylesheet_directory_uri() . '/images/load_circle.gif');

		//Armazena flags para ser sustituida pelo conteudo
		$blockContent = array('{% gallery %}','{% banner-top %}','{% banner-bottom %}','{% loader %}');

		//Bloco com o html do mapa
		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/gallery.block.html');

		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		//Exibir a camada
		print($str);
	}

	/**
	 * Backend
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previamente salvo no banco de dados
	 */
	public function form( $instance ) {

	}

	/**
	 * Validar valores salvos
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Valores a serem salvos
	 * @param array $old_instance Previamente salvo no banco de dados
	 *
	 * @return array Dados atualizados
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
	}

}

?>

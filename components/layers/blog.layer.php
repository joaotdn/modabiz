<?php
class Blog_Widget extends WP_Widget {

	/**
	 * Registrar camada
	 */
	function __construct() {
		parent::__construct(
			'blogpost_widget', // Base ID
			__( 'Blog / Campanha', 'modabiz' ), // Name
			//'dashicons-performance', // Icon
			array( 'description' => __( 'Duas últimas postagem do blog mais banner de campanha', 'modabiz' ),'classname'   => 'color-picker-widget') // Args
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_footer-widgets.php', array( $this, 'print_scripts' ), 9999 );
	}

	//Incorpore a folha de estilo apenas quando
	//estiver visivel
	public function blogposts_style() {
		wp_enqueue_style('blogposts_style', get_stylesheet_directory_uri() . '/components/layers/css/blog.block.css', array(), THEME_VERSION, "screen");
	}

	/**
	 * Injetar scripts
	 *
	 * @since 1.0
	 *
	 * @param string $hook_suffix
	 */
	public function enqueue_scripts( $hook_suffix ) {
		if ( 'widgets.php' !== $hook_suffix ) {
			return;
		}

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'underscore' );
	}

	/**
	 * Exibir scripts
	 *
	 * @since 1.0
	 */
	public function print_scripts() {
		?>
		<script>
			( function( $ ){
				function initColorPicker( widget ) {
					widget.find( '.color-picker' ).wpColorPicker( {
						change: _.throttle( function() { // For Customizer
							$(this).trigger( 'change' );
						}, 3000 )
					});
				}

				function onFormUpdate( event, widget ) {
					initColorPicker( widget );
				}

				$( document ).on( 'widget-added widget-updated', onFormUpdate );

				$( document ).ready( function() {
					$( '#widgets-right .widget:has(.color-picker)' ).each( function () {
						initColorPicker( $( this ) );
					} );
				} );
			}( jQuery ) );
		</script>
		<?php
	}

	/**
	 * Ajustes frontend da camda
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     argumentos da camada
	 * @param array $instance valores salvos para o banco de dados
	 */
	public function widget( $args, $instance ) {
		//Frontend CSS
		add_action('wp_footer',array($this,'blogposts_style'));

		//opçoes globais da aplicação
		global $modabiz_option;

		//Propriedades dinamicas
		$bg_section = ( ! empty( $instance['bg_section'] ) ) ? $instance['bg_section'] : '#333';
		$bd_width = ( ! empty( $instance['bd_width'] ) ) ? $instance['bd_width'] : '10';
		$bd_color = ( ! empty( $instance['bd_color'] ) ) ? $instance['bd_color'] : '#fff';
		$font_color = ( ! empty( $instance['font_color'] ) ) ? $instance['font_color'] : '#333333';
		$a_color = ( ! empty( $instance['a_color'] ) ) ? $instance['a_color'] : '#333333';
		$hover_color = ( ! empty( $instance['hover_color'] ) ) ? $instance['hover_color'] : '#333333';

		//CSS
		//--------------------------
		$str_css = file_get_contents(get_stylesheet_directory_uri() . '/components/layers/css/blog.block.model.txt');
		$block_current_css = array(
			'{% bg-section %}',
			'{% bd-width %}',
			'{% bd-color %}',
			'{% font-color %}',
			'{% a-color %}',
			'{% hover-color %}',
		);
		$block_new_css = array(
			$bg_section,
			$bd_width,
			$bd_color,
			$font_color,
			$a_color,
			$hover_color
		);
		$str_css = str_ireplace(
			$block_current_css,
			$block_new_css,
			$str_css
		);
		$f = fopen(dirname(__FILE__) . '/css/blog.block.css', 'w');
		fwrite($f, $str_css);
		fclose($f);

		//HTML
		//--------------------------

		//Posts
		$args = array( 'posts_per_page' => 2, 'taxonomy' => 'category' );
		$_posts = get_posts( $args );
		$html = '';

		foreach ($_posts as $post) { setup_postdata( $post );
			$html .= '<article class="blog-card small-12 medium-4 columns end">';
			$html .= '<div class="d-table small-12 left" data-thumb="'. getThumbUrl("blog-component", $post->ID) .'">';
			$html .= '<div class="d-table-cell small-12 text-center">';
			$html .= '<h5 class="font-lite text-up"><a href="'. get_first_category_link($post->ID) .'" title="'. get_first_category_name($post->ID) .'">'. get_first_category_name($post->ID) .'</a></h5>';
			$html .= '<h4><a href="'. get_permalink($post->ID) .'" title="'. get_the_title($post->ID) .'">'. get_the_title($post->ID) .'</a></h4>';
			$html .= '<p>'. substr(get_the_excerpt(), 0, 50) .'</p>';
			$html .= '<p class="font-small"><time pubdate>'. get_the_time('d \d\e F, Y') .'</time></p>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</article>';
		}

		if(isset($modabiz_option['campaing-img']) && !empty($modabiz_option['campaing-img']['url'])) {
			$camp_img = '<article class="blog-card small-12 medium-4 columns campaing rel">';
			$camp_img .= '<div class="d-table small-12 left" data-thumb="'.  $modabiz_option['campaing-img']['url'].'">';
			if(isset($modabiz_option['campaing-link']) && !empty($modabiz_option['campaing-link'])) {
				$camp_img .= '<a href="'. $modabiz_option['campaing-link'] .'" title="" class="d-block small-12 abs full-height"></a>';
			}
			$camp_img .= '</div>';
			$camp_img .= '</article>';
		} else {
			$camp_img = '';
		}

		$str = file_get_contents(get_stylesheet_directory_uri() . '/components/blocks/blog.block.html');
		//flags do html para reescrita
		$blockContent = array('{% list-posts %}','{% campaing %}');
		//reescreva as tags, na mesma ordem do conteudo
		$content = array($html,$camp_img);

		$str = str_ireplace(
			$blockContent,
			$content,
			$str
		);

		print($str);
	}

	/**
	 * Backend
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previamente salvo no banco de dados
	 */
	public function form( $instance ) {

		$instance = wp_parse_args(
			$instance,
			array(
				'bg_section' => '#333',
				'bd_width' => '10',
				'bd_color' => '#333',
				'font_color' => '#333',
				'a_color' => '#333',
				'hover_color' => '#333',
			)
		);

		$bg_section = esc_attr( $instance[ 'bg_section' ] );
		$bd_width = esc_attr( $instance[ 'bd_width' ] );
		$bd_color = esc_attr( $instance[ 'bd_color' ] );
		$font_color = esc_attr( $instance[ 'font_color' ] );
		$a_color = esc_attr( $instance[ 'a_color' ] );
		$hover_color = esc_attr( $instance[ 'hover_color' ] );

		?>
		<style>
			.mb-widget-label
			{color: darkgray;padding: 10px;background-color: whitesmoke;border-bottom: 1px solid darkgray;}
		</style>
		<h3 class="mb-widget-label">Geral</h3>
		<p>
			<label for="<?php echo $this->get_field_id( 'bg_section' ); ?>"><?php _e( 'Cor de fundo:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bg_section' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bg_section' ); ?>" value="<?php echo $bg_section; ?>" data-default-color="#333" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_color' ); ?>"><?php _e( 'Cor das bordas:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'bd_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'bd_color' ); ?>" value="<?php echo $bd_color; ?>" data-default-color="#333" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'bd_width' ); ?>"><?php _e( 'Largura da borda:' ); ?></label><br>
			<input type="number" name="<?php echo $this->get_field_name( 'bd_width' ); ?>" id="<?php echo $this->get_field_id( 'bd_width' ); ?>" value="<?php echo $bd_width; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'font_color' ); ?>"><?php _e( 'Cor da fonte:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'font_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'font_color' ); ?>" value="<?php echo $font_color; ?>" data-default-color="#333" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'a_color' ); ?>"><?php _e( 'Cor dos links:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'a_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'a_color' ); ?>" value="<?php echo $a_color; ?>" data-default-color="#333" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'hover_color' ); ?>"><?php _e( 'Cor do hover:' ); ?></label><br>
			<input type="text" name="<?php echo $this->get_field_name( 'hover_color' ); ?>" class="color-picker" id="<?php echo $this->get_field_id( 'hover_color' ); ?>" value="<?php echo $hover_color; ?>" data-default-color="#333" />
		</p>
		<?php
	}

	/**
	 * Validar valores salvos
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Valores a serem salvos
	 * @param array $old_instance Previamente salvo no banco de dados
	 *
	 * @return array Dados atualizados
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance[ 'bg_section' ] = strip_tags( $new_instance['bg_section'] );
		$instance[ 'bd_width' ] = strip_tags( $new_instance['bd_width'] );
		$instance[ 'bd_color' ] = strip_tags( $new_instance['bd_color'] );
		$instance[ 'font_color' ] = strip_tags( $new_instance['font_color'] );
		$instance[ 'a_color' ] = strip_tags( $new_instance['a_color'] );
		$instance[ 'hover_color' ] = strip_tags( $new_instance['hover_color'] );

		return $instance;
	}
}

?>

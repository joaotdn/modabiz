<!-- Menu principal topo -->

<!--off-menu-->
<?php global $modabiz_option; ?>

<a href="#" class="hide-menu"></a>
<nav id="off-menu">
  <header class="small-12 left">
    <a href="#" class="close-menu right">
      <span class="icon-cancel"></span>
    </a>
    <h4 class="small-12 left"><a href="<?php echo home_url();?>" title="<?php bloginfo('name'); ?>">Página principal</a></h4>
  </header>

  <ul class="small-12 left no-bullet">
    <?php
       $defaults = array(
         'theme_location'  => '',
         'menu'            => 'Menu topo',
         'container'       => '',
         'container_class' => '',
         'container_id'    => '',
         'menu_class'      => '',
         'menu_id'         => '',
         'echo'            => true,
         'fallback_cb'     => 'main_menu',
         'before'          => '<h4>',
         'after'           => '</h4>',
         'link_before'     => '',
         'link_after'      => '',
         'items_wrap'      => '%3$s',
         'depth'           => 0,
         'walker'          => '',
       );
       wp_nav_menu($defaults);

       if(!empty($modabiz_option['corp-loja']))
        echo '<li><h4><a href="'. $modabiz_option['corp-loja'] .'" title="Nossa loja virtual" target="_blank">Loja Virtual</a></h4></li>';
    ?>
  </ul>

  <ul class="small-12 left inline-list clear-first social-off">
    <?php
      echo ModaBizUtils::listSocialNetWork();
    ?>
  </ul>
</nav>

<?php //marca da entidade ?>
<nav id="main-menu" class="small-12 left" role="navigation">
  <div class="row rel">
    <div class="d-table-cell small-12">
      <div class="small-12 columns">
        <figure class="d-iblock logo" role="img">
            <a href="<?php echo home_url();?>" class="d-block" title="<?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?>">
              <?php
                $logo = (!empty($modabiz_option['modabiz-brand-img']['url'])) ? $modabiz_option['modabiz-brand-img']['url'] : get_template_directory_uri() . '/images/logo.png';
              ?>
              <img src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" />
            </a>
        </figure>
        <h1 class="no-margin right toggle-menu">
          <i class="icon-menu"></i>
        </h1>
      </div>
    </div>
  </div>
</nav>

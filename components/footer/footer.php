<footer id="footer" class="small-12 left">
  <div class="row">
    <section class="small-12 medium-4 columns">
      <header class="small-12 left text-center">
        <?php
          global $modabiz_option;

          $logo = (!empty($modabiz_option['modabiz-brand-img']['url'])) ? $modabiz_option['modabiz-brand-img']['url'] : get_template_directory_uri() . '/images/logo.png';
        ?>
        <h1>
          <a href="<?php echo home_url();?>" class="d-block logo-footer" title="<?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?>">
            <img src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" />
          </a>
        </h1>
        <p class="text-up">Endereço do escritório</p>
        <p>
          <?php
            //rua, numero
            if(!empty($modabiz_option['corp-rua']))
              echo $modabiz_option['corp-rua'] .'<br>';
            if(!empty($modabiz_option['corp-bairro']))
              echo $modabiz_option['corp-bairro'] . ', ';
            if(!empty($modabiz_option['corp-cidade']))
              echo $modabiz_option['corp-cidade'];
            if(!empty($modabiz_option['corp-cep']))
              echo $modabiz_option['corp-cep'];
          ?>
        </p>

        <?php
          //Endereço no GoogleMAps
          if($modabiz_option['corp-map'])
            echo '<p class="small-12 left blank-map"><strong><a href="'. $modabiz_option['corp-map'] .'" class="text-up" title="Ver endereço no Google Maps" target="_blank">Ver endereço no Google Maps</a></strong></p>';
        ?>

        <?php
          // Link para o ModaBiz
          if(MODABIZ_URL):
        ?>
        <p class="no-margin small-12 text-center"><a href="<?php echo MODABIZ_URL; ?>" target="_blank" title="Ir para o ModaBiz"><img src="<?php echo get_template_directory_uri();?>/images/icon.png" alt="ModaBiz" /></a></p>
        <?php endif; ?>
      </header>
    </section>

    <section class="small-12 medium-4 columns text-center">
      <nav class="small-12 left social-footer" role="banner">
        <ul class="inline-list d-iblock">
          <?php
            echo ModaBizUtils::listSocialNetWork();
          ?>
        </ul>
      </nav>

      <h5 class="font-lite text-up no-margin">Ligue para <?php bloginfo('name'); ?></h5>
      <h5 class="footer-phone"><a href="tel:(83) 9999-9999">(83) 9999-9999</a></h5>

      <nav class="share-footer small-12 left show-for-large-up">
        <ul class="inline-list d-iblock">
          <li><div class="fb-like" data-layout="button_count" data-href="<?php echo home_url();?>"></div></li>
          <li><a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php echo home_url();?>">Tweet</a></li>
          <li><div class="g-plusone" data-size="medium" data-width="65" data-href="<?php echo home_url();?>"></div></li>
        </ul>
      </nav>

      <div class="credits small-12 left">
        <h5 class="font-lite lh-small"><small><?php echo date('Y'); ?> Copyright - Todos os direitos reservados à <?php bloginfo('name'); ?></small></h5>
      </div>
    </section>

    <section class="small-12 medium-4 columns text-center site-menu footer-menu">
      <ul class="no-bullet d-iblock text-up no-margin">
        <?php
           $defaults = array(
             'theme_location'  => '',
             'menu'            => 'Menu rodape',
             'container'       => '',
             'container_class' => '',
             'container_id'    => '',
             'menu_class'      => '',
             'menu_id'         => '',
             'echo'            => true,
             'fallback_cb'     => 'footer_menu',
             'before'          => '<h6>',
             'after'           => '</h6>',
             'link_before'     => '',
             'link_after'      => '',
             'items_wrap'      => '%3$s',
             'depth'           => 0,
             'walker'          => '',
           );
           wp_nav_menu($defaults);
        ?>
      </ul>
    </section>
  </div>
</footer>

<?php 
/**
  * @package WordPress
  * @subpackage modabiz
  * @since ModaBiz 1.0
  */
 get_header();

 	?>
	<section id="not-found" class="small-12 left section-block">
		
		<div class="row">
			<header class="small-12 columns text-center">
				
				<figure class="divide-40 text-center">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/error.png" alt="" class="d-iblock">
				</figure>

				<h1>Desculpe :(</h1>

				<div class="divide-40"></div>

				<p class="font-large divide-40">A página que você procura não pode ser encontrada</p>
				
				<p><strong>O que pode ter acontecio:</strong></p>
				<p>1. A url pode estar errada</p>
				<p>2. O conteúdo pode estar fora do ar</p>
				<p>A página mudou de endereço</p>
			</header>

			<div class="divide-40"></div>

			<div class="small-12 medium-6 columns">
				<form action="<?php echo home_url(); ?>" class="divide-30 rel" id="search-form" method="get">
					<label for="s"><input name="s" type="text" id="s" class="small-12 left" placeholder="PESQUISAR NO BLOG"></label>
					<span class="icon-search abs"></span>
				</form>
			</div>

			<div class="small-12 medium-6 columns">
				<a href="<?php echo home_url();?>" title="Volte para o início" class="button generic-btn text-up small-12 left">Volte para o início</a>
			</div>
		</div>

	</section>
 	<?php

 get_footer();
?>
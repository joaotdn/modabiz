<?php
/**
 * CPT para Lookbooks
 *
 * @since ModaBiz 1.0
 * @subpackage ModaBiz
 */

function lookbook_init() {
  $labels = array(
    'name'               => 'Lookbooks',
    'singular_name'      => 'Lookbook',
    'add_new'            => 'Adicionar Novo',
    'add_new_item'       => 'Adicionar novo Lookbook',
    'edit_item'          => 'Editar Lookbook',
    'new_item'           => 'Novo Lookbook',
    'all_items'          => 'Todos os Lookbooks',
    'view_item'          => 'Ver Lookbook',
    'search_items'       => 'Buscar Lookbooks',
    'not_found'          => 'N&atilde;o encontrado',
    'not_found_in_trash' => 'N&atilde;o encontrado',
    'parent_item_colon'  => '',
    'menu_name'          => 'Lookbooks'
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'lookbook' ),
    //'menu_icon'           => get_stylesheet_directory_uri() . '/images/works.png',
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => 2,
    'supports'           => array( 'title','thumbnail' )
  );

  register_post_type( 'lookbook', $args );

  $labels = array(
        'name'              => __( 'Coleções'),
        'singular_name'     => __( 'Coleção'),
        'search_items'      =>  __( 'Buscar' ),
        'popular_items'     => __( 'Mais usadas' ),
        'all_items'         => __( 'Todas as Coleções' ),
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => __( 'Adicionar nova' ),
        'update_item'       => __( 'Atualizar' ),
        'rewrite'            => array( 'slug' => 'colecao' ),
        'add_new_item'      => __( 'Adicionar nova Coleção' ),
        'new_item_name'     => __( 'Nova' )
    );
    register_taxonomy("lookbooks", array("lookbook"), array(
        "hierarchical"      => true, 
        "labels"            => $labels, 
        "singular_label"    => "Coleção", 
        "rewrite"           => true,
        "add_new_item"      => "Adicionar nova Coleção",
        "new_item_name"     => "Nova Coleção",
    ));
}
add_action( 'init', 'lookbook_init' );
?>
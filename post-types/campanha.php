<?php
/**
 * CPT para Campanhas
 *
 * @since ModaBiz 1.0
 * @subpackage ModaBiz
 */

function campanha_init() {
  $labels = array(
    'name'               => 'Campanhas',
    'singular_name'      => 'Campanha',
    'add_new'            => 'Adicionar Nova',
    'add_new_item'       => 'Adicionar nova Campanha',
    'edit_item'          => 'Editar Campanha',
    'new_item'           => 'Nova Campanha',
    'all_items'          => 'Todas as Campanhas',
    'view_item'          => 'Ver Campanha',
    'search_items'       => 'Buscar Campanhas',
    'not_found'          => 'N&atilde;o encontrada',
    'not_found_in_trash' => 'N&atilde;o encontrada',
    'parent_item_colon'  => '',
    'menu_name'          => 'Campanhas'
  );

      $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'campanha' ),
        //'menu_icon'           => get_stylesheet_directory_uri() . '/images/works.png',
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => 2,
        'supports'           => array( 'title','thumbnail' )
      );

    register_post_type( 'campanha', $args );

    $labels = array(
        'name'              => __( 'Campanhas'),
        'singular_name'     => __( 'Campanha'),
        'search_items'      =>  __( 'Buscar' ),
        'popular_items'     => __( 'Mais usadas' ),
        'all_items'         => __( 'Todas as Campanhas' ),
        'parent_item'       => null,
        'parent_item_colon' => null,
        'edit_item'         => __( 'Adicionar nova' ),
        'update_item'       => __( 'Atualizar' ),
        'rewrite'            => array( 'slug' => 'campanhas' ),
        'add_new_item'      => __( 'Adicionar nova Campanha' ),
        'new_item_name'     => __( 'Nova' )
    );
    register_taxonomy("campanhas", array("campanha"), array(
        "hierarchical"      => true, 
        "labels"            => $labels, 
        "singular_label"    => "Campanha", 
        "rewrite"           => true,
        "add_new_item"      => "Adicionar nova Campanha",
        "new_item_name"     => "Nova Campanha",
    ));
}
add_action( 'init', 'campanha_init' );
?>
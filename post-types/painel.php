<?php
/**
 * Este CPT prover entradas de dados no lado cliente
 * para configurar a camada contendo o painel
 *
 * @see Panel_Widget
 * @since ModaBiz 1.0
 * @subpackage ModaBiz
 */

function panel_init() {
  $labels = array(
    'name'               => 'Painel',
    'singular_name'      => 'Painel',
    'add_new'            => 'Adicionar Novo',
    'add_new_item'       => 'Adicionar novo Painel',
    'edit_item'          => 'Editar Painel',
    'new_item'           => 'Novo Painel',
    'all_items'          => 'Todos os Painel',
    'view_item'          => 'Ver Painel',
    'search_items'       => 'Buscar Painel',
    'not_found'          => 'N&atilde;o encontrado',
    'not_found_in_trash' => 'N&atilde;o encontrado',
    'parent_item_colon'  => '',
    'menu_name'          => 'Painel'
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'exclude_from_search' => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => '' ),
    //'menu_icon'           => get_stylesheet_directory_uri() . '/images/works.png',
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => 2,
    'supports'           => array( 'title' )
  );

  register_post_type( 'panel', $args );
}
add_action( 'init', 'panel_init' );
?>

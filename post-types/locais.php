<?php
/**
 * Este CPT prover entradas de dados no lado cliente
 * para configurar a camada contendo o mapa de localizaçãp
 * 
 * @see Gmap_Widget
 * @since ModaBiz 1.0
 * @subpackage ModaBiz
 */

function locations_init() {
  $labels = array(
    'name'               => 'Localizações',
    'singular_name'      => 'Localização',
    'add_new'            => 'Adicionar Nova',
    'add_new_item'       => 'Adicionar nova Localização',
    'edit_item'          => 'Editar Localização',
    'new_item'           => 'Nova Localização',
    'all_items'          => 'Todas as Localizações',
    'view_item'          => 'Ver Localização',
    'search_items'       => 'Buscar Localizações',
    'not_found'          => 'N&atilde;o encontrada',
    'not_found_in_trash' => 'N&atilde;o encontrada',
    'parent_item_colon'  => '',
    'menu_name'          => 'Localizações'
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'exclude_from_search' => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => '' ),
    //'menu_icon'           => get_stylesheet_directory_uri() . '/images/works.png',
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => true,
    'menu_position'      => 2,
    'supports'           => array( 'title' )
  );

  register_post_type( 'locations', $args );
}
add_action( 'init', 'locations_init' );
?>
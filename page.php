<?php 
/**
 * Página principal
 */
get_header();  
	
if(is_active_sidebar('home-layers')):
	/**
	 * Camadas para a página principal
	 */
	dynamic_sidebar('home-layers');
endif;

get_footer();
?>

<aside id="sidebar" class="small-12 medium-4 columns">
	<div class="small-12 left">
		<form action="<?php echo home_url(); ?>" class="divide-30 rel" id="search-form" method="get">
			<label for="s"><input name="s" type="text" id="s" class="small-12 left" placeholder="PESQUISAR NO BLOG"></label>
			<span class="icon-search abs"></span>
		</form>

		<nav class="divide-30 blog-cats">
			<header>
				<h4 class="divide-20 lh-small font-bold">Assuntos</h4>
			</header>

			<ul class="no-bullet">
			<?php 
				global $modabiz_option;
			    $tags_array = get_tags( 'hide_empty=0' );
			    //var_dump($tags_array);
			    ob_start();
			    foreach ($tags_array as $tag) {	
			    	$tag_link = get_tag_link( $tag->term_id );
		    	?>
					<li><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?> (<?php echo $tag->count; ?>)</a></li>
		    	<?php
			    }
			    $res = ob_get_contents();
			    ob_clean();
			    echo $res;
			?>
			</ul>
		</nav>
		<?php if(!empty($modabiz_option['corp-facebook'])): ?>
		<div id="like-box" class="divide-30">
			<div class="fb-page" data-href="<?php echo $modabiz_option['corp-facebook']; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
		</div>
		<?php 
			endif; 

			//Instagram
			if(!empty($modabiz_option['instagram-username'])):
		?>
		<div class="divide-30" id="instagram-side">
			<?php if(!empty($modabiz_option['instagram-icon'])): ?>
			<figure class="divide-20 text-center">
				<img src="<?php echo $modabiz_option['instagram-icon']['url']; ?>" alt="" class="d-iblock">
			</figure>
			<?php endif; ?>
			<p class="text-center">
				<a href="http://instagram.com/<?php echo $modabiz_option['instagram-username']; ?>?ref=badge" class="ig-b- ig-b-v-24"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>
			</p>

			<nav class="small-12 left sidebar-instagram">
				<ul class="inline-list no-margin">
				<?php 
					$gallery = get_option( 'userprofile_object' );
					shuffle($gallery);
					$i = 0;
					ob_start();
					foreach ($gallery as $pic) {
						if(4 == $i) break;
						?>
						<li><a href="<?php echo $pic['uri'] ?>" target="_blank"><img src="<?php echo $pic['thumb']; ?>" alt=""></a></li>
						<?php
						$i++;
					}
					$res = ob_get_contents();
					ob_clean();
					echo $res;
				?>
				</ul>
			</nav>
		</div>
		<?php 
			endif;
		?>
		<nav id="popular-posts" class="small-12 left">
			<header class="divide-20"><h4 class="no-margin">Mais lidas</h4></header>
			<ul class="no-bullet">
			<?php
				query_posts('post_type=post&posts_per_page=5&orderby=comment_count&order=DESC');

				while (have_posts()): the_post(); ?>

				<li><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr('%s'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></li>

				<?php
				endwhile;
				wp_reset_query();
			?>
			</ul>
		</nav>
	</div>
</aside>
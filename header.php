<?php
/**
 * Template para exibir cabeçalho da aplicação
 *
 * @package WordPress
 * @subpackage ModaBiz
 * @since ModaBiz 1.0
 */
  $modaUtils = new ModaBizUtils(); // objeto para chamada de utilitarios
  global $modabiz_option; // objeto contendo todas as opções personalizadas do tema
?>
<!doctype html>
<html class="no-js" ng-app="modaBizApp" <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php if(THEME_ICON != ''): ?>
    <link rel="shortcut icon" href="<?php echo THEME_ICON; ?>" type="image/vnd.microsoft.icon"/>
    <link rel="icon" href="<?php echo THEME_ICON; ?>" type="image/x-ico"/>
    <?php endif; ?>
    <link rel="canonical" href="https://dev.twitter.com/web/tweet-button">
    <link rel="me" href="https://twitter.com/twitterdev">
    <script>
      //<![CDATA[
      var getData = {
        'urlDir':'<?php bloginfo('template_directory');?>/',
        'ajaxDir':'<?php echo stripslashes(get_admin_url()).'admin-ajax.php';?>',
      }
      //]]>
    </script>
    <style>.ig-b- { display: inline-block; }
.ig-b- img { visibility: hidden; }
.ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
.ig-b-v-24 { width: 137px; height: 24px; background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0; }
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
.ig-b-v-24 { background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png); background-size: 160px 178px; } }</style>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
  <nav id="nav-fixed" class="small-12"></nav>

    <div id="wrapper">
    <!-- init body -->

    <?php
      /**
       * Menu principal
       * @since  ModaBiz 1.0
       */
      if(!is_404()) require_once ( dirname(__FILE__) . '/components/navigation/menu.top.php' );
    ?>

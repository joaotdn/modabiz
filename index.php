<?php

/**
 * Página principal
 */
get_header();
	/**
	 * Camadas para a página principal
	 */
	if(is_active_sidebar('home-layers')):
		dynamic_sidebar('home-layers');
	endif;

get_footer();

?>

<?php
 global $modabiz_option;
 $obj = get_queried_object();

 get_header();
?>	
	<section id="camp-content-list" class="small-12 left">
		
		<!-- cabeçalho -->
		<header id="revendedor-header" class="small-12 left d-table rel" data-thumb="<?php echo get_field('tax_bg',$obj); ?>">
			<div class="black-mask"></div>
			<hgroup class="small-12 text-center d-table-cell rel">
				<div class="row">
					<h1 class="no-margin small-12 columns"><?php echo single_cat_title(); ?></h1>
				</div>
			</hgroup>
		</header>
		
		<section id="gallery-campaing" class="small-12 left section-block rel">
			<div class="row">
				<!-- galeria -->
				<nav class="small-12 left photo-collection grid js-masonry">
					<?php
	              		if (have_posts()) : while (have_posts()) : the_post();
	              		global $post;
	              		$th = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
	            	?>
	            	<div class="small-6 column">
						<figure class="small-12 left gallery-item">
							
							<header class="divide-10 ui-list abs">
								<h3 class="d-table left no-margin rel" data-icon-hover="<?php echo $modabiz_option['campaing-share-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-share-img']['url']; ?>">
									<div class="d-table-cell small-12 text-center">
										<img src="<?php echo $modabiz_option['campaing-share-img']['url']; ?>" alt="" class="icon_share">
									</div>
									<ul class="no-bullet no-margin abs social-thumb">
										<li>
											<a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank"><span class="icon-facebook cpn"></span></a>
										</li>
										<li>
											<a href="http://twitter.com/intent/tweet?status=<?php the_title(); ?>+<?php the_permalink(); ?>" target="_blank"><span class="icon-twitter cpn"></span></a>
										</li>
										<li>
											<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $th[0]; ?>&url=<?php echo $th[0]; ?>&is_video=false&description=<?php the_title(); ?>" target="_blank"><span class="icon-pinterest2 cpn"></span></a>
										</li>
									</ul>
								</h3>
							</header>

							<img src="<?php echo $th[0]; ?>" alt="">
							<a href="<?php the_permalink(); ?>" class="small-12 abs left-axy d-block cpn" title="<?php the_title(); ?>"></a>
						</figure>	
					</div>
					<?php endwhile; endif; ?>
				</nav>
			</div>
		</section>

	</section>
	
 	<?php
 	//if(is_active_sidebar('contact-layers')):
		/**
		 * Camadas para a página principal
		 */
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>
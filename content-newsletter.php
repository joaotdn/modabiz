<?php global $modabiz_option; ?>
<section id="newsletter" class="small-12 left">
	<div class="row">
		<form class="small-12 medium-8 medium-offset-2 left">
			<header class="divide-20 column text-center">
				<h5 class="text-up no-margin"><?php echo $modabiz_option['newsletter-header']; ?></h5>
			</header>

			<label for="email" class="small-12 medium-8 columns">
				<input required title="Seu e-mail" type="email" name="email" class="small-12 left" placeholder="Seu e-mail"> 
			</label>

            <div class="divide-20 show-for-small-only"></div>

			<label class="small-12 medium-4 columns">
				<button class="small-12 button generic-btn text-up text-center send-newsletter">Assinar</button>
			</label>
		</form>
	</div>
</section>
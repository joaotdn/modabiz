<?php
global $modabiz_option;
get_header();
	?>
	
	<section id="query-blog" class="small-12 left rel">
		<header id="blog-header" class="small-12 left d-table rel" data-thumb="<?php echo $modabiz_option['blog-header-bg']['url']; ?>">
			<div class="black-mask"></div>
			<div class="small-12 d-table-cell">
				<hgroup class="small-12 no-margin left text-center">
					<h1 class="text-up">Blog</h1>
					<?php if(!empty($modabiz_option['blog-header-desc'])) echo "<h5 class=\"text-low\">{$modabiz_option['blog-header-desc']}</h5>"; ?>
				</hgroup>
			</div>
		</header>
		<div class="row">
			<?php
          		if (have_posts()) : while (have_posts()) : the_post();
        	?>
			<article class="small-12 medium-8 left section-block article-content">
				<header class="article-header">
					<p>
						<time><i class="icon-calendar3"></i> <?php echo the_time('d \d\e F, Y'); ?></time>
					</p>
					<h1 class="divide-20"><?php the_title(); ?></h1>

					<div class="post-author divide-20">
						<p class="small-12 left no-margin post-author text-up">
							<strong class="left">Por: <?php echo the_author_meta( 'first_name' ); ?></strong>
							<strong class="right"><span><i class="icon-bubble"></i> <?php comments_number( 'Sem comentários', '1 comentário', '% comentários' ); ?></span></strong>
						</p>
					</div>

					<nav class="divide-20 post-tags">
						<ul class="inline-list font-bold d-iblock no-margin">
			            <?php
			              /*
			                Tags
			               */
			              if(get_the_tags()):
			                $posttags = get_the_tags();
			                foreach ($posttags as $tag):
			                  $tag_link = get_tag_link($tag->term_id);
			                ?>
			                <li><h5><a href="<?php echo $tag_link; ?>"><?php echo $tag->name; ?></a></h5></li>
			                <?php 
			                    endforeach;
			                  endif;
			                ?>
			            </ul>
					</nav>

					<nav class="post-item-share divide-20">
						<ul class="inline-list d-iblock">
				          <li><div class="fb-like" data-layout="button_count" data-href="<?php the_permalink(); ?>"></div></li>
				          <li><a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>">Tweet</a></li>
				          <li><div class="g-plusone" data-size="medium" data-width="65" data-href="<?php the_permalink(); ?>"></div></li>
				        </ul>
					</nav>

				</header>

				<?php
					$video = get_field('post_video');
					if(!empty($video)) {
						echo "<figure class=\"divide-30\">";
						echo '<div class="flex-video no-margin">' . $video . '</div>';
						echo "</figure>";
					}
				?>

				<?php the_content(); ?>

				<nav class="post-item-share divide-20">
					<ul class="inline-list d-iblock">
			          <li><div class="fb-like" data-layout="button_count" data-href="<?php the_permalink(); ?>"></div></li>
			          <li><a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>">Tweet</a></li>
			          <li><div class="g-plusone" data-size="medium" data-width="65" data-href="<?php the_permalink(); ?>"></div></li>
			        </ul>
				</nav>

				<nav id="comments" class="divide-30">
		            <div id="disqus_thread"></div>
		              <script type="text/javascript">
		                  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		                  var disqus_shortname = 'modabiz'; // required: replace example with your forum shortname

		                  /* * * DON'T EDIT BELOW THIS LINE * * */
		                  (function() {
		                      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		                      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		                      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		                  })();
		              </script>
		              <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		        </nav>

			</article>
			<?php endwhile; endif; ?>

			<?php get_sidebar(); ?>
			
		</div>
	</section>
	
	<section id="blog-layer" class="small-12 left">
		<div class="row">
		<?php
			$args = array( 'posts_per_page' => 3, 'taxonomy' => 'category', 'post__not_in' => array($post->ID) );
			$_posts = get_posts( $args );
			$html = '';

			foreach ($_posts as $post) { setup_postdata( $post );
				$html .= '<article class="blog-card small-12 medium-4 columns end">';
				$html .= '<div class="d-table small-12 left" data-thumb="'. getThumbUrl("blog-component", $post->ID) .'">';
				$html .= '<div class="d-table-cell small-12 text-center">';
				$html .= '<h5 class="font-lite text-up"><a href="'. get_first_category_link($post->ID) .'" title="'. get_first_category_name($post->ID) .'">'. get_first_category_name($post->ID) .'</a></h5>';
				$html .= '<h4><a href="'. get_permalink($post->ID) .'" title="'. get_the_title($post->ID) .'">'. get_the_title($post->ID) .'</a></h4>';
				$html .= '<p>'. substr(get_the_excerpt(), 0, 50) .'</p>';
				$html .= '<p class="font-small"><time pubdate>'. get_the_time('d \d\e F, Y') .'</time></p>';
				$html .= '</div>';
				$html .= '</div>';
				$html .= '</article>';
			}

			echo $html;
		?>
		</div>
	</section>

<?php

get_template_part( 'content' , 'newsletter' );
get_footer();

?>

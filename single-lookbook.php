<?php 
 global $modabiz_option;
 global $post;

 $th = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
 get_header();
?>	
	<section class="small-12 left section-block">
		<div class="row">
<?php 
	//Breadcrumb
	get_template_part( 'content' , 'breadcrumb' );

	//Cabeçalho da página
	get_template_part( 'content' , 'header' );
?>
			<!-- inicio do template -->
			<section id="campaing-slider" class="small-12 left page-template rel">

				<nav id="campaing-items" class="small-12 columns rel">
<?php
$galeria = get_field('lookbook_galeria',$post->ID);
$thumbs = array();
if($galeria):
	foreach ($galeria as $foto):
		$th = wp_get_attachment_image_src( $foto['lookbook_imagem'], 'full' );
		$thumbs[] = wp_get_attachment_image_src( $foto['lookbook_imagem'], 'thumbnail' )[0];
?>
					<figure class="small-12 left active-thumb item look-<?php echo $foto['lookbook_imagem']; ?>">
						
						<header class="divide-10 ui-list">
							<h3 class="d-table left no-margin rel" data-icon-hover="<?php echo $modabiz_option['campaing-share-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-share-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<img src="<?php echo $modabiz_option['campaing-share-img']['url']; ?>" alt="">
								</div>
								<ul class="no-bullet no-margin abs social-thumb">
									<li>
										<a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank"><span class="icon-facebook"></span></a>
									</li>
									<li>
										<a href="http://twitter.com/intent/tweet?status=<?php the_title(); ?>+<?php the_permalink(); ?>" target="_blank"><span class="icon-twitter"></span></a>
									</li>
									<li>
										<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $th[0]; ?>&url=<?php echo $th[0]; ?>&is_video=false&description=<?php the_title(); ?>" target="_blank"><span class="icon-pinterest2"></span></a>
									</li>
								</ul>
							</h3>

							<h3 class="d-table left no-margin rel" data-icon-hover="<?php echo $modabiz_option['campaing-like-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-like-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<img src="<?php echo $modabiz_option['campaing-like-img']['url']; ?>" alt="">
								</div>
							</h3>

							<h3 class="d-table left no-margin rel" data-reveal-id="meters-<?php echo $foto['lookbook_imagem']; ?>" data-icon-hover="<?php echo $modabiz_option['campaing-meter-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-meter-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<img src="<?php echo $modabiz_option['campaing-meter-img']['url']; ?>" alt="">
								</div>
								
								<!-- modal de medidas -->
								<div id="meters-<?php echo $foto['lookbook_imagem']; ?>" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
								  <h4 id="modalTitle" class="divide-30">Tamanhos e medidas</h4>
								  <div class="custom-select small-12 medium-6 large-5 left rel">
								  	<span class="text-up active" data-showgenero="masculino">Masculino</span>
								  	<span class="text-up deactive" data-showgenero="feminino">Feminino</span>
								  	<i class="icon-chevron-thin-down abs"></i>
								  </div>
								  
								  <div class="divide-20"></div>

								  <div class="list-meters small-12 left">
								  	<header class="table-header divide-20">
									  	<div class="small-3 columns text-center">
									  		<h6 class="no-margin">Numeração</h6>
									  	</div>
									  	<div class="small-3 columns text-center rel">
									  		<h6 class="no-margin">Busto (cm)</h6>
									  		<span class="no-margin font-small" data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Para medir o busto, contorne o tronco na altura do centro do perito com uma fita métrica">como medir?</span>
									  	</div>
									  	<div class="small-3 columns text-center rel">
									  		<h6 class="no-margin">Cintura (cm)</h6>
									  		<span class="no-margin font-small" data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Passe a fita métrica na região próxima ao umbigo, abaixo das costelas.">como medir?</span>
									  	</div>
									  	<div class="small-3 columns text-center rel">
									  		<h6 class="no-margin">Quadril (cm)</h6>
									  		<span class="no-margin font-small" data-tooltip aria-haspopup="true" class="has-tip tip-top" title="Contorne a região mais longa dos quadris para tirar a medida">como medir?</span>
									  	</div>
								  	</header>
<?php
	$medidas = $foto['medidas_masculinas'];
	if($medidas):
?>
								  	<nav class="lb-table small-12 left rel active" data-genero="masculino">
								  		<?php foreach ($medidas as $med): ?>
								  		<div class="table-row small-12 left">
								  			<p class="small-3 no-margin left text-center">
								  				<?php echo $med['medidas_masculinas_numeracao']; ?>
								  			</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_masculinas_busto']; ?>
									  		</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_masculinas_cintura']; ?>
									  		</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_masculinas_quadril']; ?>
									  		</p>
								  		</div>
								  		<?php endforeach; ?>
								  	</nav>
<?php
	endif;

	$medidas = $foto['medidas_femininas'];
	if($medidas):
?>
								  	<nav class="lb-table small-12 left rel" data-genero="feminino">
								  		<?php foreach ($medidas as $med): ?>
								  		<div class="table-row small-12 left">
								  			<p class="small-3 no-margin left text-center">
								  				<?php echo $med['medidas_femininas_numeracao']; ?>
								  			</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_femininas_busto']; ?>
									  		</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_femininas_cintura']; ?>
									  		</p>
									  		<p class="small-3 no-margin left text-center">
									  			<?php echo $med['medidas_femininas_quadril']; ?>
									  		</p>
								  		</div>
								  		<?php endforeach; ?>
								  	</nav>
<?php
	endif;
?>
								  </div>
								  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
								</div>
							</h3>

							<h3 class="d-table left no-margin rel" data-reveal-id="gift-<?php echo $foto['lookbook_imagem']; ?>" data-icon-hover="<?php echo $modabiz_option['campaing-gift-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-gift-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<img src="<?php echo $modabiz_option['campaing-gift-img']['url']; ?>" alt="">
								</div>

								<!-- modal de medidas -->
								<div id="gift-<?php echo $foto['lookbook_imagem']; ?>" class="gift-modal reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
									<h4 id="modalTitle" class="divide-30">Pedir de presente</h4>

									<form class="small-12 left form-gift">
										<input type="email" name="email" placeholder="E-MAIL *" required title="Seu email" class="small-12 left">
										<textarea name="mensagem" placeholder="MENSAGEM" title="Digite uma mensagem" cols="30" rows="10" class="small-12 left" required></textarea>
										<p class="small-12 left text-right no-margin">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/loading.gif" alt="" class="left load-gift">
											<button type="submit" class="button generic-btn text-up">Enviar</button>
										</p>
										<input type="hidden" name="link" value="<?php the_permalink(); ?>">
									</form>

									<a class="close-reveal-modal" aria-label="Close">&#215;</a>
								</div>
							</h3>
<?php if($foto['comprar_peca']): ?>
							<h3 class="d-table left no-margin rel" data-icon-hover="<?php echo $modabiz_option['campaing-sacola-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-sacola-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<a href="<?php echo $foto['comprar_peca']; ?>" target="_blank" class="go-shop"><img src="<?php echo $modabiz_option['campaing-sacola-img']['url']; ?>" alt=""></a>
								</div>
							</h3>
<?php endif; ?>
						</header>

						<div class="campaing-thumb small-12 left rel text-center">
							<img src="<?php echo $th[0]; ?>" alt="" class="d-iblock">
						</div>
					</figure>
<?php
	endforeach;
endif;
?>
				</nav>
				<!-- navegar -->
				<a href="#" class="abs nav-campaing next-campaing d-table small-1 text-center" title="Próximo">
					<div class="d-table-cell">
						<span class="icon-chevron-thin-left"></span>
					</div>
				</a>
				<a href="#" class="abs nav-campaing prev-campaing d-table small-1 text-center" title="Anterior">
					<div class="d-table-cell">
						<span class="icon-chevron-thin-right"></span>
					</div>
				</a>
			</section>

			<div class="divide-20"></div>

			<section id="campaing-carousel" class="small-12 left section-20">
				<nav id="caroulsel-items" class="small-12 columns">
					<?php
						for($i = 0; $i < count($thumbs); $i++) {
							echo '<figure class="item campaing-carousel-th" data-thumb="'. $thumbs[$i] .'"></figure>';
						}
					?>
				</nav>
			</section>
		
		</div><!-- // row -->
	</section>

 	<?php
 	//if(is_active_sidebar('contact-layers')):
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>
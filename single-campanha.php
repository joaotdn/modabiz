<?php 
 global $modabiz_option;
 global $post;

 $th = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
 get_header();
?>	
	<section class="small-12 left section-block">
		<div class="row">
<?php 
	//Breadcrumb
	get_template_part( 'content' , 'breadcrumb' );

	//Cabeçalho da página
	get_template_part( 'content' , 'header' );
?>
			<!-- inicio do template -->
			<section id="campaing-slider" class="small-12 left page-template rel">

				<nav id="campaing-items" class="small-12 columns rel">
<?php
$galeria = get_field('campanha_galeria',$post->ID);
$thumbs = array();
if($galeria):
	foreach ($galeria as $foto):
		$th = wp_get_attachment_image_src( $foto['campanha_imagem'], 'full' );
		$thumbs[] = wp_get_attachment_image_src( $foto['campanha_imagem'], 'thumbnail' )[0];
?>
					<figure class="small-12 left active-thumb item look-<?php echo $foto['campanha_imagem']; ?>">
						
						<header class="divide-10 ui-list">
							<h3 class="d-table left no-margin rel" data-icon-hover="<?php echo $modabiz_option['campaing-share-img-hover']['url']; ?>" data-icon="<?php echo $modabiz_option['campaing-share-img']['url']; ?>">
								<div class="d-table-cell small-12 text-center">
									<img src="<?php echo $modabiz_option['campaing-share-img']['url']; ?>" alt="">
								</div>
								<ul class="no-bullet no-margin abs social-thumb">
									<li>
										<a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank"><span class="icon-facebook"></span></a>
									</li>
									<li>
										<a href="http://twitter.com/intent/tweet?status=<?php the_title(); ?>+<?php the_permalink(); ?>" target="_blank"><span class="icon-twitter"></span></a>
									</li>
									<li>
										<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $th[0]; ?>&url=<?php echo $th[0]; ?>&is_video=false&description=<?php the_title(); ?>" target="_blank"><span class="icon-pinterest2"></span></a>
									</li>
								</ul>
							</h3>
						</header>

						<div class="campaing-thumb small-12 left rel text-center">
							<img src="<?php echo $th[0]; ?>" alt="" class="d-iblock">
						</div>
					</figure>
<?php
	endforeach;
endif;
?>
				</nav>
				<!-- navegar -->
				<a href="#" class="abs nav-campaing next-campaing d-table small-1 text-center" title="Próximo">
					<div class="d-table-cell">
						<span class="icon-chevron-thin-left"></span>
					</div>
				</a>
				<a href="#" class="abs nav-campaing prev-campaing d-table small-1 text-center" title="Anterior">
					<div class="d-table-cell">
						<span class="icon-chevron-thin-right"></span>
					</div>
				</a>
			</section>

			<div class="divide-20"></div>

			<section id="campaing-carousel" class="small-12 left section-20">
				<nav id="caroulsel-items" class="small-12 columns">
					<?php
						for($i = 0; $i < count($thumbs); $i++) {
							echo '<figure class="item campaing-carousel-th" data-thumb="'. $thumbs[$i] .'"></figure>';
						}
					?>
				</nav>
			</section>
		
		</div><!-- // row -->
	</section>

 	<?php
 	//if(is_active_sidebar('contact-layers')):
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>
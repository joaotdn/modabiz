<?php 
/**
  * Template Name: Lojas e revendas
  *
  * @package WordPress
  * @subpackage modabiz
  * @since ModaBiz 1.0
  */

 global $modabiz_option;
 get_header();
?>	
	<section class="small-12 left section-block no-pdb">
		<div class="row">
			<?php 
				//Breadcrumb
				get_template_part( 'content' , 'breadcrumb' );

				//Cabeçalho da página
				get_template_part( 'content' , 'header' );
			?>
		</div>

		<section id="mapInner" class="small-12 left">

			<div class="row">
				<div class="small-12 left" ng-controller="TypeaheadCtrl">
					<p class="small-12 medium-6 small-text-center medium-text-left columns">
						<a href="#" class="btn-userlocal button text-up small-12 left">
							<span>Usar minha localização atual</span>
						</a>
						<span class="city-name"></span>
					</p>
					<p class="search-local small-12 medium-6 small-text-center medium-text-right columns rel">
					    <input type="text" ng-model="asyncSelected" placeholder="FAÇA UMA BUSCA PERSONALIZADA" typeahead="address for address in getLocation($viewValue) | filter:$viewValue" typeahead-loading="loadingLocations" typeahead-on-select="dataLocation(asyncSelected) | filter:$viewValue" class="form-control">
						<span class="icon-search abs show-for-large-up"></span>
					</p>
				</div>
			</div>

			<nav id="nav-loactions" class="small-12 medium-5 left">
				<?php
					$file_locations = file_get_contents(dirname(__FILE__) . '/places.json');
					$array = json_decode($file_locations,true);
					if(isset($array) && !empty($array)):
					ob_start();
					foreach ($array as $local):
				?>
				<div class="small-12 columns item-local" data-lat="<?php echo $local['lat']; ?>" data-lng="<?php echo $local['lng']; ?>" title="<?php echo $local['titulo']; ?>">
					<div class="small-10 small-offset-1 large-8 large-offset-2 left rel">
						<span class="icon-location abs"></span>
						<header class="divide-20">
							<hgroup class="divide-20">
								<h4 class="local-name no-margin"><?php echo $local['titulo']; ?></h4>
								<h6 class="local-type lh-small no-margin"><?php echo $local['tipo']; ?></h6>
							</hgroup>

							<address class="divide-20">
								<?php
									if(!empty($local['endereco']))
										printf('<p class="no-margin">%s</p>',$local['endereco']);
									if(!empty($local['cep']))
										printf('<p class="no-margin">CEP: %s</p>',$local['cep']);
									if(!empty($local['uf']))
										printf('<p class="no-margin">%s</p>',$local['uf']);
								?>
							</address>

							<article class="divide-20">
								<header><h5 class="no-margin">Contato</h5></header>
								<?php
									if(!empty($local['telefones']))
										printf('<p class="no-margin">%s</p>',$local['telefones']);
									if(!empty($local['email']))
										printf('<p><a href="mailto:%s">%s</a></p>',$local['email'],$local['email']);
									if(!empty($local['facebook']) && !empty($local['instagram'])):
								?>
								<p class="social-share">
									<?php
										if(!empty($local['facebook']))
											printf('<a href="%s" target="_blank" class="d-iblock icon-facebook-with-circle"></a>',$local['facebook']);
										if(!empty($local['instagram']))
											printf('<a href="%s" target="_blank" class="d-iblock icon-instagram-with-circle"></a>',$local['instagram']);
									?>
								</p>
								
								<?php
									if(!empty($local['horarios']))
										printf('<header><h5 class="no-margin">Horários</h5></header><p class="no-margin">%s</p>',$local['horarios']);
								?>
								<?php endif; ?>
							</article>
						</header>
					</div>
				</div>
				<?php
					endforeach;

					$result = ob_get_contents();
					ob_clean();
					echo $result;

					endif;

					//icone da marca
					$place_icon = (!empty($modabiz_option['gmap-placeicon']['url'])) ? $modabiz_option['gmap-placeicon']['url'] : 'http://gmaps-samples.googlecode.com/svn/trunk/markers/red/blank.png';
					//icone da localização do usuário
					$user_icon = (!empty($modabiz_option['gmap-geoicon']['url'])) ? $modabiz_option['gmap-geoicon']['url'] : 'http://gmaps-samples.googlecode.com/svn/trunk/markers/green/blank.png';
					//json com dados de localização das lojas
					$places = get_stylesheet_directory_uri() . "/places.json";
					//localização incial, se houver
					$initLat = $modabiz_option['gmap-lat'];
					$initLng = $modabiz_option['gmap-lng'];
				?>
			</nav>
			<figure id="map-layer" class="small-12 medium-7 left inner-layer"
				data-arraylocal="<?php echo $places; ?>"
				data-usericon="<?php echo $user_icon; ?>"
				data-brandicon="<?php echo $place_icon; ?>"
				data-lat="<?php echo $initLat; ?>"
				data-lng="<?php echo $initLng; ?>"
			></figure>
		</section>
	</section>
<?php
 	//if(is_active_sidebar('contact-layers')):
		/**
		 * Camadas para a página principal
		 */
		//dynamic_sidebar('contact-layers');
	//endif;
 get_footer();
?>